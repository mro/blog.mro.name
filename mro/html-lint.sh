#!/bin/sh
 
[ "$1" = "" ] && {
  cat << EOF

EXAMPLE

  \$ $0 a.html sub/b.html

EOF
  exit 1
}

error_exit() {
  echo "please install \$ sudo apt-get install $1" 1>&2
  exit 2
}
xmllint --version 2>/dev/null || error_exit "libxml2-utils"

check_single() {
  ls "$1"
  v="$(xmllint --xpath "string(/*[local-name()='html']/@lang)" "$1")"
  [ "$v" = "" ] && { echo "missing mandatory attribute 'html/@lang'" 1>&2 ; exit 1; }
  printf "  %-18s %s\n" "lang:" "$v"

  for k in title date author keywords ; do
    v="$(xmllint --xpath "string(/*[local-name()='html']/*[local-name()='head']/*[local-name()='meta' and @name='$k']/@content)" "$1")"
    [ "$v" = "" ] && { echo "missing mandatory element '/html/head/meta[@name='$k']" 1>&2 ; exit 1; }
    printf "  %-18s %s\n" "$k:" "$v"
  done
  for k in self terms-of-service privacy-policy license ; do
    v="$(xmllint --xpath "string(/*[local-name()='html']//*[(local-name()='link' or local-name()='a') and contains(@rel,'$k')]/@href)" "$1")"
    [ "$v" = "" ] && { echo "missing mandatory element '/html//*[@rel='$k']" 1>&2 ; exit 1; }
    printf "  %-18s %s\n" "$k:" "$v"
  done
}

while [ "$1" != "" ]
do
  check_single "$1"
  echo ""
  shift
done
