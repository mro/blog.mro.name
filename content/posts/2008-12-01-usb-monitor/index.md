---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2008-12-01T19:00:01+00:00"
tags:
- Display
- USB
title: USB-Monitor
type: post
url: /2008/12/usb-monitor/
yourls_shorturl:
- http://s.mro.name/q
---
[Die Computerzeitung schreibt: 22-Zöller kommt mit USB-Anschluss][1] – wozu also noch ein Display mit rumschleppen? Und eine Tastatur sollte sich vor Ort auftreiben lassen.

Damit hat der Rechner in der Jackentasche Platz.

 [1]: http://www.computerzeitung.de/articles/22-zoeller_kommt_mit_usb-anschluss:/2008050/31755454_ha_CZ.html?rss=1