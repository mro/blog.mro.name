---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2013-06-06T15:45:54+00:00"
tags:
- git
- iOS
- iPhone
- plist
- PlistBuddy
- sha
- version
- XCode
title: git version sha in iOS apps
type: post
url: /2013/06/git-version-sha-in-ios-apps/
yourls_shorturl:
- http://mro.name/4d
---
there's tons of recipes out there how to do this – nevertheless I want to add another one:

{{< figure src="/wp-content/uploads/2013/06/xcode-git-sha-info-plist.png" caption="" width="300" height="213" >}}

1. create a new build phase (here named &#8222;Git Sha -> Info.plist&#8220;)
2. paste

    <pre class="line-numbers"><code class="language-sh">
    # write git version SHA into Info.plist
    #
    # inspired by
    # - http://kswizz.com/post/2686511526/git-xcode-versioning
    # - http://stackoverflow.com/a/12658311
    cd "$PROJECT_DIR"
    # say hello
    /usr/libexec/PlistBuddy -c "Set :CFBundleVersionGitSHA $(git rev-parse --short HEAD)" "$TARGET_BUILD_DIR/$INFOPLIST_PATH"
    </code></pre>
3. add key `CFBundleVersionGitSHA` with placeholder text to your Info.plist

I went for this one because

1. <span style="line-height: 13px;">easy setup,</span>
2. <span style="line-height: 13px;">no build/compiler settings changes,</span>
3. doesn't interfere with 'normal' versioning,
4. simplest I've seen so far, no dependencies except git,
5. explicit failure if e.g. git is missing.

Credits:

* <http://kswizz.com/post/2686511526/git-xcode-versioning>
* <http://stackoverflow.com/a/12658311>
