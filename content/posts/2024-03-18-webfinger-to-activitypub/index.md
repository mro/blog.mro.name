---
title: 'Show: apchk.cgi - a Webfinger => ActivityPub Resolver and Checker'
url: /2024/03/webfinger-to-activitypub/
draft: false
categories:
- en
date: 2024-03-18T12:44:39+0100
tags:
- ActivityPub
- Checker
- Federation
- HTTP
- NLnet
- RFC7033
- Seppo
- WebFinger
type: post
author: Marcus Rohrmoser
---

Part III of 'Implementing [Federation](/tags/federation/)'.

Let me introduce <tt>apchk.cgi</tt>[^apchk] &mdash; the
Webfinger/RFC7033[^rfc7033] checker and forwarder to the ActivityPub Actor
Profile[^AP_actor].

There are 2 common ways to address people in the social-web a.k.a. fediverse:

1. the twitter-lookalike handle, e.g. @alice@example.com or
2. the person's actor profile url, e.g. https://example.com/people/alice 

The first is friendly to humans while it isn't to web browsers. Until
your browser knows how to handle such addresses (which is not on the plate), you
have to feed it into a fediverse client.

Now you can also use the new, highly targeted[^unix] and permacomputing[^perma]-inspired
CGI[^CGI] <tt>apchk.cgi</tt>. Currently it redirects to said ActivityPub actor profile and can parse
and check that as well, but a redirect to the Html profile may come soon.

<tt>apchk</tt> shows you what #Seppo can understand, but as the 'diverse' in
fediverse implies, others may decide differently.

I invite you to try it at Seppo.social[^apchk], but beware, Seppo.social makes no availabilty
guarantees. If you want reliability, do seize agency and operate
<tt>apchk.cgi</tt> yourself. E.g. rent shared hosting webspace (CGI[^cgi]-enabled) starting at
EUR 2,- per month[^vendors] and copy <tt>apchk.cgi</tt> there. No further setup
required.

This goes well with the emancipated, subsidiary claim of the social web, is
welcoming, affordable, sustainable, fair and secure.

If you like it, talk about [@demo@seppo.social](https://seppo.social/demo/apchk.cgi/webfinger?resource=acct:demo@seppo.social&redirect=http://webfinger.net/rel/profile-page) in
the social web!

I am very grateful for the support by @EC_NGI@social.network.europa.eu[^ngi]
under NLnet[^nlnet] grant #Seppo[^grant].

[^AP_actor]:     4.1 Actor objects https://w3.org/TR/activitypub/#actor-objects
[^AP]:           ActivityPub https://www.w3.org/TR/activitypub/
[^CGI]:          "Common Gateway Interface" https://www.ietf.org/rfc/rfc3875.html
[^apchk]:        https://seppo.social/demo/apchk.cgi
[^grant]:        https://NLnet.nl/project/Seppo
[^ngi]:          https://social.network.europa.eu/@EC_NGI
[^nlnet]:        https://NLnet.nl
[^rfc7033]:      WebFinger https://rfc-editor.org/rfc/rfc7033
[^unix]:         "Do one thing and do it well (and play nice with others)"
[^perma]:        https://permacomputing.net/Principles/
[^vendors]:      Map of vendors known to host #Seppoes https://seppo.social/vendors
