---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2008-10-30T21:27:58+00:00"
tags:
- git
- htaccess
- WebDAV
title: Git Repo auf WebDAV (1&1)
type: post
url: /2008/10/git-repo-auf-webdav-11/
yourls_shorturl:
- http://s.mro.name/l
---
<a>E</a>igene [Git][1] Repositories gehen auch mit billigem Shared Hosting recht bequem, falls der Hoster [WebDAV][2] anbietet. Z.B. bei [1&1][3] ist WebDAV auf Port 81 aktiviert.

Und so geht's:

### WebDAV Zugriff

Hier führen viele Wege nach Rom – ich wähle den via [davfs2][4]. Letztlich müssen wir nur in der Lage sein, ein Verzeichnis auf dem WebDAV Server anzulegen. Natürlich geht das auch per [FTP][5], aber wenn wir schon WebDAV haben, benutzen wir's auch gleich dafür.

1. Eintrag in `/etc/fstab`: 
<pre class="line-numbers"><code class="language-shell-session">$ echo "http://www.myserver.example:81/ /home/localusername/mnt/myserver davfs noauto,user,nolocks 0 0" >> /etc/fstab
</code></pre>

2. WebDAV Zugangsdaten für davfs2: 
<pre class="line-numbers"><code class="language-shell-session">$ mkdir -p ~/.davfs2
$ echo "http://www.myserver.example:81/ MyDavUser MyDavPwd" >> ~/.davfs2/secrets
$ chmod 600 ~/.davfs2/secrets
</code></pre>

3. mounten: 
<pre class="line-numbers"><code class="language-shell-session">$ mount /home/localusername/mnt/myserver
</code></pre>

4. das leere Verzeichnis anlegen: 
<pre class="line-numbers"><code class="language-shell-session">$ mkdir /home/localusername/mnt/myserver/myproject.git
</code></pre>

### Ein (remote) Git Repo anlegen

Siehe dazu auch [Shared Git Repository per Webdav][6].

1. ein leeres Verzeichnis myproject.git auf dem WebDAV Server anlegen,
2. lokal ein leeres Git Repo anlegen: 
<pre class="line-numbers"><code class="language-shell-session">$ mkdir myproject.git
$ cd myproject.git
$ git init --bare
</code></pre>

3. ggf. ein bestehendes Git Repo &#8222;importieren&#8220;: 
<pre class="line-numbers"><code class="language-shell-session">$ git pull UrlOfExistingRepo
</code></pre>

4. WebDAV Zugangsdaten für git hinterlegen: 
<pre class="line-numbers"><code class="language-shell-session">$ echo "machine myserver.example login MyDavUser password MyDavPwd" >> ~/.netrc
$ chmod 600 ~/.netrc
</code></pre>

5. und das neue Repo hochladen: 
<pre class="line-numbers"><code class="language-shell-session">$ git push --all --force http://myserver.example:81/myproject.git
</code></pre>

### Nicht-WebDAV Zugriffe verhindern

Soll der Zugriff auf das Repo exklusiv nur per WebDAV erlaubt sein, empfiehlt es sich das Verzeichnis myproject.git auf dem Webserver zu sperren bzw. nur einen Benutzer zu erlauben, der ansonsten nicht benutzt wird.

Das geht über die Management Console des Webhosters oder indem man die entsprechende [.htaccess][7] Datei selbst erstellt und im Verzeichnis myproject.git ablegt.

 [1]: http://de.wikipedia.org/wiki/Git
 [2]: http://de.wikipedia.org/wiki/WebDAV
 [3]: http://www.1und1.de/
 [4]: http://dav.sourceforge.net/
 [5]: http://de.wikipedia.org/wiki/File_Transfer_Protocol
 [6]: https://largo.homelinux.org/blog/?q=node/18
 [7]: http://de.selfhtml.org/servercgi/server/htaccess.htm#verzeichnisschutz
