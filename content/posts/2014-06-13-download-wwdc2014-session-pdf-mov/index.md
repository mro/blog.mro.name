---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2014-06-13T15:56:42+00:00"
tags:
- sh
- curl
- iOS
- WWDC
title: Download WWDC2014 session pdf + mov
type: post
url: /2014/06/download-wwdc2014-session-pdf-mov/
yourls_shorturl:
- http://mro.name/4m
---


<pre class="line-numbers"><code class="language-sh">#!/bin/sh
cd "$(dirname "$0")"

PREFIX=wwdc2014
bwlimit="1000K"

for url in $(curl https://developer.apple.com/videos/wwdc/2014/ | egrep -hoe '[^"]+.pdf[^"]+')
do
  dst="$PREFIX/$(basename $url ?dl=1)"
  echo "$dst"
  curl --output "$dst" --time-cond "$dst" --remote-time --silent --create-dirs --location --limit-rate "$bwlimit" --url "$url" 2>/dev/null
done

for url in $(curl https://developer.apple.com/videos/wwdc/2014/ | egrep -hoe '[^"]+_sd[^"]+')
do
  dst="$PREFIX/$(basename $url ?dl=1)"
  echo "$dst"
  curl --output "$dst" --time-cond "$dst" --remote-time --silent --create-dirs --location --limit-rate "$bwlimit" --url "$url" 2>/dev/null
done
</code></pre>

I think downloading them this way is fair use. If you think otherwise, leave a comment below and I shall take appropriate action.

P.S.: See also the awesome [wwdc transcript fulltext index](https://github.com/mattt/asciiwwdc.com).
