---
author: Marcus Rohrmoser
categories:
- en
- offtopic
date: "1998-11-01T15:39:20+00:00"
tags:
- Collission
- Curling
- Dynamics
- Mechanics
- paper
- Physics
- Rock
- science
- theory
title: Curling Scientific
type: post
url: /1998/11/curling-scientific/
yourls_shorturl:
- http://mro.name/46
yourls_tweeted:
- 1
---

The paper is a bit shaky and has some errors but still it's better than nothing and has some points in it:

[Curling Scientific – a paper on curling rock collission dynamics][1].

P.S.: The wordpress blog-posting is from 2011 but I back-dated it to reflect the article's date.

 [1]: /wp-content/uploads/1998/11/curlsci.pdf