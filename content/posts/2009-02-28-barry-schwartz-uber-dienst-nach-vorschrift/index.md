---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-02-27T23:39:19+00:00"
tags:
- TED
title: Barry Schwartz über Dienst nach Vorschrift
type: post
url: /2009/02/barry-schwartz-uber-dienst-nach-vorschrift/
yourls_shorturl:
- http://s.mro.name/1o
---
  
[original Vortrag bei TED][1]

Die Motivation warum man etwas tut ist eminent wichtig. Und verschiedene Gründe etwas zu tun (die für sich genommen alle dafür sprechen) können zum Gegenteil führen:

&#8222;Wollen Sie ein Atommüllager in ihrer Gemeinde? 50% Ja.&#8220;

oder

&#8222;Wenn wir ihnen 6 Wochenlöhne pro Jahr zahlen – wollen sie ein Atommüllager in ihrer Gemeinde? 25% Ja.&#8220;

Schade. Eine zeitlang habe ich gehofft, das Weltanschauliche, Moralische als Motivation aus &#8222;das Richtige tun&#8220; weitgehend rauszukriegen und systematisch rein auf die [Spieltheorie][2] als Werkzeug zurückzugreifen.

 [1]: http://www.ted.com/talks/view/id/462
 [2]: http://de.wikipedia.org/wiki/Spieltheorie