---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2008-10-08T22:05:16+00:00"
tags:
- gammu
- J2ME
- maven2
title: J2ME & Maven2
type: post
url: /2008/10/j2me-maven2/
yourls_shorturl:
- http://s.mro.name/1v
---
Huh! I just found out, there is a fine [Maven2 Plugin for J2ME][1]. It comes with an [archetype][2] to jump-start developing J2ME applications with [maven][3].

Use the archetype like this:

<pre class="line-numbers"><code class="language-shell-session">$ mvn archetype:create -DarchetypeGroupId=com.pyx4me 
-DarchetypeArtifactId=j2me-simple -DarchetypeVersion=2.0.3-SNAPSHOT 
-DremoteRepositories=http://www.pyx4me.com/maven2 -DgroupId=org.example 
-DartifactId=myapp
</code></pre>

And if you develop against a nokia phone, it comes even better – there's a [gammu maven plugin][4], too!

Also check out this [blog featuring maven2 & j2me][5].

 [1]: http://pyx4me.com/pyx4me-maven-plugins/j2me-maven-plugin/
 [2]: http://pyx4me.com/pyx4me-archetypes/j2me-simple/index.html
 [3]: http://maven.apache.org/
 [4]: http://pyx4me.com/pyx4me-maven-plugins/gammu-maven-plugin/
 [5]: http://codeforfun.wordpress.com/2008/06/23/build-native-blackberry-apps-natively-on-a-mac-using-maven/
