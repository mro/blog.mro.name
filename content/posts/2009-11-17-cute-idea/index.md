---
author: Marcus Rohrmoser
categories:
- en
- screendesign
date: "2009-11-17T10:43:43+00:00"
tags:
- iPhone
- sync
title: Cute idea
type: post
url: /2009/11/cute-idea/
yourls_shorturl:
- http://s.mro.name/8
---
to trigger a refresh – just pull down the list:

{{< figure  src="/wp-content/uploads/2009/11/IMG_1223.PNG" caption="IMG_1223"  width="320"  height="480" >}}

Seen in [NibiruTech Mobile RSS v1.2](http://www.nibirutech.com/product_MR.html).
