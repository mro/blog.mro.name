---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-09-24T10:04:17+00:00"
tags:
- cruft
- failure
- iOS
- iPhone
- SDK
- XCode
title: Upgrade iOS SDK 4.0 -> 4.1 with custom location
type: post
url: /2010/09/upgrade-ios-4-0-to-4-1-with-custom-location/
yourls_shorturl:
- http://s.mro.name/e
---
again, for my custom install location I need to prepare:

1. cleanly uninstall and remove cruft:

<pre class="line-numbers"><code class="language-shell-session">
$ dir=/Users/Developer.SnowLeopard
$ sudo sh $dir/Library/uninstall-devtools
$ sudo rm -r $dir/*
$ sudo mv /Developer /Developer.deleteme
</code></pre>

2. then do the custom-location install and
3. finally restore some [hotfix softlinks][1]:

<pre class="line-numbers"><code class="language-shell-session">
$ dir=/Users/Developer.SnowLeopard
$ sudo ln -s $dir/Platforms /Developer/Platforms
$ sudo ln -s $dir/SDKs /Developer/SDKs
$ sudo ln -s $dir/Applications/Xcode.app /Developer/Applications/Xcode.app
</code></pre>

Not removing the cruft will get you this quite terse error message:

{{< figure src="/wp-content/uploads/2010/09/xcode-3.2.4-install-failure.png" caption="XCode 3.2.4 install without prior cleaning" width="300" height="222" >}}

 [1]: http://blog.mro.name/2010/06/ios-4-simulator-crash-when-installed-to-custom-folder/