---
title: 'Self-healing Systems & Permacomputing'
url: /2023/10/self-healing-systems-and-permacomputing/
draft: false
categories:
- en
date: 2023-10-25T20:51:46+0200
tags:
- agency
- Atom
- cdb
- CGI
- NLnet
- OCaml
- permacomputing
- self-hosted
- Seppo
type: post
author: Marcus Rohrmoser
---

Many people wish agency and, essentially, freedom in their everyday life and
do likewise in their electronic environments. They feel uncomfortable with the vast
asymmetry as an individual in the face of billionaire-run mass platforms where
they are easily marginalised and subject to

- arbitrary service shutdowns[^1] [^2],
- being sold[^3],
- fine-print[^4] and forced rights resignation[^5] [^6],
- data breaches,
- massive energy and resource consumption[^7],
- ubiquitous surveillance[^8] or just
- enshittification[^9].

The more you have done by others, the less you control what you get and what
it takes.

At the same time, "self-hosting"[^10] isn't appealing either. Alien jargon,
emergency updates, finickey version changes and fiddling with thorny
configurations, bit rot of long forgotten dependencies – an endless list of
embarrassing problems come to mind.

Those computer systems tend to feel like a pencil balancing on it's tip and
requiring if not permanent then reoccuring attention and eventual care. The need
for action can strike any time and with unforeseeable efforts.

Neither are appealing.

But why is this? One reason may be, the systems created by experts are biased
towards also being operated by experts. Designing self-healing properties[^11]
is more effort than not and simply doesn't pay, except you happen to fancy them.
Permanent care, on the other hand, ensures the job of the caretakers. Market
logic.

## How would self-hostable systems have to be then?

Ivan Illich spoke of "Tools for Conviviality"[^12], meaning being supportive
rather than encroaching, serving rather than dominating, promising less but
be reliable in that. Being a humble means for a well respected end. There is a big
overlap with the Permacomputing Principles[^13].

Translated to technical systems, this may include

- operate unattended but be inspectable and obvious
- be gentle and lazy, don't put stress on others (humans and machines)
- sleep or hibernate at times (reliably reoccuring, intentional downtimes)
- be self-contained or at least don't depend on specific externalities (e.g. service X, vendor Y)
- reach saturation or pulsate rather than grow cancerously

## \#Seppo[^14] makes such decisions, e.g.

- choose the memory-safe, strong typed[^15], steady-moving language 🐫&nbsp;OCaml[^16]
- produce a self-contained binary[^17] bringing all assets
- be single-user: no malicious users, no parallelism
- few moving parts (no database, no runtime like php, java, python, ruby, erlang, etc.)
- be careful with dependencies, also at compile time
- nice-to-have, but optional javascript
- render in the browser[^18] and be customisable
- autonomously roll over logging and limit to 20 MB[^19]
- cdb[^20] & text[^21] files for storage and maildir[^22]-like job queue
- a (5-sec) timeout for http requests
- exponentially delayed, finite job retries[^23], in total max. 2 weeks
- provide Atom[^24] feeds you can subscribe to
- queued ActivityPub[^25] notifications

## Funding

To compete with said billionaire-run surveillance platforms grassroot volunteer
work is brave but not professionally sustainable. I am very grateful that
some NGOs[^26] and state actors give grants for the public good and complement
individual hacktivism.

Paramount, however, are netizens with an attitude of self-esteem and practical
will:

You!

## The price you pay

Seizing agency requires ownership and a certain frugality. Running a service
like #Seppo means renting webspace (or having one in your basement) and a
domain name. A simple contract with one of the numerous vendors[^27] out there.
Security updates are ideally as frequent as qmail's (none since 25 years[^28]),
so that may be no big deal.

And then you have your own home in the social web.

## 🎉

[^1]: "The books will stop working" https://web.archive.org/web/20190627025003/https://twitter.com/rdonoghue/status/1144011630197522432  
[^2]: Google Plus, Reader  
[^3]: Whatsapp, Instagram, Friendfeed, GitHub, del.icio.us  
[^4]: "To Regulate Tech, Nullify Click-Through Contracts", Communications of the ACM, September 2023, https://doi.org/10.1145/3609981  
[^5]: Instagram seizes unlimited usage rights  
[^6]: Microsoft: "hold my beer!": https://web.archive.org/web/20231022192057/https://www.microsoft.com/en-us/servicesagreement/  
[^7]: "Website Obesity Crisis", 2015, https://web.archive.org/web/20160101000048/https://idlewords.com/talks/website_obesity.htm  
[^8]: Surveillance Capitalism https://en.wikipedia.org/w/index.php?title=Surveillance_capitalism&oldid=1180308839  
[^9]: "As Platforms Decay, Let’s Put Users First" https://web.archive.org/web/20231022021215/https://www.eff.org/deeplinks/2023/04/platforms-decay-lets-put-users-first  
[^10]: https://en.wikipedia.org/w/index.php?title=Self-hosting_%28web_services%29&oldid=1174889329  
[^11]: https://en.wikipedia.org/w/index.php?title=Self-management_%28computer_science%29&oldid=1180704768  
[^12]: "Tools for Conviviality", Ivan Illich, 1973, https://archive.org/details/illich-conviviality  
[^13]: https://Permacomputing.net/Principles  
[^14]: https://Seppo.Social  
[^15]: https://en.wikipedia.org/w/index.php?title=Strong_and_weak_typing&oldid=1178852312  
[^16]: https://OCaml.org  
[^17]: https://Seppo.Social/downloads/  
[^18]: https://codeberg.org/seppo/seppo/src/commit/2c1e1b87e036f7955624ef7836fb60331c886f5c/res/static/themes/current/posts.xslt  
[^19]: https://codeberg.org/seppo/seppo/src/commit/2c1e1b87e036f7955624ef7836fb60331c886f5c/lib/logr.ml#L41  
[^20]: http://cr.yp.to/cdb/cdb.txt  
[^21]: https://opam.ocaml.org/packages/csexp/  
[^22]: http://cr.yp.to/proto/maildir.html  
[^23]: https://codeberg.org/seppo/seppo/src/commit/2c1e1b87e036f7955624ef7836fb60331c886f5c/lib/job.ml#L40  
[^24]: The Atom Syndication Format, RFC4287, https://www.rfc-editor.org/rfc/rfc4287  
[^25]: ActivityPub, W3C Recommendation 23 January 2018, https://www.w3.org/TR/activitypub/#abstract-0  
[^26]: https://nlnet.nl/project/Seppo/  
[^27]: https://duckduckgo.com/?q=hosting+shared+webspace+%252Bcgi  
[^28]: https://en.wikipedia.org/w/index.php?title=Qmail&oldid=1177401804#Security_reward_and_Georgi_Guninski's_vulnerability  
