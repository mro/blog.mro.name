---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-10-22T10:17:23+00:00"
tags:
- ImageMagick
- iPhone
- iPhone4
- make
- Makefile
- Retina Display
title: High-Res Artwork Management Automation
type: post
url: /2010/10/high-res-artwork-management-automation/
yourls_shorturl:
- http://s.mro.name/g
---
The iPhone4 comes with a [super high-res display][1] and to leverage that encourages App Developers to provide all artwork twofold – once &#8222;normal&#8220; and once in double resolution named equally with a &#8222;`@2x`&#8220; suffix.

To ease my designers' life and avoid confusion (and designers are easily confused I found) I ask them to provide the high-res artwork only and I scale it down myself. And as this is a reoccuring task, I automated via a [Makefile][2] like this:

<pre class="line-numbers"><code class="language-makefile">
#!/usr/bin/make
# Make help: http://www.gnu.org/software/make/manual/html_node/Phony-Targets.html#Phony-Targets

# Requires ImageMagick, Installation per macport: $ sudo port install imagemagick +no_x11
CONVERT :=  convert

# Where are the images?
ASSETS_DIR := .

# Which ones? All @2x.png plus twins without @2x.png
ASSETS_HIGH :=  $(wildcard $(ASSETS_DIR)/*@2x.png)
ASSETS_LOW  :=  $(patsubst %@2x.png,%.png,$(ASSETS_HIGH))

# The scaling command
%.png: %@2x.png
  convert $&lt; -resize 50% $@

assets: $(ASSETS_LOW)

clean:
  -rm $(ASSETS_LOW)
</code></pre>

See also my [link collection about high-res images][3] and my [general Xcode project setup][4].

 [1]: http://www.apple.com/iphone/features/retina-display.html
 [2]: http://en.wikipedia.org/wiki/Make_(software)
 [3]: http://wiki.mro.name/iphone/faq#iphone_4_-_doppelte_pixeldichte
 [4]: http://wiki.mro.name/iphone/xcode_project_setup#makefile
