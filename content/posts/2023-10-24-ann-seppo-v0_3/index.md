---
title: 'Announce Seppo.Social v0.3'
url: /2023/10/ann-seppo-v0_3/
draft: false
categories:
- en
date: 2023-10-24T14:01:13+0200
tags:
- ActivityPub
- Announce
- CGI
- NLnet
- OCaml
- Seppo
type: post
author: Marcus Rohrmoser
---

Hi all,

I am happy to announce the release of v0.3 of #Seppo!, the friendly, sustainable, permacomputing inspired alternative to all those heavy microblog servers. Generously funded by NLnet https://nlnet.nl/project/Seppo

It's built to be operated by yourself on commodity shared webspace without requiring much further attention.

Read more at https://seppo.social/en/support/#installation

Download: https://Seppo.Social/Linux-x86_64-0.3  
Binary:   https://Seppo.Social/Linux-x86_64-0.3/seppo.cgi  
Source:   https://Seppo.Social/Linux-x86_64-0.3/source.tar.gz  
Git:      https://Seppo.Social/v/5f078d9

Changes

- post notes
- being subscribed to (aka 'followed')
- distribute post to subscribers
- job queue to do so
- housekeeping UX (password, profile page, timezone)

Your thoughts are very much appreciated.

@social@mro.name

P.S.: announced at

- https://lists.w3.org/Archives/Public/public-swicg/2023Oct/0079.html
- https://socialhub.activitypub.rocks/t/ann-announce-seppo-social-v0-3-and-invite-thoughts/3655
- https://digitalcourage.social/@mro/111261271071034377
- https://discuss.ocaml.org/t/ann-announce-seppo-social-v0-3-and-invite-thoughts/13277
- https://lobste.rs/s/j98vdl/announce_seppo_social_v0_3_invite
- https://news.ycombinator.com/item?id=37953517
- https://we.lurk.org/archives/list/permacomputing@we.lurk.org/thread/W66VRPHUMG3SMMBP5ZUROEXOSFHEGFKJ/
- https://lists.sr.ht/~mro/seppo/%3C20231020094629.5db30044d57ea3a75a9467ab%40Rohrmoser.name%3E
