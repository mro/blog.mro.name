---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2010-03-18T21:01:57+00:00"
tags:
- Cocoa
- CocoaHeads
- iPhone
- OCUnit
- SenTestingKit
- Unit Test
- Vortrag
title: 'CocoaHeads Vortrag gestern: Unit Testing'
type: post
url: /2010/03/cocoaheads-vortrag-gestern-unit-testing/
yourls_shorturl:
- http://s.mro.name/3t
---
um ein wenig anzugeben und schlicht den Link hier zu platzieren:

* [Cocoa Unit Testing Folien][1],
* [Cocoa Unit Testing Wiki Seite][2]

 [1]: http://wiki.mro.name/_export/s5/cocoaheads/testing
 [2]: http://mro.name/go/cocoaheads_testing