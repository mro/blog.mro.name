---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2008-10-12T15:17:01+00:00"
tags:
- HCI
title: 3D Skizzen – frisch gedacht!
type: post
url: /2008/10/3d-skizzen-frisch-gedacht/
yourls_shorturl:
- http://s.mro.name/2j
---
An der Uni Toronto entstand ein [3D-Skizzerwerkzeug][1]:

Das sieht doch richtig scharf aus, oder? Bemerkenswert finde ich:

* keine Menüs,
* keine Toolsbars,
* nix – einfach ein Stift in der Hand!

Gesehen bei [Slashdot][3].

 [1]: http://www.dgp.toronto.edu/~shbae/ilovesketch.htm
 [2]: http://tech.slashdot.org/article.pl?sid=08/10/12/0228206&from=rss
 [3]: http://tech.slashdot.org/article.pl?sid=08/10/12/0228206