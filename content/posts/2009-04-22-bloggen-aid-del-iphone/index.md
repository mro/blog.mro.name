---
author: Marcus Rohrmoser
categories:
- offtopic
date: "2009-04-22T19:51:06+00:00"
title: Bloggen aid del iPhone
type: post
url: /2009/04/bloggen-aid-del-iphone/
yourls_shorturl:
- http://s.mro.name/3s
---
Eben probiere ich das Programm &#8222;BlogWriter Lite&#8220; aus. Die Autokorrektur kommt mir dabei noch vorlauter und dadaistischer vor als sonst. Aber zur Not bequemer als per Weblberfläche.