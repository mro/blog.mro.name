---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2008-10-04T20:53:20+00:00"
tags:
- EclipseME
- gentoo
- J2ME
- WTK
title: J2ME Entwicklungsumgebung + Wireless Toolkit
type: post
url: /2008/10/j2me-entwicklungsumgebung-wireless-toolkit/
yourls_shorturl:
- http://s.mro.name/13
---
wie gewohnt auf einem [gentoo][1] stable amd64 Linux.

1. JDK: 
<pre class="line-numbers"><code class="language-shell-session">$ sudo emerge dev-java/sun-jdk
</code></pre>

2. [Eclipse][2] 3.4 herunterladen und auspacken,
3. J2ME Wireless Toolkit 2.2: 
<pre class="line-numbers"><code class="language-shell-session">$ sudo su -
$ echo "dev-java/sun-j2me-bin ~amd64" >> /etc/portage/package.keywords
$ echo "dev-java/sun-j2me-bin examples" >> /etc/portage/package.use
$ emerge dev-java/sun-j2me-bin
$ # USE-Flag "j2me" setzen, z.B. via "$ sudo ufed"
$ sudo emerge proguard
</code></pre>

4. [J2ME Wireless Toolkit][3] 2.5.2 heruterladen und installieren,
5. [EclipseMe][4] herunterladen und auspacken.

 [1]: http://www.gentoo.org
 [2]: http://www.eclipse.org
 [3]: http://java.sun.com/products/sjwtoolkit/
 [4]: http://eclipseme.org/
