---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2008-12-23T12:14:04+00:00"
tags:
- Java
- JavaFX
title: 64-bit Java Plug-In Now Available
type: post
url: /2008/12/64-bit-java-plug-in-now-available/
yourls_shorturl:
- http://s.mro.name/2l
---
Got this mail from Sun recently:

-\- snip -\- snip -\- snip -\- snip -\- snip -\- snip -\- snip -\- snip -\-

Dear Java community,

You are receiving this email because you have either voted for or added a watch to one of our most wanted features in Java SE: 64-bit Java Plug-In ([Bug ID: 4802695][1]).

This is a one-time email notification, and we usually do not contact voters and watchers about bug fixes in early access releases; however, given the popularity of this feature, we have decided to spread this news: 64-bit Java Plug-In will be available in Java SE 6 Update 12.

Although 6u12 has not officially released yet, you can [download the early access version][2] and try it yourself. Send us your feedback at [http://bugreport.sun.com][3] or post your comments at [6uN Early Access Forum][4].

Thank you,
  
Java Developer Support Team
  
Sun Microsystems

-\- snip -\- snip -\- snip -\- snip -\- snip -\- snip -\- snip -\- snip -\-

The IT industry really isn't always very fast-moving.

 [1]: http://communications1.sun.com/r/c/r?2.1.3J1.2T%5f.14KkYa.CDEBYW..N.FkcA.2DjA.DXGQEYe0
 [2]: http://communications1.sun.com/r/c/r?2.1.3J1.2T%5f.14KkYa.CDEBYW..N.FkcC.2DjA.DXMEEZA0
 [3]: http://communications1.sun.com/r/c/r?2.1.3J1.2T%5f.14KkYa.CDEBYW..N.FkcE.2DjA.DXRYEZC0
 [4]: http://communications1.sun.com/r/c/r?2.1.3J1.2T%5f.14KkYa.CDEBYW..N.FkcG.2DjA.DXXMEZE0