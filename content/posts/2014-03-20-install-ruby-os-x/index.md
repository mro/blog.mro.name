---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2014-03-20T11:32:40+00:00"
tags:
- 1.8.7
- end-of-life
- Homebrew
- Mountain Lion
- OS X
- rbenv
- Ruby
title: install ruby @ OS X
type: post
url: /2014/03/install-ruby-os-x/
yourls_shorturl:
- http://mro.name/4l
---
OS X comes with a pretty hung ruby (1.8.7) until 'Mavericks'. [ruby 1.8.7 had it's planned EOL][1] long ago, even debian/stable nowadays comes with a newer one.

So if you're still running Mountain Lion or older, you may need to install ruby.

I chose [rbenv][2] and here's how I did:

* [RTFM][3]
* install rbenv: <code lang="bash">$ brew install rbenv ruby-build</code>
* add : <code lang="bash">$ brew install rbenv ruby-build</code>
* patch `.bashrc`
* do a `$ rbenv rehash`
* purge all previously installed gems: `$ gem list | cut -d ' ' -f 1 | xargs sudo gem uninstall`
* restart terminal
* install a ruby of choice, and happily `bundle install` ever after

 [1]: https://www.ruby-lang.org/en/news/2011/10/06/plans-for-1-8-7/
 [2]: http://rbenv.org
 [3]: https://github.com/sstephenson/rbenv#homebrew-on-mac-os-x