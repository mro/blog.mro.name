---
title: 'Goodbye Platforms, hello WWW!'
url: /2024/10/goodbye-platforms-hello-www/
draft: false
categories:
- en
date: 2024-10-10T11:15:17+0200
tags:
- community
- enshittification 
- Fediverse
- internet
- monopoly
- ngi
- platform
- www
type: post
author: Marcus Rohrmoser
---

At first it was a "vague but exciting…"
idea inside an international physics research lab.

## Meant to ease collaboration in scientific research.

3 1/2 decades later the www consumes almost 1/5th of global electricity, sits
in merely every pocket, is dominated by a handful of billionaire-corporations
and gives name to addictions and diseases. Often with the young, grown up
with an omnipresent www.

## How could this happen?

### 1.0

As usual in the beginning, the www first was playground for misfits (nerds or
geeks you may call them) that spent time with computers without any practical or
monetary benefits. As a hobby. So it was opinionated, awkward, charming, ugly and
brutalist. Not appealing to a broader public at all. The web 1.0.

### 2.0

The dotcom boom showed some quick money could be made with it. However the
everyday use was still limited. You had to operate it from a computer on a desk
and plug wires into the wall. Movies were seconds long, size of a thumbnail and
actually watching them required to win the codec lottery.

The situation was so miserable, that a company with a technology to play
animations and clips in various browsers became the "to xerox" for internet
clips. Flash. Macromedia was swallowed by Adobe later on. We will see that
pattern again.

But there was emerging interest with early adopters under 30 and among venture
capitalists.

And there came a company with a refreshing air of useful products that first
reinvented internet search, invented online ads, bought a video startup and made
public free maps and satellite images and gmail. Their name became the verb for
"searching on the internet". Google.

The misfits from the beginning still had their vernacular websites and some hip
businesses had one, too. But few others.

Blogging was the thing for publishing and with that RSS/Atom feeds. The golden
age of Google Reader. The web 2.0.

### Walled gardens

Then a Harvard dropout with a scent for investment money entered stage,
Facebook. This soon turned out to be the signup button to visit the shopping
mall the www became. No need to set up and maintain infrastructure individually. However,
you had to subjugate to lengthy terms and conditions where before there was just
public law and common sense. At the same time Google killed their reader and all
the journalists and news-aficionados flocked to Twitter.

Last but not least came the iPhone and within ten years time not only virtually all
phones looked like that one, but also the www - now in form of the platforms -
had become a part of everyday life. In everybody's pocket. Ubiquitous. At least
from the perspective of Central Europe, but not limited to, I suppose.

During the whole time the vendor of an operating system for desktop computers,
mostly used for accounting, business administration and email, erected a
monopoly on that sector. (IBM feared being broken up and had outsourced this)

So we have them, Google, Apple, Facebook, Amazon and Microsoft. #GAFAM (Amazon
from Seattle fancied selling books and became the monopoly for all
real-world goods and cloud computing services).

### Corporate platforms

The vast majority of www 2024 users found the corporate platforms when they
arrived. Many have never seen a blog or a website created by hand, let alone
looked under the bonnet or tinkered with one. And they do so on devices they
carry, but don't own. They have to be jailbroken to be introspected and are
without e.g. text editors to do anything the vendor hasn't envivisoned. This
majority puts equal the www and those corporate platforms.

## What is a platform after all?

In our case it's a part of the www having one owner and many
users usually not affiliated otherwise. The owner being the sole source of rules and execution
thereof, covering financial loss or benefit. Usually without a democratic
process but arbitrary, self-invented rules and policing.

They have in common a feudal asymmetry of power between the owners and the
users. The users are many but have very little agency and face arbitraryness.
The owners are few and reside behind processes. Try to get a person with power
to decide and act on the phone (let alone face-to-face) to hear appeal or resolve an issue, good luck.

Often platforms are what greed condensates to and enshittification is their
path.

## What to do about it?

Leaving the corporate platforms is inevitable. Use them if you have to, but
don't allow them an exclusive position, don't rely on them. That's
https://IndieWeb.org/POSSE. You can be on smaller islands owned or directly cared for by
their inhabitants and connected to each other. Infrastructure comes at
a cost and the business models have to be sustainable and neither surveillance-
nor consumerism- nor exploiation-backed.

Connectedness and solidarity are sentiments unfamiliar to those living in walled gardens, however it's THE
thing of the internet. A service that does one thing well is valueable once it
can be used without barriers.

Sustainability across time and communities is rigorously sensible. I haven't
seen any other way yet.

The technology mostly exists, the shift is economical and sociological - building healthy habits.

Let's call this the next generation internet.
