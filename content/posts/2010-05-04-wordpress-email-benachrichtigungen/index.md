---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2010-05-04T09:36:13+00:00"
tags:
- Email
- WordPress
title: WordPress Email Benachrichtigungen
type: post
url: /2010/05/wordpress-email-benachrichtigungen/
yourls_shorturl:
- http://s.mro.name/y
---
ojeoje, ist das so abseitig oder hab' ich's nicht verstanden? War jedenfalls komplizierter als erwartet:

1. Die Konto Einstellungen für den Email Absender müssen von Hand in die DB geschrieben werden – die 4 `mailserver_*` Zeilen in der Tabelle `wordpress_options`,
2. Absenderadresse und Nick dann noch [per Plugin][1],

fertig. Aber wieso so kompliziert?

 [1]: http://blog.kips-world.de/wp-email-absender-aendern/
