---
title: 'Lightning-Talk: Are YOU ready to sustain IT? 🌍'
url: /2019/12/sustain-it/
draft: false
categories:
#- 🇩🇪
- en
date: 2019-12-28 12:15:00+01:00
tags:
- sustain
- IT
- 36c3
- CCC
- Lightning-Talk
- Vortrag
- Nachhaltigkeit
- Degrowth
type: post
author: Marcus Rohrmoser
---

After decades of taking exponentially growing resources for granted, IT finally has
to content with zero growth.

Can you cope with that?

[📺 Video](https://txt.mro.name/36c3/36c3-lightning-talk-sustain-it_--_480p.mp4)  
[📄 Slides](https://txt.mro.name/36c3/36c3-lightning-talk-sustain-it.pdf)

http://mro.name/36c3

