---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2009-04-28T09:42:35+00:00"
tags:
- dokuwiki
- ImageMagick
- JUGM
- PDF
- piccolo2d
- Vortrag
title: S5 Präsentationen einfrieren
type: post
url: /2009/04/s5-prasentationen-einfrieren/
yourls_shorturl:
- http://s.mro.name/2p
---
So toll das [Dokuwiki Plugin S5][1] für [einfache Präsentationen wie &#8222;Piccolo2D&#8220; gestern bei der JUGM][2] ist, ohne Netzverbindung guckt man in die Röhre. Auch eine Archivierung für nachfolgende Generationen ist schwierig.

Da mir der Suchhelfer mit dem Doppel-O nichts sinnvolles ausgespuckt hat, habe ich einfach eine Serie von Screenshots der Präsentation (im Vollbildmodus) gemacht und ein wenig verkleinert (PDF Viewer scheinen Verkleinern qualitativ nicht so gut hinzukriegen) per [Imagemagick][3] in ein PDF moderater Größe konvertiert:

<pre class="line-numbers"><code class="language-shell-session">$ convert *.png -resize 1024x768 -compress LZW slides.pdf
</code></pre>

 [1]: http://www.dokuwiki.org/plugin:s5
 [2]: http://mro.name/go/jugm-p2d
 [3]: http://www.imagemagick.org/
