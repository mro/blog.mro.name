---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-06-22T20:53:02+00:00"
tags:
- Apple
- Gesture
- HCI
- iPhone
- PDF
- Mocap
- Patent
title: Markerloses Mocap
type: post
url: /2009/06/markerloses-mocap/
yourls_shorturl:
- http://s.mro.name/3w
---
Heise schreibt heute wieder mal was zum Thema: [Apple will Bewegungserkennung als
Nutzerschnittstelle patentieren lassen][1].

Wer sich für die Patentschrift interessiert, kann sich mit diesem [bash-Script][2] (benutzt
[wget][3] und [Imagemagick][4]) ein PDF bauen:

<!--more-->

<pre class="line-numbers"><code class="language-sh">#!/bin/sh
# Download the us patent application us20090153474
# and convert the single tiff images to a more handy pdf.
#
pat="us20090153474"
base="http://aiw2.uspto.gov"
file="/.DImg?Docid=${pat}ki&IDKey=47B13F807488&ImgFormat=tif"

cmd="convert"
for (( i = 1; i < = 20; i++ ))
do
  src="$base$file&PageNum=$i"
  if [[ $i -lt 10 ]]
  then
    num="0"$i
  else
    num=""$i
  fi
  dst="${pat}_$num.tiff"
  wget --output-document "$dst" "$src"

  cmd="$cmd $dst"
  if [[ $i -eq 1 ]]; then
    cmd="$cmd -rotate -90"
  fi
  if [[ $i -eq 8 ]]; then
    cmd="$cmd -rotate 90"
  fi
done
cmd="$cmd $pat.pdf"
echo "$cmd"
</code></pre>

 [1]: http://www.heise.de/newsticker/Apple-will-Bewegungserkennung-als-Nutzerschnittstelle-patentieren-lassen--/meldung/140888
 [2]: http://de.wikipedia.org/wiki/Bourne-again_shell
 [3]: http://de.wikipedia.org/wiki/Wget
 [4]: http://de.wikipedia.org/wiki/ImageMagick
