---
author: Marcus Rohrmoser
categories:
- en
- development
date: 2016-06-13 08:42:33.000000000 +02:00
tags:
- App Store
- sh
- commandline
- Image
- Inkscape
- iOS
- optipng
- pngquant
- Script
- sips
title: Render iOS App Icons from one SVG Image
type: post
url: /2016/06/render-ios-app-icons-from-one-svg-image/
---

<pre class="line-numbers"><code class="language-sh">#!/bin/sh
#
#  Copyright (c) 2015-2016 Marcus Rohrmoser http://mro.name/~me. All rights reserved.
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http: //www.gnu.org/licenses></http:>.
#
cd "$(dirname "$0")" ; mkdir build ; cd build

src="../../Spray-2.svg"

inkscape=/Applications/Inkscape.app/Contents/Resources/bin/inkscape

${inkscape} --help >/dev/null 2>&1 || { echo "Inkscape is not installed." && exit 1; }
pngquant --help >/dev/null 2>&1 || { echo "pngquant is not installed." && exit 1; }
optipng -help >/dev/null 2>&1 || { echo "optipng is not installed." && exit 1; }

pngquant="pngquant --skip-if-larger --speed 1"

OPTS="--export-area-page --export-width=1024 --export-height=1024 --export-background=white --without-gui"


dst="iTunesArtwork.png"
"${inkscape}" --export-png="$(pwd)/${dst}" $OPTS --file="$(pwd)/${src}"
${pngquant} "$(pwd)/${dst}" &

dst=iTunesArtwork.svg
cp "${src}" "${dst}"
# http://stackoverflow.com/a/10492912
${inkscape} "$(pwd)/${dst}" \
  --verb=FileVacuum --verb=FileSave \
  --verb=FileClose --verb=FileQuit
${inkscape} $OPTS --vacuum-defs --export-plain-svg="$(pwd)/${dst}" --file="$(pwd)/${dst}"

for siz in 29 40 60 76
do
  for scale in 1 2 3
  do
    dst="AppIcon-${siz}x${siz}@${scale}.png"
    sips --resampleHeightWidthMax "$(($siz * $scale))" iTunesArtwork.png --out "${dst}"
    ${pngquant} ${dst} &
  done
done

siz=167
scale=2
dst="AppIcon-83x83@${scale}.png"
sips --resampleHeightWidthMax "${siz}" iTunesArtwork.png --out "${dst}"
${pngquant} ${dst} &

wait

for i in *-fs8.png
do
  mv "${i}" "$(basename "${i}" -fs8.png).png"
done

for i in *.png
do
  optipng -o 7 "$i" &
done

wait
</code></pre>
