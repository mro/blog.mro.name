---
title: 'Scaling Infrastructure - down'
url: /2024/10/scaling-infrastructure/
draft: false
categories:
- en
date: 2024-10-28T19:59:49+0200
tags:
- permacomputing
- git
- ssh
type: post
author: Marcus Rohrmoser
---

A personal history.

In 2009, I was an indie developer doing iPhone App development and needed (or
at least thought so) git[^git] collaboration, a ticket system and a wiki.

## subversion

Before, with a Java Enterprise day job, I had my side projects on
a self-hosted subversion[^subversion] and on berlios.de[^berlios]. It was a
shallow learning curve to begin with, but now I felt for something else.

## gitolite

I started with a cheap VPS with a few GB of storage, a linux and
gitolite[^gitolite]. I could add new collaborators to gitolite's config files and they
could pull and push.

## redmine

For tickets and wikis I had a redmine[^redmine] on the same server connected to the git
repositories and watching the commit messages. Getting ruby on rails to run at all was
a hassle and every update needed some tweaks.

## github.com

Not much later github.com came along, was brand new, hip, had a simple issue
tracker and wiki and sure did I move my public stuff there. When onboarding new
collaborators, they had to sign up at github.com and got invited into the
projects. No need for me to bother with keys, passwords and resets, nice!

When that billionaire corporation I always avoided aquired the neat startup,
I was embarrassed but stayed for the moment &mdash; feeling a bit locked in.
Deleted some repositories and opened accounts at gitlab.com, notabug.org and
codeberg.org. But did not do much more for the moment.

## gogs

The shock being traded hit me hard and I decided not to depend on a single
supplier no longer.

At least if that supplier wasn't myself.

I set up a gogs[^gogs] server and copied each github.com repository there, but keept
github.com as a mirror for the time being. Parting is hard.

The next creepy github moment was, when I was added to an "arctic code vault" &mdash;
whatever that was. I was well aware what open source means, but that rubbed me
the wrong way nevertheless. Code hording.

## Distribution!

Over the next years I withdrew most repositories from github.com and kept only
those with some stargazers. Every new project started at my gogs and
got mirrors at codeberg.org, sr.ht, notabug.org and repo.or.cz. Each project's
Readme lists the mirrors. Finally there was no project left at github.com.

That felt like the way to do.

## ssh

Felt so until the VPS began to shake &mdash; my provider had technical issues and I
became aware a cheap VPS may not be the right home for my daily workflow.

I had everthing mirrored, but my host is the one I push to and the changes then
propagate to the mirrors on a post-receive-hook. So when the VPS was down, I was
sort of blocked from pushing.

The next step was to ditch personal git hosting, tickets and wikis altogether.
For communication I mostly rely on codeberg.org. The repo to push to and feed
the mirrors is connected via ssh[^ssh].

That's my current setup:

- git repositories on disc of a shared (web) host,
- connected via ssh,
- feed the mirrors via a post-receive hook,
- let collaboration happen wherever it happens.

## Learnings

Finally, I rely on git, ssh and file storage. Could have done so from the
start (ssh is around since ~1995) :-)

Use shiny services and service interfaces if you like them, but you don't have
to. Or you can use things under your control that work for ages without much
attention and go on with your worklife.

[^git]:        https://git-scm.com
[^subversion]: https://subversion.apache.org
[^berlios]:    An open-source codehoster that shut down ~2012 because code-hosting 'is done now as github exists'
[^gitolite]:   https://gitolite.com
[^redmine]:    https://www.redmine.org
[^gogs]:       https://gogs.io
[^ssh]:        https://www.openssh.com
