---
title: 'Thinkpad X250 with FreeBSD (Xfce 🐀)'
url: /2023/04/thinkpad-x250-freebsd-xfce/
draft: false
categories:
- en
- sysadmin
date: 2023-04-17T17:47:52+0200
tags:
- Thinkpad
- X250
- FreeBSD
- Xfce
- zfs
type: post
author: Marcus Rohrmoser
---

tl;dr https://codeberg.org/mro/x250-freebsd

Recently a refurbished [Thinkpad X250](https://thinkwiki.de/X250)
from [GreenPanda.de](https://GreenPanda.de)
(EUR 160,-) arrived. I plan to use it as my main machine for

- programming (🐫 ocaml)
- writing (txt, html, css)
- reading (documentation, forums etc.)
- graphics editing (gimp & inkscape)
- email, irc, messaging
- video meetings
- minetest :-)

## The wishlist

[x] network, Wifi  
[x] ocaml, helix, gitui, nnn, fortune, cowsay, fish  
[x] tidy, xmllint, xsltproc, raptor2, tinycdb, awk, dash  
[x] lynx, ssh, mosh, openssl  
[x] firefox, vlc, espeak-ng, minetest, mumble  
[x] 😀 emojis  
[x] display brightness & keys  
[x] microphone  
[x] webcam, chromium  
[x] audio volume & keys  
[x] lid close sleep/resume  
[ ] finger scanner unlock  

## Let's go

At first I got the [FreeBSD 13.2 USB image](https://download.freebsd.org/ftp/releases/ISO-IMAGES/13.2/FreeBSD-13.2-RELEASE-amd64-memstick.img.xz),
flashed a thumbdrive and installed it wiping the preinstalled useless Microsoft
and Norton software.

On installation I chose the Wifi region `APAC2 / DE` without bothering much about it.

Then came

```sh-session
$ cat /etc/wpa_supplicant.conf
ctrl_interface=/var/run/wpa_supplicant
eapol_version=2
ap_scan=1
fast_reauth=1

network={
  # example closed network
	ssid="<redacted>"
	psk="<redacted>"
	priority=3
}
network={
  # example open network
	ssid="<redacted>"
	scan_ssid=0
	key_mgmt=NONE
	priority=1
}
# more networks
# ...
$ service netif restart
```

Next

```sh-session
$ freebsd-update fetch
$ freebsd-update install
$ pkg upgrade
\# $ make -C /usr/ports/graphics/drm-510-kmod install
```

```sh-session
$ ssh-keygen -t ed25519
$ pkg install doas portmaster rsync curl fish dash tinycdb mosh nnn ncdu git gitui lynx helix stow tree htop fzf
$ cat /usr/local/etc/doas.conf
permit nopass keepenv :wheel
permit nopass keepenv root as root
$ pkg install mixertui espeak-ng
$ pkg install graphviz mscgen tidy-html5 raptor2 fusefs-sshfs fusefs-curlftpfs
```

```sh-session
$ pkg install gmake gcc ocaml-opam
$ opam init ; opam switch create 4.10.2
```

```sh-session
$ pkg install gpu-firmware-kmod drm-510-kmod
$ sysrc kld_list+=i915kms
$ pkg install xorg-minimal xfce lightdm-gtk-greeter mesa-demos
$ sysrc dbus_enable="YES"
$ sysrc lightdm_enable="YES"
$ glxinfo -B
$ glxgears
libGL error: failed to authenticate magic 1
libGL error: failed to load driver: iris
2477 frames in 5.0 seconds = 495.331 FPS
```

```sh-session
$ pkg install wpa_supplicant_gui figlet cowsay fortune-mod-freebsd-classic-g20171128
$ pkg-install dillo firefox minetest_game vlc mumble
$ pkg install twemoji-color-font-ttf-14.0.2 noto-emoji-2.034
```

Hints for hardware compatibility and 101 were in

https://unixcop.com/how-to-install-xfce-in-freebsd-13/#comment-18913,
https://forums.freebsd.org/threads/resume-from-suspend-on-thinkpad-x250.84517/
https://forums.freebsd.org/threads/boot-to-mountroot-anyway-to-rescue-it.73244/#post-446760

And *always* leave the `zfs_load="YES"` in `/boot/loader.conf` if you chose zfs
on installation.

## Rescue boot

- on boot press `F12` and choose the thumbdrive to boot from in the BIOS menu
- boot into a shell or live system  
```sh-session
# https://forums.freebsd.org/threads/boot-to-mountroot-anyway-to-rescue-it.73244/#post-446760
$ mkdir /tmp/root
$ zpool import -fR /tmp/root zroot
$ zfs mount zroot/ROOT/default
$ ...
$ zpool export zroot
$ shutdown -p now
```
