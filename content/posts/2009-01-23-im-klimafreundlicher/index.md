---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-01-23T16:31:25+00:00"
tags:
- CCC
- Jabber
- TLS
- Pidgin
title: IM klimafreundlicher
type: post
url: /2009/01/im-klimafreundlicher/
yourls_shorturl:
- http://s.mro.name/u
---
Bisher hab' ich als [Jabber][1] Server hauptsächlich [jabber.org][2] verwendet.

Da der in den USA steht, geht somit jede Statusänderung und jeder Textschnipsel über den großen Teich – das muß ja nicht unbedingt sein.

Also: Ab jetzt benutze ich verstärkt [jabber.ccc.de][3]. Allderdings galt's dem [Pidgin][4] zunächst das SSL Zertifikat zu unterzuschieben:

<pre class="line-numbers"><code class="language-shell-session">$ cd ~/.purple/certificates/x509/tls_peers
$ openssl s_client -connect jabberd.jabber.ccc.de:443 >> jabber.ccc.de
# alles vor -----BEGIN CERTIFICATE-----
# und nach -----END CERTIFICATE-----
# löschen:
$ vi jabber.ccc.de
</code></pre>

([Woher][5], [Wohin][6])

 [1]: http://de.wikipedia.org/wiki/Jabber
 [2]: http://jabber.org
 [3]: http://web.jabber.ccc.de/
 [4]: http://de.wikipedia.org/wiki/Pidgin_(Instant_Messenger)
 [5]: http://web.jabber.ccc.de/?p=44
 [6]: http://web.jabber.ccc.de/?p=29#comment-674
