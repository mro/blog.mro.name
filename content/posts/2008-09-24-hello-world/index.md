---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2008-09-24T19:59:24+00:00"
tags:
- inspire
- TED
title: Hello world!
type: post
url: /2008/09/hello-world/
yourls_shorturl:
- http://s.mro.name/3d
---

[5000 Tage](http://www.ted.com/index.php/talks/kevin_kelly_on_the_next_5_000_days_of_the_web.html)
hatte das Internet weitgehend Ruhe vor mir. Die sind jetzt vorbei.

{{<ted kevin_kelly_on_the_next_5_000_days_of_the_web >}}
