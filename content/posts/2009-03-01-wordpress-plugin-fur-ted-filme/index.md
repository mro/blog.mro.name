---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-03-01T18:55:59+00:00"
tags:
- Plugin
- PHP
- TED
title: WordPress Plugin für TED Filme
type: post
url: /2009/03/wordpress-plugin-fur-ted-filme/
yourls_shorturl:
- http://s.mro.name/14
---
Basierend auf dem [Plugin von Robert Anselm][1] experimentiere ich nachdem TED den 'embed' Code geändert hat derzeit mit folgendem Plugin Code:

<!--more-->

<pre class="line-numbers"><code class="language-php">&lt;?php
/*
Plugin Name: TEDTalks for WordPress
Plugin URI: http://www.robertanselm.com/2.0/
Description: A filter for WordPress that displays TED.com videos based on the
Youtube WordPress plugin by Joern Zaefferer. Copy and paste the TEDTalks
"Embed Video" code to a text editor and look for the .flv filename of the
flashvideo. (eg.: JOHNMAEDA-2007_high.flv ).
In WordPress type: [TEDTALKS JOHNMAEDA-2007_high.flv].
Version: 1.0
Author: Robert Anselm
Author URI: http://www.robertanselm.com/2.0/

Instructions

Copy this file you unzipped into the wp-content/plugins folder of WordPress,
then go to Administration > Plugins, it should be in the list. Activtate it
and every occurence of the expression [TEDTALKS file] will as
an embedded flash player. Replace "file" with the TEDTALKS .flv filename.
Copy and paste the TEDTalks "Embed Viedo" code to a text editor and look
for the .flv filename of the flashvideo. (eg.: JOHNMAEDA-2007_high.flv ).
In WordPress type: [TEDTALKS JOHNMAEDA-2007_high.flv].

See also the player embed swap at TED:
 http://blog.TED.com/2009/01/TEDtalks_time_f.php
 http://blog.TED.com/2009/01/TEDtalks_embed_1.php
*/

define("TEDTALKS_REGEXP", "/\[TEDTALKS ([[:print:]]+)[-_: ]([0-9]{4}[GP]?)\]/");
define("TEDTALKS_TARGET", "<object width='446' height='326'><param name='movie' value='http://video.TED.com/assets/player/swf/EmbedPlayer.swf'></param><param name='allowFullScreen' value='true' /><param name='wmode' value='transparent'></param><param name='bgColor' value='#ffffff'></param> <param name='flashvars' value='vu=http://video.TED.com/talks/embed/###TED_###-embed_high.flv&su=http://images.TED.com/images/TED/TEDindex/embed-posters/###TED-###.embed_thumbnail.jpg&vw=432&vh=240&ap=0&ti=462' /><embed src='http://video.TED.com/assets/player/swf/EmbedPlayer.swf' pluginspace='http://www.macromedia.com/go/getflashplayer' type='application/x-shockwave-flash' wmode='transparent' bgColor='#ffffff' width='446' height='326' allowFullScreen='true' flashvars='vu=http://video.TED.com/talks/embed/###TED_###-embed_high.flv&su=http://images.TED.com/images/TED/TEDindex/embed-posters/###TED-###.embed_thumbnail.jpg&vw=432&vh=240&ap=0&ti=462'></embed></object>");

function TEDtalks_plugin_callback($match)
{
  $output = TEDTALKS_TARGET;
  $speaker = $match[1];
  $year = $match[2];
  $output = str_replace("###TED-###", $speaker.'-'.$year, $output);
  $output = str_replace("###TED_###", $speaker.'_'.$year, $output);
  //if(strlen($year) > 4)
  if('P'==substr($year,4))
    $output = str_replace("-embed_high.flv", "-embed-PARTNER_high.flv", $output);
  return ($output);
}

function TEDtalks_plugin($content)
{
  return (preg_replace_callback(TEDTALKS_REGEXP, 'TEDtalks_plugin_callback', $content));
}

add_filter('the_content', 'TEDtalks_plugin');
add_filter('comment_text', 'TEDtalks_plugin');

?>
</code></pre>

Das Plugin ist ein wenig komplizierter zu erklären, aber einfacher zu benutzen (Magic!).

 [1]: http://www.robertanselm.com/2.0/?p=62
