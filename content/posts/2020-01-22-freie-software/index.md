---
title: Freie Software an freien Schulen
url: /2020/01/freie-software-an-freien-schulen/
draft: false
categories:
#- 🇩🇪
- de
date: 2020-01-22 08:08:17+01:00
tags:
- Schule
- Digitalisierung
- FOSS
type: post
author: Marcus Rohrmoser
---

Sehr viele Menschen benutzen inzwischen Software. Oft in einer Weise, ähnlich dem
Essen, dem T-Shirt-Kauf oder der Schulwahl: Man macht das, was (vermeintlich) alle
machen, macht sich wenig Gedanken (geht ja auch ohne) und hat kaum Ansprüche (weil
man wenig weiß). So geschieht auch in allen diesen Fällen das gleiche: Es bildet
sich eine Parallelwelt, in der die Dinge produziert werden. Diese optimiert sich
betriebswirtschaftlich, isoliert sich von den Menschen, bedient zunehmend
Eigeninteressen (Shareholder) und strebt nach Monopolbildung, um dann die
Abhängigkeit intensiv zu monetarisieren.

Aber es gibt immer auch Menschen, die da nicht mitmachen wollen oder können. In
radikalster Form macht man es eben selbst. Aber nicht alle sind Gärtnerin,
Schneider oder Pädagogin oder haben die nötige Zeit. Dann sucht man sich einen Weg,
der die eigenen und gesellschaftlichen Interessen vor die der Shareholder stellt.
Sehr oft sind das subsidiäre, kleinteilige und emanzipatorische Wege. Die muß man
erst mal finden, weil in der Werbung sieht man die nicht.

Bei den Schulen sind das oft sog. "freie" Schulen. Bei Software ist es "freie"
Software.

Beiden gemein ist die Bedeutung von Offenheit. So hat an der Monte z.B. das
Hospitieren eine große Bedeutung auf mehreren Ebenen. Ähnliche Folgen hat bei
Software die Offenlegung des Programmtextes. Das ist das, was Programmierer
produzieren und der Maschine zur Abarbeitung übergeben, grob vergleichbar mit einem
sehr pingeligen Kochrezept. Dann spricht man von "Open Source Software". Aber
genauso, wie durch Hospitation allein noch keine Montessori-Pädagogik entsteht,
geht "freie Software" wesentlich weiter.

Freie Software ist in Fachkreisen lange und wohl bekannt, ebenso wie auch strenge
Bio-Richtlinien oder Montessori-Pädagogik in Fachkreisen lange bekannt sind.
Außerhalb wird's schnell mager, vor allem mit der begrifflichen Trennschärfe.

Den Überlapp von Erfahrung mit (Montessori-)Pädagogik einerseits und freier
Software andrerseits schätze ich als sehr schmal ein.

Dazu möchte ich etwas beitragen.

Seit 30 Jahren besitze ich Computer, seit 20 Jahren verdiene ich meinen
Lebensunterhalt als Programmierer und seit 10 Jahren habe ich Apps im [Apple App
Store](https://apps.apple.com/de/developer/marcus-rohrmoser-mobile-software/id333210958)
und bin Mitglied u.a. in der Gesellschaft für Informatik ([gi.de](https://gi.de)),
dem Chaos Computer Club ([ccc.de](https://ccc.de)) und inzwischen noch dem Forum
InformatikerInnen für Frieden ([fiff.de](https://fiff.de)).

Seit einigen Jahren halte ich jährlich einen Blitzvortrag am Chaos Communication
Congress. Zuletzt am 36c3 über Nachhaltigkeitsaspekte von Software 1) sowie einen
kurzen O-Ton im Deutschlandfunk 2). Im Vorjahr sprach ich über die Notwendigkeit
und Wege den globalen Monopolen die Aufmerksamkeit zu entziehen 3).

Durch den Digitalpakt der Bundesregierung bekommt das Thema freie Software und
Schule gerade eine zusätzliche Dringlichkeit. Mehr Technik ist dabei nicht immer
automatisch besser 4).

Viele Grüße,  

Marcus Rohrmoser

1. https://txt.mro.name/36c3/36c3-lightning-talk-systain-it.mp4
2. https://txt.mro.name/36c3/2019-12-30-113500--36c3-dlf-oton.mp3
3. https://txt.mro.name/35c3/35c3-dissolving-gafam.mov
4. https://americanaffairsjournal.org/2019/08/rotten-stem-how-technology-corrupts-education/

