---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2011-01-27T09:39:35+00:00"
tags:
- Cocoa
- CocoaHeads
- CoreData
- Fulltext
- iPhone
- Search
- SQLite
- Vortrag
title: 'Vortrag: Index Suche mit CoreData und SQLite'
type: post
url: /2011/01/vortrag-index-suche-mit-coredata-und-sqlite/
yourls_shorturl:
- http://mro.name/43
yourls_tweeted:
- 1
---
Gestern gab's einen Mini-Vortrag von mir bei den [CocoaHeads München][1]:

* [CoreData (iOS) ist nur mit Tricks dazu zu bringen den Index bei Textsuche zu benutzen][2],
* [SQLite Full Text Search (FTS) ist der Hammer][3].

[Die Folien dazu][4].

 [1]: http://cocoaheads.org/de/Munich/
 [2]: http://wiki.mro.name/cocoaheads/binary_search#coredata
 [3]: http://wiki.mro.name/cocoaheads/binary_search#sqlite_fulltext_index
 [4]: http://wiki.mro.name/_export/s5/cocoaheads/binary_search