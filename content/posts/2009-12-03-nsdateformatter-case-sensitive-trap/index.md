---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-12-03T14:05:24+00:00"
tags:
- Cocoa
- iPhone
- NSDateFormatter
- Objective C
- OS X
- SenTestingKit
- Unicode
title: NSDateFormatter case sensitive trap
type: post
url: /2009/12/nsdateformatter-case-sensitive-trap/
yourls_shorturl:
- http://s.mro.name/3b
---
Though [NSDateFormatter behaves slightly different than documented][1], the following might even be correct, as strange as it might look (mind the last two lines):

<pre class="line-numbers"><code class="language-objc">-(void)testNSDateFormatterTrap
{
    NSDateFormatter *lower = [[[NSDateFormatter alloc] init] autorelease];
    lower.dateFormat = @"yyyy-MM-dd HH:mm:SS ZZZ";

    NSDateFormatter *upper = [[[NSDateFormatter alloc] init] autorelease];
    upper.dateFormat = @"YYYY-MM-dd HH:mm:SS ZZZ";

    lower.timeZone = upper.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];

    NSDate *d = [lower dateFromString:@"1970-01-01 00:00:00 +0000"];
    STAssertEqualObjects(@"1970-01-01 00:00:00 +0000", [lower stringFromDate:d], @"lower iso wrong");
    STAssertEqualObjects(@"1970-01-01 00:00:00 +0000", [upper stringFromDate:d], @"upper iso wrong");

    d = [d addTimeInterval:(-60*60)];

    STAssertEqualObjects(@"1969-12-31 23:00:00 +0000", [lower stringFromDate:d], @"lower iso wrong");
    STAssertEqualObjects(@"1970-12-31 23:00:00 +0000", [upper stringFromDate:d], @"upper iso wrong");
}
</code></pre>

The [Unicode Format Pattern Documentation][2] explains the difference of the upper- and lowercase year format – but frankly I don't get the &#8222;Year of week of year&#8220; idea.

But that subtracting one hour in fact **adds almost a whole year** – that's odd to me.

So I rather stay away from the uppercase form – be it correct or buggy.

Seen with iPhone SDK 3.1.2 and XCode 3.2.1 on Snow Leopard.

**Update:**

I think I got it! Uppercase YYYY makes sense only in combination with a calendar week – and not months or quarters.

Look at January 1st 2010. It belongs to calendar week 53 of 2009. Week 1 / 2010 starts on Jan 4th.

 [1]: http://blog.mro.name/2009/08/nsdateformatter-http-header/
 [2]: http://unicode.org/reports/tr35/tr35-6.html#Date_Field_Symbol_Table
