---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-07-23T22:02:38+00:00"
tags:
- bisect
- Delta Debugging
- Eclipse
- git
- piccolo2d
title: Delta Debugging / git bisect
type: post
url: /2009/07/delta-debugging-git-bisect/
yourls_shorturl:
- http://s.mro.name/2z
---
Just had the problem – which of the last 40+ commits broke a certain feature in [Piccolo2D rev550][1]. As I used [git as a svn proxy][2] before, I remembered having heard of it's binary-search [delta-debugging][3] feature.

So I started my first [git bisect][4] session:

<!--more-->

1. go back fairly far (`git checkout 9377e6a0c6e9bcdff1803f18918e8a1a9282a4ee`, Eclipse Refresh, try, fine!),
2. start bisecting (doing each step: Eclipse->Refresh, Eclipse->Project->Clean, Try it out):

    <pre class="line-numbers"><code class="language-shell-session">
    (master) $ git bisect start
    (master|BISECTING) $ git bisect bad
    (master|BISECTING) $ git bisect good 9377e6a0c6e9bcdff1803f18918e8a1a9282a4ee
    Bisecting: 47 revisions left to test after this (roughly 6 steps)
    [484ba591db63f6f476511787a5156cdaeb51889c] Fixed Issue 100; Plastered PInputEvent...
    (484ba59...|BISECTING) $ git bisect good
    Bisecting: 23 revisions left to test after this (roughly 5 steps)
    [fa515a6d0eb5ce23d3e47df5e5b4e81e94fdfa81] Added tests for PBasicInputEventHandler
    (fa515a6...|BISECTING) $ git bisect good
    Bisecting: 11 revisions left to test after this (roughly 4 steps)
    [6e496e12219346e856c8c519c29fedab3f196e35] Cleaned up PNavigationEventhandler.  R...
    (6e496e1...|BISECTING) $ git bisect bad
    Bisecting: 5 revisions left to test after this (roughly 3 steps)
    [c591c3098e6268cfc90fcb84672d642b0878a4b7] Removed some unnecessary imports from...
    (c591c30...|BISECTING) $ git bisect bad
    Bisecting: 2 revisions left to test after this (roughly 2 steps)
    [f3ca00181f2512b10bf12204395ad99f3d474f00] #84 added developers.
    (f3ca001...|BISECTING) $ git bisect good
    Bisecting: 1 revisions left to test after this (roughly 1 steps)
    [99e495ec4fc54b0f3a36ffd3b605f8a27bd86cdf] Refactored mousePressed and mouseRelease...
    (99e495e...|BISECTING) $ git bisect bad
    Bisecting: 0 revisions left to test after this (roughly 0 steps)
    [3f0c21d7cb88e696c23d4058acb708898bb60363] Just updated my contact info in the Maven...
    (3f0c21d...|BISECTING) $ git bisect good
    99e495ec4fc54b0f3a36ffd3b605f8a27bd86cdf is first bad commit
    commit 99e495ec4fc54b0f3a36ffd3b605f8a27bd86cdf
    Author:
    Date:   Mon Jul 20 15:02:56 2009 +0000

        Refactored mousePressed and mouseReleased so that code common to each is now in a hel...

        git-svn-id: https://piccolo2d.googlecode.com/svn/piccolo2d.java/trunk@524 aadc08cf-13...

    :040000 040000 b6c487cfdbe4df04947923af5fd05c45aa2ad38d 4dcd8070206a34a2d4037c3d90409ff1e...
    (3f0c21d...|BISECTING) $ git bisect reset
    Previous HEAD position was 3f0c21d... Just updated my contact info in the Maven Config File.
    Switched to branch 'master'
    </code></pre>
3. done, got the fishy commit! (524)

P.S.: I cut the long lines, git spits them out in full, naturally.

 [1]: http://code.google.com/p/piccolo2d/source/detail?r=550
 [2]: http://www.kernel.org/pub/software/scm/git/docs/git-svn.html
 [3]: http://en.wikipedia.org/wiki/Delta_Debugging
 [4]: http://www.kernel.org/pub/software/scm/git/docs/git-bisect.html
