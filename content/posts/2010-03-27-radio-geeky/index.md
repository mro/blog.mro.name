---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2010-03-27T12:05:02+00:00"
tags:
- Bayern2
- cron
- Debian
- github
- iTunes
- Linux
- Mitschnitt
- Podcast
- Radio
- RSS
- Ruby
- scrAPI
- Screen Scraping
- SQLite
- streamripper
- Zündfunk
title: Radio Geeky
type: post
url: /2010/03/radio-geeky/
yourls_shorturl:
- http://s.mro.name/3k
---
wie schon in einem [älteren Artikel][1] angerissen möchte ich zeitgesteuert Radiosendungen aufnehmen – und zwar auf einem Rechner (mit Apache Webserver), der rund um die Uhr läuft.

Nun habe ich kürzlich einem großen [Zündfunk][2]-Fan, der selten Zeit hat ihn zu hören, diesen Mitschnitt als [Podcast][3] zum Geburtstag geschenkt. Das ganze wurde aufwändiger als ich dachte und setzt sich mittlerweile zusammen aus:

1. [Screen Scraping][4] der [B2 Programmwebsite][5] mit [scrAPI][6] – incl. Datumskorrektur weil das Programm nicht um Mitternacht umbricht sondern um 5 Uhr früh,
2. diese Daten in einer [SQLite][7] DB [cachen][8] um nicht mehrmals am Tag zu scrapen (wenn man z.B. noch andere Sendungen aufnimmt),
3. per [cron][9] einige Minuten vor Sendungsbeginn den [streamripper][10] starten ([v1.62.3][11] wegen alter [glib][12] auf [Debian Linux][13]), 
  * da der einige Zeit zum Starten braucht triggere ich Sendungsbeginn und -ende zeitlich exakt über ein [Script, das Titelwechsel in den laufenden Streamripper injiziert][14] – der B2 Stream liefert selbst keine Sendungsinfos,
4. falls der Stream zwischendurch unterbrochen wurde setzen wir die verschiedenen [mp3][15] Dateien zu einer zusammen (ohne Überblendung per Holzhammer: [cat][16])
5. Sendungsinfos parallel zum mp3 als [xml][17] speichern, um nach Aufnahmeende von der DB unabhängig zu sein,
6. aus allen vorhandenen mp3+xml Paaren ein [Podcast RSS][18] bauen und mit [gzip][19] packen,
7. tägliche Gleitlöschung (mp3 älter als 3 Wochen) per cron+[find][20] damit die Platte nicht volläuft.

Den Podcast abonniert man schließlich z.B. mit [iTunes][21] und bekommt direkt nach der Sendung die frische Zündfunkfolge incl. komplettem Text zur Sendung frei Haus.

Das komplette [Ruby Script gibts bei github][22].

 [1]: http://blog.mro.name/2009/12/radiomitschnitt-per-rezept/
 [2]: http://www.br-online.de/bayern2/zuendfunk/
 [3]: http://de.wikipedia.org/wiki/Podcast
 [4]: http://www.rechtzweinull.de/index.php?/archives/100-Screen-Scraping-Wann-ist-das-Auslesen-und-die-Veroeffentlichung-fremder-Daten-zulaessig.html
 [5]: http://www.br-online.de/br/jsp/global/funktion/programmvorschau/programmfahne.jsp?programm=B2
 [6]: http://blog.labnotes.org/tag/scrapi/
 [7]: http://www.sqlite.org/
 [8]: http://de.wikipedia.org/wiki/Cache
 [9]: http://de.wikipedia.org/wiki/Cron
 [10]: http://de.wikipedia.org/wiki/Streamripper
 [11]: http://sourceforge.net/projects/streamripper/files/streamripper%20%28older%20unix%29/1.62.3/streamripper-1.62.3.tar.gz/download
 [12]: http://de.wikipedia.org/wiki/GLib
 [13]: http://de.wikipedia.org/wiki/Debian
 [14]: http://code.google.com/p/xstreamripper/source/browse/trunk/streamripper/fetch_external_metadata.pl
 [15]: http://de.wikipedia.org/wiki/MP3
 [16]: http://en.wikipedia.org/wiki/Cat_(Unix)
 [17]: http://de.wikipedia.org/wiki/Extensible_Markup_Language
 [18]: http://de.wikipedia.org/wiki/RSS
 [19]: http://www.gnu.org/software/gzip/
 [20]: http://de.wikipedia.org/wiki/Find
 [21]: http://de.wikipedia.org/wiki/ITunes
 [22]: http://gist.github.com/345983