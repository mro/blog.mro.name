---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2009-03-30T20:19:44+00:00"
tags:
- BKA
- Sicherheit
- Zensur
title: Zensur, grundsätzlich betrachtet
type: post
url: /2009/03/zensur-grundsatzlich-betrachtet/
yourls_shorturl:
- http://s.mro.name/3l
---
Das aktuelle Thema Internetzensur finde ich extrem spannend. Einerseits heißt's ganz klar in [Artikel 5 GG: &#8222;Eine Zensur findet nicht statt&#8220;][1].

Mit [Zensur wiederum ist laut Wikipedia][2] das Kontrollieren oder Unterdrücken unerwünschter Inhalte gemeint.

Damit wäre der Fall eigentlich klar: Der von-der-Leyen-Schäublesche Internetfilter ist verfassungswidrig.

<!--more-->Andrerseits ist laut 

[§184d StGB][3] und [§184b StGB][4] auch die Verbreitung &#8222;durch Rundfunk, Medien- oder Teledienste&#8220; voll strafbar.

Allerdings scheint das [INet Access Provider][5] nicht zu betreffen – ansonsten wären die weit öfter strafrechtlich dran. Oder steht typischerweise in den (sämtlichen?) AGBs, daß sich der Kunde verpflichtet keine illegalen Inhalte zu transportieren und der Provider würde dann quasi [arglistig getäuscht][6] und wäre schuldlos?

Also – was tun? Was sind die vorrangigen Ziele? Dürfen durch Verfolgung der Konsumenten Behördenmittel gebunden werden, die bei der Verfolgung der Produzenten evtl. fehlen?

Ehrlich gesagt empfinde ich eine aktive Beschäftigung der Behörden mit Konsumenten als Zeitverschwendung und würde aber gegen Produzenten und Anbieter mit voller Härte aktiv vorgehen lassen. Falls jemand als Konsument auffällt gibt's auch saures.

Ein Meldesystem könnte die Behörden unterstützen – allerdings wenn das [BKA auf so lustige Ideen kommt, aus dem Besuch Ihrer Website einen Verdacht zu konstruieren][7] ist das natürlich leider wieder nix.

Soviel wenn der Anbieter unserer [Jurisdiktion][8] unterliegt. Was aber wenn nicht? Ist ein &#8222;blöd, dann können wir nichts machen&#8220; akzeptabel? Ja evtl. die einzig mögliche, verfassungsgemäße Reaktion? Auch im Sinne eines [&#8222;wehret den Anfängen&#8220;][9] um einer Verfassungserosion vorzubeugen. Richtig schön ist das nicht – aber an der Verfassung zu zündeln ist auch alles andere als schön. &#8222;[They who can give up essential liberty to obtain a little temporary safety, deserve neither liberty nor safety.][10]&#8220;

Es bleibt schwierig.

 [1]: http://www.bundestag.de/parlament/funktion/gesetze/grundgesetz/gg_01.html#005
 [2]: http://de.wikipedia.org/wiki/Zensur_%28Informationskontrolle%29
 [3]: http://www.gesetze-im-internet.de/stgb/__184d.html
 [4]: http://www.gesetze-im-internet.de/stgb/__184b.html
 [5]: http://de.wikipedia.org/wiki/Access-Provider#Internetzugang_.28Konnektivit.C3.A4t.2C_Verbindung_zum_Internet.29
 [6]: http://de.wikipedia.org/wiki/Arglistige_T%C3%A4uschung
 [7]: http://blog.mro.name/2009/03/bka-als-honeypot/
 [8]: http://de.wikipedia.org/wiki/%C3%96rtliche_Zust%C3%A4ndigkeit
 [9]: http://www.lateinservice.de/anthologia/2001/prosestio.htm
 [10]: http://en.wikiquote.org/wiki/Benjamin_Franklin