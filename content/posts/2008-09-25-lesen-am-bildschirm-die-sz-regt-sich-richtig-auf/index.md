---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2008-09-25T15:08:55+00:00"
tags:
- HCI
title: Lesen am Bildschirm – die SZ regt sich richtig auf
type: post
url: /2008/09/lesen-am-bildschirm-die-sz-regt-sich-richtig-auf/
yourls_shorturl:
- http://s.mro.name/3n
---
Im Artikel [Lesen am Bildschirm – Der freie Fall der Seh-Linie][1] mokiert die SZ u.a., daß [Jakob
Nielsen](http://de.wikipedia.org/wiki/Jakob_Nielsen) in zwei verschiedenen Studien zum gleichen
Ergebnis kommt. Wo ist das Problem?

 [1]: http://www.sueddeutsche.de/kultur/141/311065/text/