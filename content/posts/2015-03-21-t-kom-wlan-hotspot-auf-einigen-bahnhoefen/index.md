---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2015-03-21T07:42:21+00:00"
tags:
- Bahn
- DB
- T-Kom
- WLAN
title: T-Kom WLAN Hotspot auf einigen Bahnhöfen…
type: post
url: /2015/03/t-kom-wlan-hotspot-auf-einigen-bahnhoefen/
yourls_fetching:
- 1
---
Nehmen wir mal kurz an, man ist T-Kom Kunde, steht im Zug auf dem Bahngleis und möchte per WLAN in's Netz.

DB [WLAN im Bahnhof: Täglich 30 Minuten kostenlos][1] sagt:

> 30 Minuten gratis WLAN an über 120 Bahnhöfen
>
> Das nennt man Service: Mit diesem innovativen und kundenorientierten Servicekonzept geben wir dem Warten eine neue Qualität.
>
> Nutzen Sie Ihre Reisezeit noch effizienter: In über 120 Bahnhöfen bieten wir Ihnen einen Zugang zum Internet über das WLAN der Telekom. Jeden Tag sind die ersten 30 Minuten WLAN kostenlos, danach kann zu Telekom-Tarifen weiter gesurft werden.
>
> An vielen Bahnhöfen können Sie WLAN nutzen. Mit dem Service von Telekom und der Deutschen Bahn surfen Sie problemlos mit Ihrem WLAN-fähigen Smartphone oder Tablet kabellos im Internet. Überall, wo Sie nebenstehendes blaues Piktogramm sehen, können Sie online gehen.

Die folgende Anleitung hat 8(!) Schritte.

Und so sieht das in der Praxis aus (obwohl die T-Kom noch nicht mal dem Risiko der [Störerhaftung][2] ausgesetzt ist):

<!--more-->

{{< figure  src="/wp-content/uploads/2015/03/IMG_7561.png" caption=""  width="320"  height="568" >}}
{{< figure  src="/wp-content/uploads/2015/03/IMG_75621.png" caption=""  width="320"  height="568" >}}
{{< figure  src="/wp-content/uploads/2015/03/IMG_7563.png" caption=""  width="320"  height="568" >}}

oder auch nicht. Ohne SMS Empfang leider kein WLAN.

Man darf raten was länger dauert:

* sich durch die Anmeldeprozedur klicken,
* die Nutzungsbedingungen lesen,
* wirklich Surfen.

P.S.: Und wie soll das eigentlich gehen – Nutzungsbedingungen lesen _**bevor**_ man WLAN bekommt?

 [1]: http://www.bahn.de/p/view/service/bahnhof/railnet_bahnhof.shtml
 [2]: https://digitalegesellschaft.de/portfolio-items/storerhaftung-beseitigen/
 [3]: http://blog.mro.name/wp-content/uploads/2015/03/IMG_7561.png
 [4]: http://blog.mro.name/wp-content/uploads/2015/03/IMG_75621.png
 [5]: http://blog.mro.name/wp-content/uploads/2015/03/IMG_7563.png