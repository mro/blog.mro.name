---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2009-11-28T21:58:42+00:00"
tags:
- git
- Graphviz
- iPhone
- MacPorts
- OS X
- SANE
- SenTestingKit
- Snow Leopard
- Twain
- XCode
title: Upgrade to Snow Leopard
type: post
url: /2009/11/upgrade-to-snow-leopard/
yourls_shorturl:
- http://s.mro.name/36
---
1. Mac OS X 10.6 Snow Leopard (partition 25G, HFS+ Journaled, Upper/Lower)
2. Mac OS X 10.5 Leopard (partition 25G, HFS+ Journaled, Upper/Lower)
3. partition userspace 100G, HFS+ Journaled, Upper/Lower

    <pre class="line-numbers"><code class="language-shell-session">
    $ cat /etc/fstab
    # mount partition "userspace" as /Users
    UUID=D016E3FD-E322-3006-A8F5-D2348C6A5B7B /Users  hfs rw,auto
    </code></pre>

4. create user &#8222;mig&#8220;
5. TimeMachine restore Users + Settings
6. delete user &#8222;mig&#8220;
7. manually copy user &#8222;Shared&#8220;
8. iPhone SDK 3.1.2 + XCode 3.2.1
9. [modgenerator 1.5][1] plus [trick][2]

    <pre class="line-numbers"><code class="language-shell-session">
    $ ln -s /Developer/usr/bin/momc /Developer/Library/Xcode/Plug-ins/XDCoreDataModel.xdplugin/Contents/Resources/momc
    </code></pre>

10. [git 1.6.5.2][3], manually add to `.bash_profile`:

    <pre class="line-numbers"><code class="language-shell-session">
    export PATH=$PATH:/usr/local/git/bin
    </code></pre>

11. [Macports][4] / http://trac.macports.org/wiki/Migration

    <pre class="line-numbers"><code class="language-shell-session">
    $ sudo port selfupdate
    $ sudo port install bcpp pwgen wget lftp fortune optipng graphviz ragel imagemagick
    $ sudo port clean --all installed
    $ sudo port -f uninstall inactive
    </code></pre>

12. [graphviz-2.25.20091129.0545.pkg][5]
13. [Textwrangler 3.0 (2538)][6]
14. [Gimp 2.6.7][7]
15. [NeoOffice 3.0.1][8]
16. [Evernote 1.5.2 (62233)][9]
17. [TrueCrypt 6.3a][10]
18. [Dropbox v0.6.570][11]
19. [Skype 2.8.0.722][12]
20. [Firefox 3.5,5][13]
21. [Miro Video Player 2.5.3 (775f9134)][14]
22. [wxMaxima-0.8.3 & Maxima-5.19.2.dmg][15]
23. [Twain SANE Scanner Support][16]

Remaining issues:

1. SenTestingKit error highlighting not working.

 [1]: http://rentzsch.com/code/mogenerator_v1.5
 [2]: http://aralbalkan.com/2152
 [3]: http://code.google.com/p/git-osx-installer/
 [4]: http://www.macports.org/install.php
 [5]: http://www.graphviz.org/Download_macos.php
 [6]: http://www.barebones.com/products/TextWrangler/
 [7]: http://gimp.lisanet.de/Website/Download.html
 [8]: http://www.neooffice.org/neojava/de/mirrors.php?file=NeoOffice-3.0-Intel.dmg
 [9]: http://www.evernote.com/about/intl/de/download/
 [10]: http://www.truecrypt.org/downloads
 [11]: http://www.dropbox.com/
 [12]: http://skype.com/
 [13]: http://www.mozilla-europe.org/de/firefox/
 [14]: http://www.getmiro.com/
 [15]: http://sourceforge.net/projects/maxima/files/
 [16]: http://blog.mro.name/2008/12/scanner-mustek-1200-cu-unter-mac-os-x-105/
