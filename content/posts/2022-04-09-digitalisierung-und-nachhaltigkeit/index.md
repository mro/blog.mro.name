---
title: 'Vortrag: Digitalisierung&nbsp;und&nbsp;Nachhaltigkeit 🌍'
url: /2022/04/digitalisierung-und-nachhaltigkeit/
draft: false
categories:
#- 🇩🇪
- de
date: 2022-04-09 17:17:17+02:00
tags:
- Digitalisierung
- Nachhaltigkeit
- Degrowth
- Vortrag
- Q3
- Traunstein
- CMW
type: post
author: Marcus Rohrmoser
---

> Das Internet wird Schätzungen zufolge im Jahr 2025 bis zu 20% des weltweiten
> Stromverbrauchs und über 5% der CO2 Emissionen verursachen. Im Impulsvortrag
> wird es zunächst um die Kernidee der Nachhaltigkeit gehen, um anschließend auf
> verschiedene Ansätze sowie auf das Problem der exponentiell wachsenden
> IT-Landschaft auf einem begrenzten Planeten einzugehen. Zuletzt wird es
> Orientierungshilfen für den eigenen IT-Alltag und die Vorstellung von
> Alternativtools jenseits der Globalkonzerne geben.
>
> [Programm Chiemgauer Medienwochen, Di, 29. März
> 2022](https://web.archive.org/web/20220409161237/https://chiemgauer-medienwochen.de/programm/#fancy-title-76)

<div align="center">
Jede/r<br/>
Selber<br/>
Jetzt
</div>

[Die Folien gibt's auch](http://marcus.rohrmoser.name/cmw22).

Siehe auch ["Can you sustain IT?" (2019)](http://mro.name/36c3).
