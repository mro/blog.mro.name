---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2008-12-29T22:30:54+00:00"
tags:
- Apple
- Gesture
- HCI
- iPhone
- Patent
- Touchscreen
title: Apple Patent Application „Swipe Gestures for Touch Screen Keyboards „
type: post
url: /2008/12/apple-patent-application-swipe-gestures-for-touch-screen-keyboards/
yourls_shorturl:
- http://s.mro.name/2s
---
E.g. [golem][1] reports a new patent application concerning touch screens.

As the [US patent office][2] website does only provide somewhat hard to handle TIFFs, I provided a
bash script to download and convert them into one single pdf: [Download and pdfify US Patent
Application 20080316183][3].

 [1]: http://www.golem.de/0812/64310.html
 [2]: http://appft1.uspto.gov/netacgi/nph-Parser?Sect1=PTO2&Sect2=HITOFF&p=1&u=%2Fnetahtml%2FPTO%2Fsearch-bool.html&r=1&f=G&l=50&co1=AND&d=PG01&s1=20080316183&OS=20080316183&RS=20080316183
 [3]: http://wiki.mro.name/playground/us_patent_20080316183