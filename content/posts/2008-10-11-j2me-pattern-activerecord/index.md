---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2008-10-10T22:05:11+00:00"
tags:
- ActiveRecord
- J2ME
- pattern
title: 'J2ME Pattern: ActiveRecord'
type: post
url: /2008/10/j2me-pattern-activerecord/
yourls_shorturl:
- http://s.mro.name/2f
---
Though [J2ME][1] forces you to be close with adding classes, type-safe & convenient persistence might be something you don't want to miss.

And as [RecordStore][2]s are the primary application persistence stores, there's some boilerplate code you can refactor out into one single common base class.

Also – especially from a pattern standpoint – it's a must and increases the unterstandability and maintainability of your code.

So what's the design assumptions and decisions?

1. multiple RecordStores can be kept open at least as long as an application is active. So open them in [startApp()][3] and close them in [pauseApp()][4] and [destroyApp()][5],
2. use the [ActiveRecord][6] pattern rather than [ValueBean][7]/[DAO][8] to keep the number of classes small,
3. never access RecordStores directly but shield them with ActiveRecords,
4. keep track of the ActiveRecord instances and update them via [RecordListener][9]s,
5. provide getBytes/setBytes instance methods to the ActiveRecords,
6. maybe make ActiveRecords even [RecordListener][9] [Singleton][10]s and refill the data on usage,
7. maybe even use a single Hashtables subclass as ActiveRecords to even save more classes (requires very disciplined unit testing!),
8. create few multi-purpose [RecordFilter][11] and [RecordComparator][12] subclasses per RecordStore,

Check back later for sample code.

Or just don't code persistence manually but use the [floggy J2ME persistence framework][13]. Looks interesting, but I didn't try it out yet.

 [1]: http://java.sun.com/javame/
 [2]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/rms/RecordStore.html
 [3]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/midlet/MIDlet.html#startApp()
 [4]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/midlet/MIDlet.html#pauseApp()
 [5]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/midlet/MIDlet.html#destroyApp(boolean)
 [6]: http://martinfowler.com/eaaCatalog/activeRecord.html
 [7]: http://java.sun.com/blueprints/corej2eepatterns/Patterns/TransferObject.html
 [8]: http://java.sun.com/blueprints/corej2eepatterns/Patterns/DataAccessObject.html
 [9]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/rms/RecordListener.html
 [10]: http://en.wikipedia.org/wiki/Singleton_pattern
 [11]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/rms/RecordFilter.html
 [12]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/rms/RecordComparator.html
 [13]: http://floggy.sourceforge.net/