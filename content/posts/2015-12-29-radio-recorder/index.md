---
title: 'Lightning-Talk: Internet Radio Recorder'
url: /2015/12/radio-recorder/
draft: false
categories:
#- 🇩🇪
- en
date: 2015-12-29 11:05:00+01:00
tags:
- 32c3
- CCC
- Lightning-Talk
- Vortrag
- Radio
- Recorder
type: post
author: Marcus Rohrmoser
---

“… Aus urheberrechtlichen Gründen sind nur ausgewählte Sendungen und Beiträge
als Podcast verfügbar …”


[📺 Video](https://txt.mro.name/32c3/32c3-nur-ausgewaehlte-sendungen-als-podcast_--_480p.mp4)  
[📄 Slides](https://txt.mro.name/32c3/32c3-nur-ausgewaehlte-sendungen-als-podcast.pdf)

http://mro.name/32c3

