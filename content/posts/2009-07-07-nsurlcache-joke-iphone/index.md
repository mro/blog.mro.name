---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-07-07T12:08:18+00:00"
tags:
- cache
- Cocoa
- iPhone
- NSURLCache
- Objective C
title: NSURLCache Joke / iPhone
type: post
url: /2009/07/nsurlcache-joke-iphone/
yourls_shorturl:
- http://s.mro.name/1n
---
Did you ever wonder why Apple's own Demo App [URLCache][1] doesn't use the [NSURLCache][2] class, but rather reimplements disk caching instead? Well, it looks like NSURLCache promises disk-caching, but doesn't keep this promise.

 [1]: https://developer.apple.com/iphone/library/samplecode/URLCache/index.html
 [2]: http://developer.apple.com/iphone/library/documentation/Cocoa/Reference/Foundation/Classes/NSURLCache_Class/Reference/Reference.html