---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-06-02T17:59:19+00:00"
tags:
- git
- gitosis
- Hosting
- vServer
title: billiger vServer als git Server
type: post
url: /2009/06/billiger-vserver-als-git-server/
yourls_shorturl:
- http://s.mro.name/i
---
An [git][1] finde ich u.a. großartig, daß es so unkompliziert zu hosten ist (auf einem lokalen Filesystem, auf [http (readonly), ftp oder webdav Shares][2], per [gitosis (ssh getunnelt)][3], als [eigener (readonly) Dienst][4] oder ganz [ohne Server via Mail][5]).

<!--more-->

Ich benutze einen [10 EUR vServer][6] mit relativ wenig RAM als (gitosis) Server. Nun habe ich doch einige Repositories mit umfänglicher Geschichte oder z.T. riesigen Dateien, so daß mancher

<pre class="line-numbers"><code class="language-shell-session">$ git clone <repo-url>
</code></pre>

speicherbedingt die Grätsche macht. [Folgende Einträge][7] in der serverseitigen `.git/config` bzw. `~/.gitconfig` schaffen Abhilfe:

<pre class="line-numbers"><code class="language-git"># http://kerneltrap.org/mailarchive/git/2008/8/11/2889364
[core]
    packedGitWindowSize = 16m
    packedGitLimit = 64m
[pack]
    threads = 1
    windowMemory = 64m
    deltaCacheSize = 1m
</code></pre>

 [1]: http://git-scm.com/
 [2]: http://www.kernel.org/pub/software/scm/git/docs/git-clone.html#URLS
 [3]: http://scie.nti.st/2007/11/14/hosting-git-repositories-the-easy-and-secure-way
 [4]: http://www.kernel.org/pub/software/scm/git-core/docs/git-daemon.html
 [5]: http://book.git-scm.com/5_git_and_email.html
 [6]: https://www.campusspeicher.de/?page=c2VydmVy
 [7]: http://kerneltrap.org/mailarchive/git/2008/8/11/2889364
