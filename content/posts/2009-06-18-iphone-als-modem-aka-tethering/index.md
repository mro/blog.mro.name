---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-06-18T15:17:55+00:00"
tags:
- iPhone
- Modem
- Tethering
title: iPhone als Modem a.k.a. Tethering
type: post
url: /2009/06/iphone-als-modem-aka-tethering/
yourls_shorturl:
- http://s.mro.name/3u
---
OS 3.0 und unsere Freunde aus Österreich machen's möglich:

{{< figure  src="/wp-content/uploads/2009/06/img_0667.png" caption="" width="200" >}}
{{< figure  src="/wp-content/uploads/2009/06/img_0666.png" caption="" width="200" >}}

<!--more-->

1. mit dem iPhone <http://help.benm.at> ansteuern,
2. das passende Profil aussuchen,
3. &#8222;Internet Tethering&#8220; in &#8222;Einstellungen -> Allgemein -> Netzwerk&#8220; einschalten
{{< figure  src="/wp-content/uploads/2009/06/img_0664.png" caption="" width="200" >}}
{{< figure  src="/wp-content/uploads/2009/06/img_0665.png" caption="" width="200" >}}
4. iPhone per USB ankabeln,
5. im aufpoppenden Netzwerkeinstellungsdialog &#8222;Anwenden&#8220;
{{< figure  src="/wp-content/uploads/2009/06/tethering-macbook.png" caption="Tethering / MacBook" width="400" >}}
6. fertig:
{{< figure  src="/wp-content/uploads/2009/06/tethering.png" caption="Tethering" width="150" >}}

So geht's zumindest technisch – ob das zu den Vertragsbedingungen paßt ist [eher unwahrscheinlich][1] und muß jeder in seinem Vertrag selbst nachgucken.

 [1]: http://www.apfelnews.eu/2009/03/19/iphone-os-30-tethering-iphone-als-modem-via-bluetooth-oder-usb/