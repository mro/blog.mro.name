---
title: Des Kaisers neue App 📱
url: /2020/05/des_kaisers_neue_app/
draft: false
categories:
#- 🇩🇪
- de
date: 2020-05-26 11:39:17+02:00
tags:
- App
- Magie
- Aufklärung
- Digitalisierung
- Zauberlehrling
- intuitiv
- Covid19
type: post
author: Marcus Rohrmoser
---

> … Das sind die Mittel des Mittelalters auch im Jahr 2020, wo uns eigentlich
> smartere Instrumente durch die Digitalisierung zur Verfügung stehen. Hier haben
> wir die dringende Frage an die Regierung: Wo sind die Apps? Wo ist mindestens die
> Tracing-App, die wir brauchen, um Infektionsketten nachzuverfolgen? Die
> digitalen Defizite Deutschlands kosten uns Gesundheit, Wohlstand und Freiheit,
> und das ist nicht länger hinnehmbar. 
>  
> Deutscher Bundestag, Plenarsaal, 23. April 2020, 9:57 Uhr  
> 📄 https://dipbt.bundestag.de/dip21/btp/19/19156.pdf#page=19  
> 📺 https://dbtg.tv/cvid/7441882 ~08:00

aber

> Does anybody think this will do something useful? ... This is just
> something governments want to do for the hell of it. To me, it's just techies
> doing techie things because they don't know what else to do
>  
> https://www.schneier.com/blog/archives/2020/05/me\_on\_covad-19\_.html

Hilft eine App gegen pandemische Virusausbreitung? Möglicherweise nicht im
eigentlichen Sinn, aber dennoch auf mancherlei andere Art.

## Technik versteckt Verantwortung

Das schöne an einer technischen Lösung ist, sie ist Aufgabe der Techniker. Damit
kann man zwar als Laie (z.B. FDP Fuzzi) noch eine Meinung haben, aber man ist aus
der Pflicht. Und wenn die Techniker _richtige_ Techniker sind, finden sie aus
Laiensicht die _eine_ objektiv _optimale_ Lösung.

Besser geht's nicht. Alternativlos, oder?

Daß diese Lösung weder auffindbar (niemand ist allwissend und kann alle Faktoren
wägen) noch gangbar wäre (sie betrifft alle und muß breiter entschieden werden),
wird schlicht verdrängt.

## Sie schafft (eigene) Entspannung

Ist die Verantwortung erstmal delegiert und man selbst nicht mehr in der Pflicht,
fällt auch jeglicher Druck irgendetwas zu tun weg. Schön, weil dann kann man selbst
ein Stück zur Normalität zurückkehren und mit voller Intensität PR betreiben.

## "Ich bin innovativ!"

Die Forderung nach Apps bedient außerdem die Reaktions-Reflexe und zeigt einmal
mehr, wer Teil der innovativen, technisierten Zukunft sein will und wer nicht. Es
ist dies das Technikversprechen der Nachkriegsjahrzehnte des 20. Jahrhunderts. Es
ist die Traumwelt, die mit Penicilin und Röntgen vielversprechend begann, über
Nukleareuphorie, Asbest, Contergan, FCKW, Glyphosat, Microplastik im Fisch und PCB
im Eisbärfett ihren Glanz verlor und schließlich zum fossilen Kohlenstoff-Erwachen
führte.

Angesichts des globalen Konsuminfarkts die Rettung von einer Branche zu erwarten,
die sich quasi seit Anbeginn auf exponentiellem Wachstum ihres Resourcenverbrauchs
ausruht (Moore's Law) ist beschämend ignorant.

## It's magic!

Da die moderne Informationstechnik und insbesondere (Smartphone-) Apps in Ihrer
Wirkungsweise der breiten Masse ähnlich rätselhaft sein dürften, wie chemische
Prozesse den Menschen des Mittelalters, schlüpft der App-Lieferant in die Rolle des
Alchemisten und verspricht Blei zu Gold zu machen. An den Erfolg mag er (oder sie)
selbst glauben, eintreten wird er nicht.

Wichtig ist denn auch weniger der Erfolg, als vielmehr das heiße Bemühen ---
sichtbar an monströsen Budgets, riesigen Teams und aufwändiger Maschinerie. Die
besten Alchemisten sind natürlich die teuersten. Und umgekehrt. ("Nobody got fired
buying IBM")

Zu Pass kommt dabei die vermeintlich "intuitive Bedienung", da so nicht erklärt
werden muß, was oft genug an Banalität und Obszönität unter der glänzenden
Oberfläche steckt. Wer die vermeintlich kinderleichte Bedienung nicht meistert, ist
einfach zu dumm, jeder Erklärungsversuch Zeitverschwendung.

So bleiben die Wirkmechanismen esoterisch und Einblicke einer Priesterkaste
vorbehalten. Es bleibt verborgen, daß auch die Alchemisten selbst oftmals nicht
verstehen, was sie da tun und phänomenologisch Reagenzien kombinieren (try
and error, "Our software dependency problem"[^cox2019], "Gedanken zur Software
Explosion"[^wirth1994]).

Die breite Masse der "User" verbleibt längst in selbstverschuldeter Unmündigkeit
und unternimmt gar nicht erst den Versuch Transparenz einzufordern oder Abläufe zu
verstehen, sondern begnügt sich mit reinem Konsum. Die "Intuitivität" der Bedienung
macht ein Verstehenwollen der Funktionsweise zum schrulligen Spielverderberwunsch.

Es blinkt aber auch zu schön.

## Fazit

Es geht ganz postfaktisch nicht um die Realität, sondern um Glauben.

So kann das Heilsversprechen unhinterfragt bestehen bleiben.

[^cox2019]:   Russ Cox, 2019, https://research.swtch.com/deps
[^wirth1994]: Niklaus Wirth, 1994, https://dl.gi.de/handle/20.500.12116/15910 bzw. https://cr.yp.to/bib/1995/wirth.pdf

## Über den Autor

Marcus Rohrmoser,  
App Entwickler seit 2009,  
Mitglied u.a. in [FIfF](https://fiff.de), [CCC](https://ccc.de), [GI](https://gi.de), [ACM](https://acm.org),  
Kurzvorträge beim CCC, zuletzt [36c3: "Are YOU ready to sustain IT?"](https://media.ccc.de/v/36c3-10524-lightning_talks_day_2#t=2011)

## Verweise

