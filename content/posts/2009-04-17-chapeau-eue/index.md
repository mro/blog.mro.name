---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2009-04-17T17:45:14+00:00"
tags:
- BKA
- CCC
- GI
- Sicherheit
- Zensur
title: Chapeau 1&1, kein Verstecken statt Bestrafen!
type: post
url: /2009/04/chapeau-eue/
yourls_shorturl:
- http://s.mro.name/3r
---
[Heute Vormittag][1] haben ja der [wachsame Herr Ziercke vom BKA][2] und unsere [telegene Bundesfamilienministerin mit 5 großen Providern einen Vertrag über ein Internetzensürchen][3] geschlossen.

Wenige große Provider waren mutig genug, sich dieser [skrupellosen Wahlkampfaktion][4] zu entziehen, u.a. Freenet und [1&1, deren Pressesprecher in der SZ seine Bestürzung][5] über das Vorgehen der fauchenden Ministerin darlegt.

Der [Forderung der GI nach wirksamer Verfolgung][6] schließe ich mich voll an und verweise auf die [Erfahrungen von Carechild, wie schnell man KiPr0nServer vom Netz bekommt][7] – wenn man das denn will.

Das Internet darf kein grundrechtsfreier Raum sein – auch wenn das Ziercke & Schäuble anscheinend gerne so hätten.

**Nachtrag**: Der CCC bringt einen [Audio-Mitschnitt der Pressekonferenz zum Internet-Zensurvertrag][8].

 [1]: http://dasalte.ccc.de/updates/2009/besucht-zensursula?language=de
 [2]: http://www.carechild.de/carechild/careblog/bka_chef_ziercke_fordert_gesetz_zur_sperrung_von_webseiten_mit_kinderpornographie_511_154.html
 [3]: http://www.zeit.de/online/2009/17/netzsperren-leyen-stoppschild#
 [4]: http://www.carechild.de/carechild/careblog/bundestagsabgeordneter_uhl_starke_worte_aber_wenig_kompetenz_574_1.html
 [5]: http://www.sueddeutsche.de/computer/810/465401/text/
 [6]: http://www.gi-ev.de/presse/pressemitteilungen-2009/pressemitteilung-vom-3-april-2009.html
 [7]: http://www.carechild.de/news/politik/internetzensur_carechild_versuch_blamiert_deutsche_politiker_566_1.html
 [8]: http://ccc.de/updates/2009/filter-mitschnitt?language=de