---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-06-18T16:28:23+00:00"
tags:
- DiskUtility
- DiskWarrior
- FileVault
- HFS
- MacBook
- OS X
title: HFS Filesystem Party
type: post
url: /2009/06/hfs-filesystem-spas/
yourls_shorturl:
- http://s.mro.name/p
---
Vorgestern Nacht hat – möglicherweise im Zusammenhang mit einem unaufmerksam durchgeführten [Java Update][1] – das [Filesystem meines Macbooks][2] einen Schuß bekommen. Folge: Die Maschine bootet, bootet, bootet – und schaltet sich kommentarlos aus (sic!). Alles sehr ergonomisch – ohne verwirrende Fehlermeldungen.

<!--more-->

{{< figure  src="/wp-content/uploads/2009/06/bild-1.png" caption="Das letzte Lebenszeichen." width="400" >}}

{{< figure  src="/wp-content/uploads/2009/06/bild-12.png" caption="The Road to Hell..." width="400" >}}
  
Also das [&#8222;Disk Utility&#8220; von der Installations DVD][3] gefragt – das beim
&#8222;Verify&#8220; aus der Kurve flog und das Filesystem auch nicht reparieren konnte (Stichwort
[&#8222;Invalid Key Length&#8220;][4] und [&#8222;Invalid Node Structure&#8220;][5]). Also hab' ich
meine Freunde von [Okapi][6] kontaktiert, die hatten den [DiskWarrior][7] rumliegen, abholen und
wieder nach Hause.

Nur – wie kriege ich den Krieger gestartet? Regulär booten ging nicht, Single-Mode oder von der
Installations-DVD bedeutet read-only Partitionen, das DVD Laufwerk ist belegt und USB Stick hatte
ich keinen. Ach ja, vom CodeWarrior hatte ich eine Version vor Oktober 2008 – dessen DVD bootete
mein Macbook nicht.

{{< figure  src="/wp-content/uploads/2009/06/dw-fehler.png" caption="dw-fehler" width="400" >}}

Inzwischen war's 16:30 und ich klagte mein Leid meinem Mac-Kumpel, der kurzerhand beschloß bei mir
vorbeizukommen, seinerseits mit Macbook, externer Festplatte und einem USB-Fisch. Wäre ja gelacht.

Nach ca. einer Stunde erfolglosen Versuchen den Warrior auf den USB-Stick, den USB-Stick in's
Macbook und den Warrior gestartet zu bekommen riß uns der Geduldsfaden und wir entschlossen uns zur
Radikallösung:

* Festplatte ausbauen und extern an's 2. Macbook anschließen,
* alle relevanten Dateien (im wesenlichen iTunes, Libary und ~/.* Files)  aus dem [FileVault][8] (ja, ich geb's zu) heraus auf's Macbook-II und weiter über LAN auf meinen Gentoo-Desktop kopieren,
* Platte wieder einbauen, Löschen und frisches OS X installieren + Systemupdate,
* Daten von Gentoo-Rechner wieder auf's Macbook (diesmal ohne FileVault),
* iPhone SDK wieder installieren,
* [Dropbox, git, Skype, Firefox, macports, bcpp, lftp, Gimp, NeoOffice, Inkscape, pwgen, TrueCrypt, etc.][9] wieder installieren.

Fazit: Kein Datenverlust, aber ein zwei Tage Arbeitsausfall. Das Backup (gibt's, ist aber nicht
komplett und nicht komfortabel wiederherzustellen) muß besser werden. [Time-Capsule][10], ich
komme&#8230;

 [1]: http://www.golem.de/0906/67782.html
 [2]: http://de.wikipedia.org/wiki/Hierarchisches_Dateisystem
 [3]: http://kb.wisc.edu/helpdesk/page.php?id=3810
 [4]: http://www.google.com/search?q=%22invalid+key+length%22
 [5]: http://www.google.com/search?q=%22invalid+node+structure%22
 [6]: http://okapi.de/
 [7]: http://www.alsoft.com/DiskWarrior/
 [8]: http://de.wikipedia.org/wiki/FileVault
 [9]: http://wiki.mro.name/orga/wahid
 [10]: http://www.apple.com/de/timecapsule/