---
author: Marcus Rohrmoser
categories:
- en
date: 2019-01-23 21:22:23+01:00
tags:
- Hugo
title: 'A new Blog 🎂'
type: post
url: /2019/01/a-new-blog/
---

[Hugo](https://gohugo.io/). Finally 🚀

I decided to wait no longer and rather drop features (for now e.g. showing comments, sigh),
start minimal and a bit broken, improve as I go along.
