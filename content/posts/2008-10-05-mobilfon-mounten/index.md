---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2008-10-05T16:01:17+00:00"
tags:
- 6610
- fuse
- gentoo
- IrDA
- Nokia
- obex
title: Mobilfon via IrDA mounten
type: post
url: /2008/10/mobilfon-mounten/
yourls_shorturl:
- http://s.mro.name/3p
---
Mit [obexftp][1], [obexfs][2] und [fuse][3] läßt sich ein Mobil-Telephon sogar in's Filesystem einhängen.

Nachdem das [Telephon erreichbar ist,][4] braucht's eine Kernel Einstellung:

<pre class="line-numbers"><code class="language-shell-session">File systems  --->
        <M> Filesystem in Userspace support
</code></pre>

und die Installation der Pakete:

<pre class="line-numbers"><code class="language-shell-session">$ sudo emerge sys-fs/fuse
$ sudo emerge app-mobilephone/obexftp
$ sudo emerge sys-fs/obexfs
</code></pre>

Schnell noch die Verbindung getestet (ggf. mehrmals probieren):

<pre class="line-numbers"><code class="language-shell-session">$ gammu --identify
$ obexftp -i -l
</code></pre>

Schon läßt sich das Telephon via obexfs & fuse mounten:

<pre class="line-numbers"><code class="language-shell-session">$ obexfs -i mountpoint
$ ls -l mountpoint
$ fusermount -u mountpoint
</code></pre>

**Vorsicht!** Während das Telephon gemountet ist, funktioniert gammu nicht!

 [1]: http://dev.zuckschwerdt.org/openobex/wiki/ObexFtp
 [2]: http://dev.zuckschwerdt.org/openobex/wiki/ObexFs
 [3]: http://fuse.sourceforge.net
 [4]: /2008/10/nokia-6610-linux-gentoo-stable-amd64/
