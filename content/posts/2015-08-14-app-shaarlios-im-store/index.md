---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2015-08-14T13:28:26+00:00"
tags:
- App Store
- github
- GPL
- iOS
- Shaarli
title: App 'ShaarliOS' im Store 📱
type: post
url: /2015/08/app-shaarlios-im-store/
---
brandneu für einen Euro im [App Store][1] ist meine 'Share Extension' für iOS 8+ und [Shaarli][2].

Und – das ist ein Experiment – gleichzeitig als [GPLv3 Free Software][3] bei [github][4].

Bin gespannt.

 [1]: http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1027441388&mt=8
 [2]: http://sebsauvage.net/wiki/doku.php?id=php:shaarli
 [3]: http://www.gnu.org/philosophy/free-sw.de.html
 [4]: https://github.com/mro/ShaarliOS
