---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-07-10T14:29:36+00:00"
tags:
- sh
- ImageMagick
- OS X
- printf
- SANE
- scanimage
- Scanner
title: Sane Batch Scan Workaround
type: post
url: /2009/07/sane-batch-scan-workaroun/
yourls_shorturl:
- http://s.mro.name/2v
---
Obwohl [scanimage][1] aus dem [SANE][2] Werkzeugkasten in der (aktuellen?) [OS X][3] Version

<pre class="line-numbers"><code class="language-shell-session">$ scanimage --version
scanimage (sane-backends) 1.0.21cvs; backend version 1.0.21
</code></pre>

(installiert nach <http://blog.mro.name/2008/12/scanner-mustek-1200-cu-unter-mac-os-x-105/>) beim Versuch per &#8222;batch&#8220; mehrere Seiten zu scannen jeweils nach Seite 1 aus der Kurve fliegt:

<pre class="line-numbers"><code class="language-shell-session">$ scanimage --batch-start=45 --batch-count=16 --batch --batch-prompt \
--format=pnm --mode Color --resolution 300
...
Scanning page 46
scanimage: sane_start: Invalid argument
</code></pre>

und das auch ein [bekanntes Problem][4] zu sein scheint, gibt's einen Workaround per selbstgebautem Shell-Script:

<pre class="line-numbers"><code class="language-sh">#!/bin/sh
# http://blog.mro.name/2009/07/sane-batch-scan-workaroun/
pre=scan-`date "+%Y-%m-%dT%H:%M"`
echo "To create a pdf, call after scanning the pages:"
echo "    convert $pre*.png -despeckle -density 100x100 -compress JPEG -quality 50 -page a4 $pre.pdf"
for ((i=0;i &lt; 100;i++))
do
  echo "Place document no. $i on the scanner."
  echo "Press &lt;RETURN> to continue, &lt;q> + &lt;RETURN> to quit."
  echo \\a
  read key
  if [[ $key == "q" || $key == "Q" ]] ; then
    break
  fi
  dst=$pre-page$(printf %03d $i)
  scanimage --mode Gray --resolution 300 --buffer-size=1024 --progress > $dst.pnm
  # background conversion:
  (convert $dst.pnm -level 15,85% -density 300x300 -type grayscale \
    -resample 100x100 $dst.png && rm $dst.pnm) &
done
ls -l $pre*
</code></pre>

é voilá.

Um das ganze dann noch in ein PDF moderater Größe zu verwandeln genügt ([ImageMagick z.B. via darwinport][5] vorausgesetzt):

<pre class="line-numbers"><code class="language-shell-session">$ time convert *.png -despeckle -density 100x100 \
-compress JPEG -quality 75 -page a4 out.pdf

real  3m39.867s
user  2m55.069s
sys 0m9.628s
</code></pre>

.

 [1]: http://www.sane-project.org/man/scanimage.1.html
 [2]: http://de.wikipedia.org/wiki/Scanner_Access_Now_Easy
 [3]: http://de.wikipedia.org/wiki/OS_X
 [4]: http://forum.ubuntuusers.de/topic/probleme-mit-scanner-im-batch-betrieb-scanima/?highlight=pag#post-761267
 [5]: http://www.imagemagick.org/script/binary-releases.php#macosx
