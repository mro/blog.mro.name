---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-06-22T10:55:56+00:00"
tags:
- alloc
- compiler
- deprecated
- gcc
- initWithFrame
- iPhone
- Objective C
- UITableViewCell
- warning
- XCode
title: 'XCode: missing „deprecated“ warnings'
type: post
url: /2010/06/xcode-missing-deprecated-warnings/
yourls_shorturl:
- http://s.mro.name/w
---
when developing for long-term use, you want to use APIs that aren't [likely to be removed soon, a.k.a. &#8222;deprecated&#8220;][1].

So, don't use downward compatible calls below a point you really aim for.

XCode helps with compiler warnings about &#8222;deprecated&#8220; calls – if &#8222;Project -> Edit Project Settings -> GCC\_WARN\_ABOUT\_DEPRECATED\_FUNCTIONS&#8220; is set:

{{< figure  src="/wp-content/uploads/2010/06/Bildschirmfoto-2010-06-22-um-12.39.48.png" caption="Compiler setting: Warn about deprecated calls"  width="385"  height="150" >}}

But be careful, this complains e.g. here only once while [`initWithFrame:`](http://developer.apple.com/iphone/library/documentation/uikit/reference/UITableViewCell_Class/DeprecationAppendix/AppendixADeprecatedAPI.html#//apple_ref/doc/uid/TP40006938-CH3-SW32) is also deprecated (compiling for iPhone OS 3.0):

{{< figure  src="/wp-content/uploads/2010/06/Bildschirmfoto-2010-06-22-um-12.41.57.png" caption="No &quot;deprecated&quot; warning on initXY"  width="300"  height="14" >}}

As [Kevin Ballard pointed out at Stackoverflow][2], this is because `[AnyClass alloc]` returns a type `id` – which doesn't know about it's interface.

To get this kind of compiler warnings, you have to type-cast the `[AnyClass alloc]` like this:

<pre class="line-numbers"><code class="language-objc">
[(AnyClass*)[AnyClass alloc] initXY];
</code></pre>

Maybe time for a macro?

The macro could look like

<pre class="line-numbers"><code class="language-objc">
// http://blog.mro.name/2010/06/xcode-missing-deprecated-warnings/
#define alloc(c)  ((c*)[c alloc])
</code></pre>

Migrate your codebase via XCode &#8222;Edit -> Find in Project&#8220; with search pattern `\[\s*([^\[\]]+)\s+alloc\s*\]` and replacement pattern `alloc(\1)`.

 [1]: http://en.wikipedia.org/wiki/Deprecation
 [2]: http://stackoverflow.com/questions/2135514/deprecated-not-triggering-compiler-warning-with-subclass/2136130#comment-2076316
