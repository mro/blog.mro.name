---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-11-27T20:36:53+00:00"
tags:
- Java
- jdiskreport
- Webstart
title: Was belegt eigentlich wieviel Plattenplatz?
type: post
url: /2009/11/was-belegt-eigentlich-wieviel-plattenplatz/
yourls_shorturl:
- http://s.mro.name/3h
---
sagt einem [JDiskReport][1] (z.B. [per Java Webstart][2]) recht schön zum Durchklicken:

{{< figure src="http://www.jgoodies.com/wp-content/uploads/2012/04/o1960.jpg" alt="JDiskReport" >}}

 [1]: http://www.jgoodies.com/freeware/jdiskreport/
 [2]: http://www.jgoodies.com/download/jdiskreport/jdiskreport.jnlp