---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-03-29T20:42:09+00:00"
tags:
- inspire
- TED
title: Internet in Dosen
type: post
url: /2009/03/internet-in-dosen/
yourls_shorturl:
- http://s.mro.name/16
---
Ca. 700 [MebiByte][1] gemischter Internet-(Film-)Aufschnitt:

* [Willie Smits restores a rainforest | Video on TED.com][2]
* [Money as Debt – Geld als Schuld (deutsche Untertitel) | Google Video][3]
* [Barry Schwartz on our loss of wisdom | Video on TED.com][4]
* [Matthieu Ricard on the habits of happiness | Video on TED.com][5]
* [David Carson on design, discovery and humor | Video on TED.com][6]
* [e-Mails verschlüsseln mit S/MIME | vimeo.com][7]
* [Laura Trice suggests we all say thank you | Video on TED.com][8]
* [Johnny Lee demos Wii Remote hacks | Video on TED.com][9]
* [Blaise Aguera y Arcas demos Photosynth | Video on TED.com][10]
* [Robert Lang folds way-new origami | Video on TED.com][11]
* [Nalini Nadkarni on conserving the canopy | Video on TED.com][12]
* [Peter Donnelly shows how stats fool juries | Video on TED.com][13]

 [1]: http://en.wikipedia.org/wiki/Mebibyte
 [2]: http://www.ted.com/index.php/talks/willie_smits_restores_a_rainforest.html
 [3]: http://video.google.com/videoplay?docid=6433985877267580603&hl=en
 [4]: http://www.ted.com/talks/barry_schwartz_on_our_loss_of_wisdom.html
 [5]: http://www.ted.com/index.php/talks/matthieu_ricard_on_the_habits_of_happiness.html
 [6]: http://www.ted.com/index.php/talks/david_carson_on_design.html
 [7]: http://vimeo.com/3215359
 [8]: http://www.ted.com/index.php/talks/laura_trice_suggests_we_all_say_thank_you.html
 [9]: http://www.ted.com/index.php/talks/johnny_lee_demos_wii_remote_hacks.html
 [10]: http://www.ted.com/index.php/talks/blaise_aguera_y_arcas_demos_photosynth.html
 [11]: http://www.ted.com/index.php/talks/robert_lang_folds_way_new_origami.html
 [12]: http://www.ted.com/index.php/talks/nalini_nadkani_on_conserving_the_canopy.html
 [13]: http://www.ted.com/index.php/talks/peter_donnelly_shows_how_stats_fool_juries.html