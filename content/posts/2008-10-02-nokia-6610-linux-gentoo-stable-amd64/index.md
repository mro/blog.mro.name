---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2008-10-02T12:42:12+00:00"
tags:
- 6610
- gammu
- gentoo
- IrDA
- Nokia
title: Nokia 6610 & Linux (gentoo stable, amd64)
type: post
url: /2008/10/nokia-6610-linux-gentoo-stable-amd64/
yourls_shorturl:
- http://s.mro.name/3a
---
Auch von einem Linux Rechner aus kann man auf Mobiltelefone zugreifen, allerdings sind ein paar Vorbereitungen nötig. Siehe dazu auch die [Gentoo IrDA Anleitung][1].

Ausgangspunkt ist ein [Nokia 6610][2] Mobilfon, ein [Gentoo AMD64 stable][3] Rechner und ein [Cytronix IR-USB Dongle][4].

Zunächst wollen ein paar Kernel Einstellungen gesetzt sein:

<pre class="line-numbers"><code class="language-shell-session">Networking  --->
        <M> IrDA (infrared) subsystem support  --->
           <M> IrCOMM protocol
        Infrared-port device drivers  --->
             <M> IrDA USB dongles
             <M> SigmaTel STIr4200 bridge (EXPERIMENTAL)
</code></pre>

Damit die Module automatisch geladen werden:

<pre class="line-numbers"><code class="language-shell-session">$ cat /etc/modprobe.d/irda
alias irda0 stir4200
$ sudo update-modules
</code></pre>

Ein paar Pakete installieren:

<pre class="line-numbers"><code class="language-shell-session">$ sudo emerge irda-utils gammu
</code></pre>

und [gammu][5] einstellen:

<pre class="line-numbers"><code class="language-shell-session">$ cat ~/.gammurc
[gammu]
name = Nokia 6610
# port=/dev/ircomm0
connection = irdaphonet
synchronizetime = yes
# rsslevel = teststable
startinfo = yes
usephonedb = yes
# UsePhoneDB=yes
</code></pre>

Das Telephon noch auf Infrarot lauschen lassen, und schon sollte es klappen:

<pre class="line-numbers"><code class="language-shell-session">$ sudo irattach irda0 -s;sleep 10;gammu --identify
INFO: there is later testing Gammu (1.20.94 instead of 1.20.0) available!
Manufacturer         : Nokia
Model                : 6610 (NHL-4U)
Firmware             : 5.52 C (19-09-03)
Hardware             : 0660
IMEI                 : 352927001317658
Original IMEI        : 352927/00/131765/8
Manufactured         : 11/2003
Product code         : 0510460
UEM                  : 8
</code></pre>

Das war's.

Jetzt läßt sich per gammu das Telephon inspizieren, z.B.

<pre class="line-numbers"><code class="language-shell-session">$ gammu getfilesystem
    c:/1.   SFolder "C (Permanent_memory)"
    c:/5.  HS |-- Folder "Empf. Dateien"
    c:/2.   S |-- Folder "Galerie"
    c:/3.   S |    |-- Folder "Bilder"
   c:/37.     |    |    |-- "Cell.jpg"
   c:/40.     |    |    |-- "Clock.jpg"
   c:/39.     |    |    |-- "Crisp.jpg"
...
</code></pre>

 [1]: http://gentoo-wiki.com/IrDA
 [2]: http://www.forum.nokia.com/devices/6610
 [3]: http://www.gentoo.org/doc/en/handbook/handbook-amd64.xml
 [4]: http://www.google.de/url?sa=t&source=web&ct=res&cd=1&url=http%3A%2F%2Fwww.sigmatel.com%2Fproducts%2Fportable%2Fwireless%2Fstir4200.aspx&ei=xJ3kSNWDDJj20AWv_qH0DA&usg=AFQjCNGjCXaTOxnBzWVhHGA8jHKqvTm-Vw&sig2=wqSL28ar6VUbgWFtClzICw
 [5]: http://www.gammu.org/
