---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2015-03-23T07:42:33+00:00"
tags:
- GPX
- schema
- validate
- W3C
- XML
title: Validate GPX
type: post
url: /2015/03/validate-gpx/
yourls_fetching:
- 1
---

$ [xmllint](http://xmlsoft.org/xmllint.html) -\-noout -\-schema [http://www.topografix.com/GPX/1/1/gpx.xsd](http://www.topografix.com/gpx.asp) \<gpx file or url\>