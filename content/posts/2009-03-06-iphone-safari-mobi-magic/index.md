---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-03-06T21:51:47+00:00"
tags:
- iPhone
- Safari
title: iPhone Safari .mobi magic
type: post
url: /2009/03/iphone-safari-mobi-magic/
yourls_shorturl:
- http://s.mro.name/23
---
Several weeks I was puzzling about a [mystical rendering difference of bitwise identical html documents][1]. The [official documentation][2] sadly wasn't too helpful, but [this post][3] finally solved it:

.mobi domains silently get different rendering defaults.

Isn't that nuts? Many thanks to Christopher for enlightening this obscure fact.

{{< figure src="/wp-content/uploads/2009/03/bild-7.png" caption=".mobi" >}}

{{< figure src="/wp-content/uploads/2009/03/bild-9.png" caption="non .mobi" >}}

 [1]: http://alsw.mobi/zoom.html
 [2]: http://developer.apple.com/webapps/docs/documentation/AppleApplications/Reference/SafariWebContent/UsingtheViewport/chapter_4_section_7.html
 [3]: http://groups.google.com/group/iphonewebdev/msg/c0a36a4993d0f8f2
