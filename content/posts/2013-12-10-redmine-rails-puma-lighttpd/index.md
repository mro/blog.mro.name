---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2013-12-10T12:16:46+00:00"
tags:
- Debian
- lighttpd
- puma
- Rack
- Rails
- redmine
- Ruby
- Ruby on Rails
title: redmine (rails) + puma + lighttpd
type: post
url: /2013/12/redmine-rails-puma-lighttpd/
yourls_shorturl:
- http://mro.name/4i
---
Running [redmine][1] with a lightweight [ruby on rails][2]/webserver stack on a [debian][3] server – [puma][4] and [lighttpd][5]:

Assumed you've got both redmine and lighttpd already installed:

1. install puma gem: `$ sudo gem install puma` 
2. **caution**: [ArgumentError on ruby 1.8.7][6]
3. get [tools/jungle/init.d/puma][7] to `/etc/init.d/`
4. get [tools/jungle/init.d/run-puma][8] to `/usr/local/bin/`
5. add a puma app: `$ sudo /etc/init.d/puma add /your/app/path www-data`
6. `$ sudo -u www-data mkdir /your/app/path/tmp/puma`
7. puma config in rails app `/your/app/path/config/puma.rb`
<pre class="line-numbers"><code class="language-ruby">$ cat your/app/path/config/puma.rb
environment 'production'
pidfile File.expand_path( File.dirname(__FILE__) + "/../tmp/puma/pid" )
state_path File.expand_path( File.dirname(__FILE__) + "/../tmp/puma/state" )
threads 1,4
port 8082
activate_control_app
</code></pre>

8. `$ grep puma /your/app/path/Gemfile.local  
gem "puma"`
9. lighttpd proxy config:
<pre class="line-numbers"><code class="language-shell-session">$ grep 8082 /etc/lighttpd/conf-enabled/10-proxy.conf
$HTTP["host"] == "redmine.example.com" { proxy.server = ( "" => (("host" => "127.0.0.1", "port" => 8082))) }
</code></pre>

10. `$ sudo /etc/init.d/puma start`
11. `$ sleep 5 ; cat /your/app/path/tmp/puma/state`
12. `$ sudo update-rc.d -f puma defaults`

Versions:

* ruby: 1.8.7-p358 (2012-02-08) [x86_64-linux]
* rails: 3.2.15
* lighttpd: 1.4.28
* puma: 2.7.1
* redmine: 2.4.0

 [1]: http://redmine.org
 [2]: http://rubyonrails.org
 [3]: http://www.debian.org/
 [4]: http://puma.io
 [5]: http://http://www.lighttpd.net/
 [6]: https://github.com/puma/puma/issues/427
 [7]: (https://github.com/puma/puma/blob/0fa28f9d1f493a67df94db181c796912f8c4de06/tools/jungle/init.d/puma
 [8]: https://github.com/puma/puma/blob/0fa28f9d1f493a67df94db181c796912f8c4de06/tools/jungle/init.d/run-puma
