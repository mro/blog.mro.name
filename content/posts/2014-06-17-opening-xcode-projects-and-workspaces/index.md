---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2014-06-17T09:56:26+00:00"
tags:
- sh
- XCode
title: Opening Xcode projects and workspaces
type: post
url: /2014/06/opening-xcode-projects-and-workspaces/
yourls_shorturl:
- http://mro.name/4n
---
Inspired by [ortas post][1] about it I came up with this one-liner:

<pre class="line-numbers"><code class="language-shell-session">alias openx='open *.xcworkspace 2>/dev/null || open *.xcodeproj 2>/dev/null || echo "nagnagnag"'
</code></pre>

 [1]: https://orta.github.io/rebase/on/opening-xcode-projects-and-workspaces/
