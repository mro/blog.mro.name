---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2009-06-21T16:40:55+00:00"
tags:
- CoreData
- iPhone
- Objective C
title: CoreData – Hello World.
type: post
url: /2009/06/coredata-hello-world/
yourls_shorturl:
- http://s.mro.name/11
---
[CoreData][1] ist im iPhone OS 3.0 ganz frisch dazugekommen und riecht auch noch ein wenig nach Plastik.

Leider ist der Beispielcode auf den Apple auch im [iPhone CoreData Tutorial][2] immer wieder verweist ([Locations Sample Project][3]) dem Umzug der OS 3.0 beta zum Opfer gefallen.

Also los geht's zum ersten Schritt:

<!--more-->
  
## 1. Ein leeres Projekt

Nachdem iPhone SDK 3.0 installiert ist: &#8222;XCode -> File -> New Project&#8220; mit &#8222;Use Core Data for storage&#8220; Häkchen&#8220;:

{{< figure src="/wp-content/uploads/2009/06/bild-5.png" caption="Neues Projekt mit CoreData Unterstützung" >}}

Der Application Delegate enthält dann gleich eine ganze Menge Code zur Verwaltung der Datenquelle, u.a. mit

<pre class="line-numbers"><code class="language-objc">NSURL *storeUrl = [NSURL fileURLWithPath:
[[self applicationDocumentsDirectory]
stringByAppendingPathComponent: @"CoreDataDemo.sqlite"]];
</code></pre>

den Namen der sqlite DB Datei, in die gespeichert werden soll.

## 2. Das Schema

{{< figure src="/wp-content/uploads/2009/06/bild-6.png" caption="Ein neues CoreData Schema" >}}

und sollte so heißen wie die DB oben.

## 3. Zugriffe

Siehe [Core Data Programming Guide – Fetching Managed Objects][4].

Damit die Beispiel-Zugriffe Sinn ergeben, muß das Schema eine Entity &#8222;Employee&#8220; mit Attributen &#8222;lastName&#8220; (String) und &#8222;salary&#8220; (Numeric) enthalten.

Dann sollte der erste Einstieg eigentlich klappen.

 [1]: http://developer.apple.com/iphone/library/referencelibrary/GettingStarted/GettingStartedWithCoreData/index.html#//apple_ref/doc/uid/TP40005316
 [2]: http://developer.apple.com/iphone/library/documentation/DataManagement/Conceptual/iPhoneCoreData01/Introduction/Introduction.html
 [3]: http://developer.apple.com/iphone/prerelease/library/samplecode/Locations/index.html
 [4]: http://developer.apple.com/documentation/Cocoa/Conceptual/CoreData/Articles/cdFetching.html#//apple_ref/doc/uid/TP40002484
