---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-06-19T12:56:05+00:00"
tags:
- DNS
- Nameserver
- T-Kom
- T-Online
- Zensur
title: Übt die T-Kom schon Zensieren?
type: post
url: /2009/06/ubt-die-t-kom-schon-zensieren/
yourls_shorturl:
- http://s.mro.name/2u
---

{{< figure  src="/wp-content/uploads/2009/06/t-kom-annoyance.png" caption="Ist das schon die Vorstufe zum DNS Blocking?" width="500" >}}

– eben gesehen und an Zensur gedacht.

Was soll das denn? Ich finde das mindestens sehr aufdringlich. Da ist bald ein anderer Nameserver fällig.

(Oder verstehe ich das komplett falsch?)