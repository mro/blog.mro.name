---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-06-18T18:25:27+00:00"
tags:
- iTunes
- OS X
title: iTunes Mediathek aus dem Home Verzeichnis raus
type: post
url: /2009/06/itunes-mediathek-aus-dem-home-verzeichnis-raus/
yourls_shorturl:
- http://s.mro.name/34
---
Damit das Home-Verzeichnis meines Macbooks mehr Arbeit und weniger Ballast enthält nehme ich die MP3s raus:

<!--more-->

{{< figure  src="/wp-content/uploads/2009/06/bild-18.png" caption="iTunes Mediathek zentral"  width="400"  >}}

und jetzt noch umschaufeln, iTunes -> Ablage -> Mediathek:

{{< figure  src="/wp-content/uploads/2009/06/bild-19.png" caption=""  width="328" height="168" >}}

fertig!