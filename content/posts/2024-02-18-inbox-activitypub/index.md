---
title: 'Who is welcome to my Inbox? (ActivityPub)'
url: /2024/02/inbox-activitypub/
draft: false
categories:
- en
date: 2024-02-18T20:34:11+0100
tags:
- ActivityPub
- Federation
- Inbox
- Spam
- Seppo
type: post
author: Marcus Rohrmoser
---

Part II of 'Implementing [Federation](/tags/federation/)'.

The fediverse recently saw another spam wave[^wave]. It's not all surprising,
Mike[^warn] has been warning since long, but not all prepared or simply haven't
heard the warnings. This wave won't remain the last nor worst, I suspect. And
Mike recently had another valuable piece of advice[^listen] for those who are
willing to listen.

IMO things boil down to: *Who is welcome to my inbox?*

The spectrum spans from known actors[^actor] I subscribe to (yes), to known
actors I block (no). That's the easy part.

What about unknown actors, should they be able to contact me? And if so, how?

Naturally, it depends. But are there common aspects?

Of course we shall leverage the social nature and good manners like e.g.
knocking and some patience, which might help work the much needed wonders. I trust
people (my friends, namely) more than technology.

So #Seppo[^seppo], the first person tooter, shall not rely on 3rd parties nor
external cooperation to decide.

Also maybe some sort of (announced) opening hours may make sense.

## Welcome

are 

1. actors subscribed to, mentioned or interacted with by me or an actor I am subscribed to (direct network),
1. bringing invite codes (one-time 'magic' words to mention),
1. actors having 'knocked' (see below),
1. any actors during (announced?) open hours.

## Unwelcome

are

1. actors I block
1. actors blocked by an actor I subscribe to (salt-hashed actor ids, if I knew)
1. all unknown

## Knocking

At first incoming messages from strangers are discarded, but get a 24h
delayed, automated reply telling a generated (one-time) magic word. If this comes back
within 3 more days, the sending actor gets guestlisted.

In essence, such a setting welcomes those you subscribe to or already
interacted with, but still leaves a chance for stranger contact. But there will
be no content accepted prior the guestlisting.

Also it allows to collect feedback (mentioning the invite code) from strangers
during a limited period of time.

It requires no sorcery nor external wisdom you may or may not get, is as
accessible as #Seppo generally is and doesn't discriminate.

I look forward to implement it one fine day and see how it turns out.

Seppo is generously supported by NLnet &amp; EU 🇪🇺 under grant https://nlnet.nl/project/Seppo

[^wave]: https://mastodon.social/@Gargron/111953045633249137
[^warn]: https://macgirvin.com/item/b242df84-ba35-4a02-9489-fb7bdcee9e41
[^listen]: https://fediversity.site/item/03004cf8-cd03-4c57-9a97-081c49657346
[^actor]: https://www.w3.org/TR/activitypub/#actors
[^seppo]: https://seppo.social
