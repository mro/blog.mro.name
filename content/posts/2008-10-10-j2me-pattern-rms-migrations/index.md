---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2008-10-09T23:14:31+00:00"
tags:
- J2ME
- migration
- pattern
title: 'J2ME Pattern: RMS Migrations'
type: post
url: /2008/10/j2me-pattern-rms-migrations/
yourls_shorturl:
- http://s.mro.name/9
---
Inspired by [RoR Migrations][1] I'll summarize how to gain similar benefits in the J2ME world with respect to it's special needs:

* attach a version marker to each [RecordStore][2] name,
* as there's no callback at install time and at launch time things must be quick as possible, use a fall-back mechanism on opening RecordStores,
* therefore use a custom method to open RecordStores and give it a handler for [RecordStoreNotFoundException][3],
* use methods rather than classes to implement migrations,
* only migrate forward,
* hardcode the migration methods into the RecordStoreNotFoundException handler – there's no Reflection in J2ME.

=>

* you can change the RMS store names and storage byte semantics whenever you like without the fear of breaking anything,
* there's impact on startup time only if the storage semantics changed and needs to be converted,
* you don't add myriads of classes over time,
* you need to write a converter method for each version bump of each store.

Check back later for sample code.

 [1]: http://wiki.rubyonrails.org/rails/pages/ActiveRecordMigration
 [2]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/rms/RecordStore.html
 [3]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/rms/RecordStoreNotFoundException.html