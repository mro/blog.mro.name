---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-06-23T11:15:24+00:00"
tags:
- Base SDK
- Deployment Target
- Device
- iOS 4
- iPhone
- iPhone OS 3.0
- XCode
title: Develop with iOS 4 SDK on an OS 3.0 Device
type: post
url: /2010/06/develop-with-ios-4-on-an-os-3-0-device/
yourls_shorturl:
- http://s.mro.name/5
---
After upgrading to iOS 4.0 SDK, iPhone OS 3.0 is no longer a valid &#8222;Base SDK&#8220;. Naively switching to iPhone 3.2 prevents deployment to a 3.0 device.

But such &#8222;Project -> Edit Project Settings&#8220; work out fine:

{{< figure  src="/wp-content/uploads/2010/06/iPhoneOS3_0-device-with-iOS4_0-SDK-300x111.png" caption="iPhoneOS 3.0 development device with iOS 4.0 SDK"  width="300"  height="111" >}}

[&#8222;Deployment Target&#8220; hint found here][1].

 [1]: http://forum.unity3d.com/viewtopic.php?p=335612