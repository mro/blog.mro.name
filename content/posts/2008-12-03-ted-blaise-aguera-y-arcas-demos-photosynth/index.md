---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2008-12-02T22:01:56+00:00"
tags:
- 3D
- imaging
- inspire
- PhotoSynth
- semantics
- TED
title: 'TED: Blaise Aguera y Arcas demos Photosynth'
type: post
url: /2008/12/ted-blaise-aguera-y-arcas-demos-photosynth/
yourls_shorturl:
- http://s.mro.name/2y
---
[Blaise Aguera y Arcas demos Photosynth][1]. The vision of automatic semantic correlation of images is stunning. And automatic 3d models – hu!

{{<ted blaise_aguera_y_arcas_demos_photosynth >}}

 [1]: http://www.ted.com/index.php/talks/blaise_aguera_y_arcas_demos_photosynth.html