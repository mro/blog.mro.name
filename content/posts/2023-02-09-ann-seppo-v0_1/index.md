---
title: 'Announce Seppo.Social v0.1 and Request for Comments'
url: /2023/02/ann-seppo-v0_1/
draft: false
categories:
- en
date: 2023-02-09T10:43:09+0100
tags:
- ActivityPub
- Announce
- CGI
- NLnet
- OCaml
- Seppo
type: post
author: Marcus Rohrmoser
---

Hi all,

I am happy to announce the premiere release of #Seppo!, Personal Social Media under funding of NLnet.nl.

Find it at https://Seppo.Social/downloads/

It has no notable user facing ActivityPub features so far, but

- easy setup of instance & account,
- webfinger discoverability (from e.g. mastodon search),
- a welcoming, long-term reliable website.

I made this embarrassingly limited release to build awareness for low-barrier-entry internet services in general and especially in the field of personal communication as well as letting the #fediverse and permacomputing communities know.

Your comments are very much appreciated.

@fediverse@mro.name


