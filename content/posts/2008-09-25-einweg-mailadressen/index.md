---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2008-09-25T14:56:40+00:00"
title: Einweg Mailadressen
type: post
url: /2008/09/einweg-mailadressen/
yourls_shorturl:
- http://s.mro.name/26
---
Das Thema kam mir in letzter Zeit mehrmals unter, zuletzt auf [SZ
online](http://www.sueddeutsche.de/computer/616/311537/text/). Der [Link zum
CCC](https://anonbox.net/index.de.html) will ausprobiert werden.