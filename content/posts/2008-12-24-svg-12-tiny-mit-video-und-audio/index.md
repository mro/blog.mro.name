---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2008-12-24T08:42:19+00:00"
tags:
- Audio
- SVG
- Video
title: SVG 1.2 Tiny mit Video und Audio
type: post
url: /2008/12/svg-12-tiny-mit-video-und-audio/
yourls_shorturl:
- http://s.mro.name/d
---
hm – bis das wohl in den Browsern ankommt?

<http://www.w3.org/TR/2008/REC-SVGTiny12-20081222/multimedia.html#VideoElement>

Mit dem [W3C Beispiel][1] kann man selber probieren ob's der eigene Browser schon kennt. Aussehen sollte das dann so:

{{< figure src="http://www.w3.org/TR/2008/REC-SVGTiny12-20081222/examples/media02.png" caption="W3V SVG Tiny 1.2 Video Demo" width="420" height="340" >}}

 [1]: http://www.w3.org/TR/2008/REC-SVGTiny12-20081222/examples/noonoo.svg