---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2008-10-08T18:24:57+00:00"
tags:
- 6610
- CLDC
- J2ME
- MIDP
title: Nokia 6610 J2ME Capabilities
type: post
url: /2008/10/nokia-6610-j2me-capabilities/
yourls_shorturl:
- http://s.mro.name/a
---
The [Nokia 6610][1] has those [J2ME/CLDC/MIDP capabilities][2]. Frankly I don't understand all of it's findings, especially the one claiming &#8222;RMS: not implemented&#8220; – if that means [javax.microedition.rms][3] it's just not true.

Check your phone at [mobile-utopia][4].

 [1]: http://www.forum.nokia.com/devices/6610
 [2]: http://www.mobile-utopia.com/calibrator/Nokia6610%7Bfs%7D1.0+(5.52)+Profile%7Bfs%7DMIDP-1.0+Configuration%7Bfs%7DCLDC-1.0---Nokia6610%7Bfs%7D5.52
 [3]: http://java.sun.com/javame/reference/apis/jsr118/javax/microedition/rms/package-summary.html
 [4]: http://www.mobile-utopia.com/calibrator