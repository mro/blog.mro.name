---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2009-12-12T21:32:07+00:00"
tags:
- 3
- 3.2.1
- Cocoa
- CoreAnimation
- iPhone
- Simulator
- Snow Leopard
- underretain
- XCode
title: underretain in CoreAnimation / iPhone Simulator
type: post
url: /2009/12/underretain-in-coreanimation-iphone-simulator/
yourls_shorturl:
- http://s.mro.name/2w
---
After [upgrading to Snow Leopard and XCode 3.2.1][1] I've seen such console output

<pre class="line-numbers"><code class="language-log">CAUnderRetain(32139,0xa0391500) malloc: *** error for object 0x3838000: pointer being freed was not allocated
*** set a breakpoint in malloc_error_break to debug
</code></pre>

when rotating the iPhone Simulator in a project with base SDK 3.0.

This can be reproduced as follows:

* Create a fresh iPhone project (I use a &#8222;navigation App&#8220; with CoreData)
  {{< figure  src="/wp-content/uploads/2009/12/Bildschirmfoto-2009-12-12-um-21.38.06.png" caption="Fresh Navigation based App + CoreData"  width="300"  height="255" >}}
* add

    <pre class="line-numbers"><code class="language-objc">
    // Override to allow orientations other than the default portrait orientation.
    - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
      // Return YES for supported orientations.
      return YES;
    }

    -(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
      NSLog(@"willRotateToInterfaceOrientation");
    }
    </code></pre>
  to the `RootViewConroller.m`
* set the base SDK to 3.0:
  {{< figure  src="/wp-content/uploads/2009/12/Bildschirmfoto-2009-12-12-um-21.41.02.png" caption="Base SDk 3.0"  width="290"  height="300" >}}
* compile + run with Simulator 3.1.2, everthing fine when rotating the simulator:
  {{< figure  src="/wp-content/uploads/2009/12/Bildschirmfoto-2009-12-12-um-21.42.09.png" caption="Simulator 3.1.2 rotates fine"  width="300"  height="158" >}}
* Simulator 3.0 shows the underretain:
  {{< figure  src="/wp-content/uploads/2009/12/Bildschirmfoto-2009-12-12-um-21.50.41.png" caption="Simulator 3.0 underretain"  width="265"  height="300" >}}

    <pre class="line-numbers"><code class="language-gdb">[Session started at 2009-12-12 21:49:31 +0100.]
    GNU gdb 6.3.50-20050815 (Apple version gdb-1346) (Fri Sep 18 20:40:51 UTC 2009)
    Copyright 2004 Free Software Foundation, Inc.
    GDB is free software, covered by the GNU General Public License, and you are
    welcome to change it and/or distribute copies of it under certain conditions.
    Type "show copying" to see the conditions.
    There is absolutely no warranty for GDB.  Type "show warranty" for details.
    This GDB was configured as "x86_64-apple-darwin".sharedlibrary apply-load-rules all
    Attaching to process 32139.
    2009-12-12 21:49:43.845 CAUnderRetain[32139:207] willRotateToInterfaceOrientation
    CAUnderRetain(32139,0xa0391500) malloc: *** error for object 0x1853000: pointer being freed was not allocated
    *** set a breakpoint in malloc_error_break to debug
    CAUnderRetain(32139,0xa0391500) malloc: *** error for object 0x3824000: pointer being freed was not allocated
    *** set a breakpoint in malloc_error_break to debug
    warning: Unable to restore previously selected frame.
    warning: Couldn't find minimal bounds for "_sigtramp" - backtraces may be unreliable
    (gdb) break malloc_error_break
    Breakpoint 1 at 0x92626072
    (gdb) continue
    2009-12-12 21:49:58.490 CAUnderRetain[32139:207] willRotateToInterfaceOrientation
    CAUnderRetain(32139,0xa0391500) malloc: *** error for object 0x3838000: pointer being freed was not allocated
    *** set a breakpoint in malloc_error_break to debug

    Unable to disassemble malloc_error_break.
    (gdb) bt
    #0  0x92626072 in malloc_error_break ()
    #1  0x92535303 in free ()
    #2  0x0015ce49 in dataReleaseInfo ()
    #3  0x001481d9 in data_provider_finalize ()
    #4  0x30204421 in _CFRelease ()
    #5  0x00147fa2 in image_finalize ()
    #6  0x30204421 in _CFRelease ()
    #7  0x00c12ded in CALayerStateRelease ()
    #8  0x00c18290 in -[CALayer dealloc] ()
    #9  0x00c0a00e in CALayerRelease ()
    #10 0x00c0b265 in CA::release_root_if_unused ()
    #11 0x00c0b1ef in x_hash_table_remove_if ()
    #12 0x00c0afd4 in CA::Transaction::commit ()
    #13 0x00c132e0 in CA::Transaction::observer_callback ()
    #14 0x30245c32 in __CFRunLoopDoObservers ()
    #15 0x3024503f in CFRunLoopRunSpecific ()
    #16 0x30244628 in CFRunLoopRunInMode ()
    #17 0x32044c31 in GSEventRunModal ()
    #18 0x32044cf6 in GSEventRun ()
    #19 0x309021ee in UIApplicationMain ()
    #20 0x00002018 in main (argc=1, argv=0xbfffea84) at /Users/.../CAUnderRetain/main.m:14
    (gdb)
    </code></pre>

 [1]: http://blog.mro.name/2009/11/upgrade-to-snow-leopard/
