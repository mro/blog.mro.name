---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2015-03-30T06:42:31+00:00"
tags:
- Date
- HTTP Header
- Last-Modified
- PHP
- RFC
- RFC1123
- RFC2616
- RFC7231
- RFC7232
- W3C
title: Why PHP is utterly broken
type: post
url: /2015/03/why-php-is-utterly-broken/
yourls_fetching:
- 1
---
_**tl;dr:**_ neither does [`DATE_RFC1123`][1] produce a [`rfc1123-date`][2], nor does
[`DATE_RFC850`][3] a [`rfc850-date`][2]. What sense does that make?

This example may look scrupulous but seems typical.

Mind the mandatory word `GMT` at the end of [`rfc1123-date`][2]. While [`DATE_RFC1123`][1] renders
something ending with `+0000`. Which is the same _timezone_ but **not the same _string_**.

So [`DATE_RFC1123`][1] is useless for HTTP headers ([RFC2616][2]). I wonder what else it should be
there for.

While this isn't an issue of PHP (the language) itself, it's IMO typical for the actual PHP code
existing. It does work somehow, despite the fact that it's totally agnostic of the basics it claims
to be based upon. So it may fail any given, unforeseeable, moment, when any of the implicitly
assumed side-conditions change.

P.S.: The newer [RFC7231 (HTTP-Date)][4] and e.g. [RFC 7232, Last-Modified][5] don't change a thing.

P.P.S.: I don't do PHP, I just came across when patching <https://github.com/mro/Shaarli> a bit.

**Update:** Another [nice one][6].

 [1]: http://php.net/manual/en/class.datetime.php#datetime.constants.rfc1123
 [2]: http://tools.ietf.org/html/rfc2616#section-3.3.1
 [3]: http://php.net/manual/en/class.datetime.php#datetime.constants.rfc850
 [4]: http://tools.ietf.org/html/rfc7231#section-7.1.1.1
 [5]: http://tools.ietf.org/html/rfc7232#section-2.2
 [6]: http://blog.fefe.de/?ts=abb96baa