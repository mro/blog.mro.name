---
title: 'Lightning-Talk: Self-hosted Microblogging: ShaarliGo'
url: /2017/12/microblogging/
draft: false
categories:
#- 🇩🇪
- en
date: 2017-12-28 11:05:00+01:00
tags:
- 34c3
- CCC
- CGI
- Lightning-Talk
- Microblog
- self-hosted
- ShaarliGo
- Vortrag
type: post
author: Marcus Rohrmoser
---

“I invented the web. Here are three things we need to change to save it”
(Tim Berners Lee, 2017)


[📺 Video](https://txt.mro.name/34c3/34c3-self-hosted-microblogging-shaarligo_--_480p.mp4)  
[📄 Slides](https://txt.mro.name/34c3/34c3-self-hosted-microblogging-shaarligo.pdf)

http://mro.name/34c3

