---
title: TLS may not be what you think it is.
url: /2022/11/tls-may-not-be-what-you-think-it-is/
draft: false
categories:
- en
date: 2022-11-24T09:54:32+0100
tags:
- TLS
- privacy
- letsencrypt
type: post
author: Marcus Rohrmoser
---

For several years now there are numerous and prominent calls to "re-decentralise
the internet"[1]. This essentially means increasing the number of contributors &
server operators, not just consumers.

And these contributions have to happen independently of necessary supplies by
centralised entities in order to be "decentralised", right?

At the same time TLS[2] became so commonplace that is is a requirement for 'proper'
services, mostly driven by banking, shopping and the free and uncomplicated
certificates website operators can get from Let's Encrypt[3]. So there was no need
to use the geeky self-signed certificates no more, which came with a scary message
in the browser and needed extra action (trust, uh!) to work at all. Letsencrypt is
used by 300 million websites today (including this blog).

By that the unpopular, decentral, self-signed certificates became even less known
while a central infrastructure became mandatory (HSTS[4]). The winner took it all,
again.

Today TLS is a de-facto-centralised requirement which must be totally trusted by
everybody.

But now: "CA could open backdoor for US spies" <https://netzpolitik.org/?p=390138>

Seems we escaped out of the frying pan into the fire.

[1] https://www.wired.co.uk/article/tim-berners-lee-reclaim-the-web  
[2] https://en.wikipedia.org/wiki/Transport_Layer_Security  
[3] https://letsencrypt.org/  
[4] https://datatracker.ietf.org/doc/html/rfc6797  

P.S.: let alone the obligation to hug the certs every 3 months or your site will
go dark anyway.
