---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2010-01-07T13:09:31+00:00"
tags:
- apache
- Dropbox
- Email
- htaccess
- Netiquette
- Redirect
- RewriteEngine
- Ruby
title: hübsche Dropbox Links
type: post
url: /2010/01/hubsche-dropbox-links/
yourls_shorturl:
- http://s.mro.name/2
---
Weil [große Email Anhänge stinken][1], bietet sich z.B. die [Dropbox][2] als Dateiaustauschplatform an.

Einfach die Datei in den (lokalen) Dropbox/Public Ordner verfrachten, syncen lassen und per &#8222;Rechte Maustaste -> Copy Public Link&#8220; in die Mail kopieren.

### Redirect

Nun sind solche Links nicht besonders hübsch (Branding!, Branding!, CI! ruft die Marketingabteilung) und  spätestens wenn man's abtippen muß oder per Telephon weitersagt wird's gruslig.

Hat man aber eine eigene Website, lassen sich die [Dropbox Links aufhübschen][3] und eine Weiterleitung einrichten, dann wird aus

</code></pre>
http://dl.dropbox.com/u/1234567/BigDocument.pdf
</code></pre>

z.B.

</code></pre>
http://dropbox.mydomain.com/BigDocument.pdf
</code></pre>

und schon sind die Brandingfreunde happy.

### index.html

Geht es nicht um eine einzelne Datei sondern um mehrere, will man evtl. eine Übersicht in einer index.html haben. So eine Übersicht legt z.B. mein [ruby][4] Script [htmlizedb][5] an. Das Script will im [Terminal][6] in dem Verzeichnis gestartet werden, für das die Übersicht her soll.

### Redirect + index.html

Bei Webservern ist allgemein üblich, daß man den Dateinamen index.html im Link weglassen kann. Der Webserver nimmt dann an man meint selbige. Die Dropbox verhält sich aber in dieser Hinsicht nicht wie ein normaler Webserver, sondern will immer den kompletten Dateinamen.

Hat man aber bereits den Redirect Trick in Benutzung, läßt sich die Redirect Steuerdatei (`.htaccess`) aufbohren, daß der Redirect die index.html Angabe ergänzt:

<pre class="line-numbers"><code class="language-apacheconf">RewriteEngine on
# dropbox abbreviation:
# http://www.cimitan.com/blog/2008/09/17/htaccess-to-redirect-on-dropboxs-public-folder/#
# last path component has no dot -> redirect to index.html
RewriteRule ^(.+/)?([^./]+)/?$  http://dl.dropbox.com/u/1234567/$1$2/index.html [last,qsappend]
# everything else: redirect as is
RewriteRule ^(.+)$  http://dl.dropbox.com/u/1234567/$1  [last,qsappend]
</code></pre>

violá, nun wird von

<pre><code>http://dropbox.mydomain.com/FolderWithBigFiles
</code></pre>

automatisch auf den Dropbox Link

<pre><code>http://dl.dropbox.com/u/1234567/FolderWithBigFiles/index.html
</code></pre>

weitergeleitet.

Toll, nicht?

 [1]: http://friendfeed.com/akawee/10ab93b0/e-mail-missbrauch-teil-2-der-anhang-ist-oft-die
 [2]: http://friendfeed.com/webjuwelen/9ed5b1a9/skrupellose-werbung-fur-die-dropbox-dieser
 [3]: http://www.cimitan.com/blog/2008/09/17/htaccess-to-redirect-on-dropboxs-public-folder/#
 [4]: http://www.ruby-lang.org/de/
 [5]: http://gist.github.com/271223
 [6]: http://de.wikipedia.org/wiki/Bourne-again_shell
