---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2009-04-16T22:01:20+00:00"
tags:
- inspire
- robot
- TED
- war
title: War Robots
type: post
url: /2009/04/war-robots/
yourls_shorturl:
- http://s.mro.name/20
---

[original Talk at TED][1]

War robots in use at present. Examples, attitudes and possible implications.

Quite an eye opener, quite TED.

 [1]: http://www.ted.com/talks/view/id/504