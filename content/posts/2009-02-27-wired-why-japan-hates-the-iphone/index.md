---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2009-02-27T17:16:06+00:00"
tags:
- iPhone
title: wired | Why Japan Hates the iPhone
type: post
url: /2009/02/wired-why-japan-hates-the-iphone/
yourls_shorturl:
- http://s.mro.name/2a
---
[wired | Why Japan Hates the iPhone][1] – though [maybe being made up][2],

I find this quite interesting – in the end it's the closed, proprietary nature of the beast. No parallel apps, no apps that double Apple functionality (no video, audio etc.). I'm really curious about how the [G1 Android][3] does in Japan.

Seen at:

[Slashdot | Why Japan Hates the iPhone][4].

 [1]: http://blog.wired.com/gadgets/2009/02/why-the-iphone.html
 [2]: http://apple.slashdot.org/article.pl?sid=09/03/01/1358233&from=rss
 [3]: http://en.wikipedia.org/wiki/HTC_Dream
 [4]: http://apple.slashdot.org/article.pl?sid=09/02/27/144256&from=rss