---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-12-30T00:08:22+00:00"
tags:
- Bayern2
- cron
- Cronjob
- id3v2
- Linux
- Mitschnitt
- Radio
- Stream
- streamripper
- vServer
title: Radiomitschnitt per Rezept
type: post
url: /2009/12/radiomitschnitt-per-rezept/
yourls_shorturl:
- http://s.mro.name/1f
---
Seit längerem schneide ich mir interessante Sendungen aus dem (Internet-)Radio mit, vor allem [Hörspiele auf B2][1].

Bisher mußte dazu immer mein alter (Linux-)Rechner laufen und den Mitschnitt mußte ich händisch starten – wie unbequem. Oder man ist unterwegs und verpaßt den Mitschnitt. Wie ärgerlich.

Doch das ist ab jetzt vorbei, denn ab sofort schneidet mein vServer (läuft ja eh ständig) mit!

Und so geht's:

1. Ein Script auf dem vServer, das den Mitschnitt startet und beendet:

    <pre class="line-numbers"><code class="language-shell-session">#!/bin/sh

    # id3v2 scheint Bilder nicht zu mögen:
    # - http://ubuntuforums.org/showthread.php?t=1353213
    # - readonly: http://id3v2.cvs.sourceforge.net/viewvc/id3v2/id3v2/id3v2.cpp?revision=1.17&view=markup#l_610
    # - http://www.id3.org/id3v2.4.0-frames Kapitel4.14.
    # erfolglos: id3v2 --APIC "0\0image/jpeg\03hello\0`cat moby_dick.jpg`" $file

    if [ $# -lt 3 ]; then
      echo "rip audio streams to a local file."
      echo " "
      echo "Usage:"
      echo " "
      echo "  $0 stream marker duration id3v2_tags"
      echo "    stream     typically a URL, see 'man streamripper'"
      echo "    marker     filename prefix"
      echo "    seconds    how long will we dump"
      echo "    id3v2_tags optional, tag the download with id3v2"
      echo " "
      echo "Example:"
      echo "  $0 http://gffstream.ic.llnwd.net/stream/gffstream_w11a bayern2 3660"
      echo " "
      exit 1
    fi

    src=$1 ; shift
    basename=$1 ; shift
    seconds=$1 ; shift

    dst=$basename-`date --iso-8601=seconds`

    echo "dumping $seconds (sec) from $src to $dst ..."

    streamripper $src -u Mozilla -i -a $dst -l $seconds
    rm $dst.cue

    if [ $# -gt 0 ] ; then
      id3v2 "$@" $dst.mp3
    fi

    chmod a+r $dst.*

    echo done.
    </code></pre>

2. Ein Cronjob für jeden Mitschnitt:

    <pre class="line-numbers"><code class="language-shell-session">PATH=/bin:/usr/bin
    HOME=/home/USERNAME

    # Recorder

    rec_cmd=/home/USERNAME/bin/stream-rip
    rec_dir=/home/USERNAME/pub/recorder

    ON3=http://gffstream.ic.llnwd.net/stream/gffstream_w9a
    BAYERN1=http://gffstream.ic.llnwd.net/stream/gffstream_w10a
    BAYERN2=http://gffstream.ic.llnwd.net/stream/gffstream_w11a
    M945=http://stream.m945.mwn.de:80/m945-hq.mp3

    # B2 Krimihörspiel
    29 20 * * 3 $rec_cmd $BAYERN2 $rec_dir/b2-krimi 3660 -A "B2 Krimi"
    </code></pre>

 [1]: http://www.br-online.de/bayern2/hoerspiel-und-medienkunst/index.xml
