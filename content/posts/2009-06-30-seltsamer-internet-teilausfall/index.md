---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-06-30T12:32:48+00:00"
tags:
- Ausfall
- BKA
- Outage
- T-DSL
title: Seltsamer Internet-Teilausfall
type: post
url: /2009/06/seltsamer-internet-teilausfall/
yourls_shorturl:
- http://s.mro.name/1d
---
Momentan gibt's bei mir (via T-DSL / München) Probleme bei transatlantischen Zugriffen:

{{< figure  src="/wp-content/uploads/2009/06/nix-transatlantik-300x173.png" caption=".com: nix - .de: ok"  width="300"  height="173" >}}

Hat da wohl ein BKA Prakti falsch geklickt? Naja, immerhin keine [Stop Seite vom antipornographischen Schutzwall][1].

Nachtrag: Jetzt (14:51) geht's wieder.

 [1]: http://files.getdropbox.com/u/965005/stop.html