---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-03-14T20:46:03+00:00"
tags:
- Campusspeicher
- gentoo
- git
- vServer
title: vServer mit gentoo – oder doch besser ohne.
type: post
url: /2009/03/vserver-mit-gentoo-oder-doch-besser-ohne/
yourls_shorturl:
- http://s.mro.name/2m
---
Vor 4 Tagen habe ich mir den kleinsten [vServer][1] von meinen [Hosting Freunden vom Campusspeicher][2] geholt um einige [Git Repos][3] (v.a. die Email Backups) jenseits des DSL Flaschenhalses zu lagern.

Außerdem wollte ich einen [SSH-fähigen][4] Lagerplatz, da mir eine unverschlüsselte [FTP][5] Übertragung auf Dauer einfach unangenehm war.

<!--more-->Seither habe ich praktisch nichts anderes gemacht, als den Server immer wieder neu zu installieren, da ich leichtsinnigerweise als OS &#8222;gentoo minimal&#8220; genommen habe und sich der Feigling konstant weigert nach 

`emerge --update --newuse --deep world` nochmal auf die Beine zu kommen – wenn sich der [gcc nicht wegen Speichermangel gleich weigert zu kompilieren][6]. Woran das liegt – einigen Verdacht ja, aber keine Ahnung (vServer Laie, der ich bin).

Inzwischen hab ich die Faxen dicke und – [nachdem ich schon fast auf Ubuntu umgestiegen wäre][7] – begnüge ich mich vorerst mit einem veralteten OS – hauptsache Git geht und SSH ist einigermaßen dicht – also:

[Git Basis][8]:

</p> 

  1. KEIN `emerge --sync` oder `world`


  2. `echo "dev-util/git ~x86" >> /etc/portage/package.keywords`


  3. `echo "dev-util/git -gtk" >> /etc/portage/package.use`


  4. `emerge dev-util/git lftp setuptools<br />`


  5. SSH Konfiguration wie im [EUSERV gentoo vServer Blog von Jochen Schalanda][9] beschrieben (`groupadd ssh && useradd -g users -G wheel,ssh,portage -m myuser && passwd -d myuser` &#8230;) und evtl. noch zusätzlich den SSH Port von der 22 weg legen,
</ol> 

[Gitosis][10]:

</p> 

  1. `useradd -c "git version control" -m -n git`


  2. `passwd -d git`


  3. `su - git`


  4. git@localhost ~ $ `gitosis-init < /tmp/id_dsa.pub`


  5. git@localhost ~ $ `exit`
</ol> 

[Git-Daemon][11]:

</p> 

  1. `rc-update add git-daemon default`
</ol> 

So, das muß für's erste reichen.

 [1]: http://de.wikipedia.org/wiki/Server#Virtuelle_Server
 [2]: http://www.campusspeicher.de/
 [3]: http://de.wikipedia.org/wiki/Git
 [4]: http://de.wikipedia.org/wiki/Secure_Shell
 [5]: http://de.wikipedia.org/wiki/File_Transfer_Protocol
 [6]: http://hostingfu.com/article/compiling-with-gcc-on-low-memory-vps
 [7]: http://hostingfu.com/article/switched-gentoo-ubuntu
 [8]: http://www.kernel.org/pub/software/scm/git/docs/git.html
 [9]: http://blog.schalanda.name/archives/176-EUserv-vServer-Active-Installation-des-Grundsystems.html
 [10]: http://scie.nti.st/2007/11/14/hosting-git-repositories-the-easy-and-secure-way
 [11]: http://www.kernel.org/pub/software/scm/git/docs/git-daemon.html