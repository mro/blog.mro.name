---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2008-10-14T21:03:29+00:00"
tags:
- AppStore
- J2ME
title: …sold through handset manufacturers and network operators…
type: post
url: /2008/10/sold-through-handset-manufacturers-and-network-operators/
yourls_shorturl:
- http://s.mro.name/35
---
Just shows how desparately J2ME lacks a sales model a la Apple's AppStore: [Turn Your Phone into Babel Fish][1].

 [1]: http://www.wirelessweek.com/article.aspx?id=163654