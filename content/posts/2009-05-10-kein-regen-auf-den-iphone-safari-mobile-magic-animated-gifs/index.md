---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2009-05-10T20:11:57+00:00"
tags:
- iPhone
- Safari
title: 'Kein Regen auf dem iPhone // Safari Mobile Magic: (un-)Animated GIFs'
type: post
url: /2009/05/kein-regen-auf-den-iphone-safari-mobile-magic-animated-gifs/
yourls_shorturl:
- http://s.mro.name/3e
---
Wer schon mal versucht hat z.B. das [Regenradar von Wetteronline][1] auf dem iPhone anzugucken kennt das Problem:

Man sieht nur ein Standbild statt einem Daumenkino. Nach meinen [Erlebnissen mit .mobi Domains][2] gewarnt, förderte eine kurze Suche das Problem zu Tage: Wenn Animated GIFs eine [bestimmte Größe überschreiten][3], zeigt Safari Mobile tatsächlich nur den ersten Frame.

Also was tun? Eine HTML Seite bauen, die per Javascript die Einzelbilder durchrotiert: <http://mro.name/regen.html>

 [1]: http://www.wetteronline.de/dldlrad.htm
 [2]: http://blog.mro.name/2009/03/iphone-safari-mobi-magic/
 [3]: http://developer.apple.com/safari/library/documentation/AppleApplications/Reference/SafariWebContent/CreatingContentforSafarioniPhone/chapter_2_section_6.html