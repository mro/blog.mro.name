---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2015-05-11T06:42:55+00:00"
tags:
- chruby
- Email
- redmine
- Ruby
title: redmine & chruby (fetch emails)
type: post
url: /2015/05/redmine-chruby-fetch-emails/
yourls_fetching:
- 1
---

<pre class="line-numbers"><code class="language-sh">#!/bin/bash
# chruby needs bash
#
#
# redmine email import with chruby.
#
#
# Put this script into <redmine_dir>/script and a set crontab like
#   $ sudo -u www-data crontab -l
#   */15 * * * * <redmine_dir>/script/fetch-email.sh
#
cd "$(dirname "$0")/.."

log="log/$(basename "$0" .sh).log"
cat >> "$log" &lt;&lt;EOF

$(date --rfc-3339=seconds) $0
EOF

# https://github.com/postmodern/chruby
source /usr/local/share/chruby/chruby.sh
source /usr/local/share/chruby/auto.sh

# in case we use rbx in .ruby-version, we need to explicitly set ruby in
# script/fetch-email.sh due to
# https://github.com/rubinius/rubinius/issues/2916
chruby ruby || { echo "Failed to change ruby." 1>> "$log" && exit 1; }

bundle --version 1>/dev/null || { nice gem install bundle 1>> "$log" 2>> "$log" ; }
bundle check 1>/dev/null 2>/dev/null || { nice bundle install 1>> "$log" 2>> "$log" ; }

# http://www.redmine.org/projects/redmine/wiki/RedmineReceivingEmails
nice bundle exec rake redmine:email:receive_imap \
  RAILS_ENV="production" \
  host=<foo> \
  ssl=1 \
  port=993 \
  username=<bar> \
  password=<foobar> \
  unknown_user=accept \
  1>> "$log" \
  2>> "$log"
</code></pre>
