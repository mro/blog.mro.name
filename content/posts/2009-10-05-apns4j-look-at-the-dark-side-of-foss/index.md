---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-10-04T22:53:36+00:00"
tags:
- APNS
- apns4j
- iPhone
- Java
- Push
title: apns4j – look at the dark side of FOSS
type: post
url: /2009/10/apns4j-look-at-the-dark-side-of-foss/
yourls_shorturl:
- http://s.mro.name/1m
---
It's either trivial or harder than it looks – (at least) four stalled projects named apns4j:

* <http://sourceforge.net/projects/apns4j/> (zero files in svn)
* <https://apns4j.dev.java.net/source/browse/apns4j/> (zero *.java in svn, same ownername as above)
* <http://code.google.com/p/apns4j/> (zero files in svn, same ownername as above)
* <http://github.com/netmask/apns4j> (three commits)

I didn't look closer – maybe the last was gifted with a mature birth. The first three look like the project got stuck in choosing it's hoster.