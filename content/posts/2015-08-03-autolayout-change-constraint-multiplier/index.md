---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2015-08-03T06:42:40+00:00"
tags:
- Apple
- Autolayout
- iOS
- NSLayoutConstraint
- Objective C
title: 'Autolayout: change constraint multiplier'
type: post
url: /2015/08/autolayout-change-constraint-multiplier/
---
the [NSLayoutConstraint multiplier](https://developer.apple.com/library/ios/documentation/AppKit/Reference/NSLayoutConstraint_Class/#//apple_ref/occ/instp/NSLayoutConstraint/multiplier) property is read-only. But if you need to change it, you can replace the constraint with a modified clone like

<pre class="line-numbers"><code class="language-objc">@implementation NSLayoutConstraint(ChangeMultiplier)
// visal form center http://stackoverflow.com/a/13148012/349514
-(NSLayoutConstraint *)constraintWithMultiplier:(CGFloat)multiplier
{
  return [NSLayoutConstraint
    constraintWithItem:self.firstItem
    attribute:self.firstAttribute
    relatedBy:self.relation
    toItem:self.secondItem
    attribute:self.secondAttribute
    multiplier:multiplier
    constant:self.constant];
}
@end
</code></pre>

and replace it like

<pre class="line-numbers"><code class="language-objc">NSLayoutConstraint *c = [self.constraintToChange constraintWithMultiplier:0.75];
  [self.view removeConstraint:self.constraintToChange];
  [self.view addConstraint:self.constraintToChange = c];
  [self.view layoutIfNeeded];
</code></pre>
