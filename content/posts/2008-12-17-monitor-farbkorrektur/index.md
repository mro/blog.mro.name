---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2008-12-17T14:44:27+00:00"
tags:
- Farbkorrektur
- gentoo
- Iiyama
- MacBook
- Monitor
title: Monitor Farbkorrektur
type: post
url: /2008/12/monitor-farbkorrektur/
yourls_shorturl:
- http://s.mro.name/1x
---
ein dauerhaftes Ärgernis ist (für mich) die verhagelte Farbwiedergabe [meines Monitors][1].

Nach einem [ersten ernsthaften Einstellversuch im Juli][2] war ich relativ zufrieden – bis ich den Monitor per [DVI][3] an ein Macbook angeschlossen habe. Plötzlich waren da – auf demselben Monitor – zarte Farbabstufungen, wo vorher (per [VGA][4] am [gentoo][5] Rechner angeschlossen) scheinbar einfarbige Flächen waren.

Noch eklatanter fällt der Vergleich mit dem eingebauten Macbook Display aus.

Also: Die Einstellung taugt nix, ein neuer Anlauf:

1. Monitor Hardware und NVidia X Server Settings auf Fabrikeinstellung zurück,
2. Monitor Hardware Farbtemperatur auf sRGB (Einstellung &#8222;s&#8220;),
3. per NVidia X Server Settings:
4. zuerst alle Kanäle Gamma auf 0,9 und Kontrast auf -0.10,
5. dann Grün Kontrast auf -0.15

besser.

Zu Weihnachten wünsche ich mir allerdings irgendwann ein Einstellprogramm, das mir nacheinander etliche Testbilder mit Fragen a la &#8222;Sehen die die Farbabstufung?&#8220; präsentiert und am Schluß die Graka einstellt. Macht das nicht PhotoShop so?

 [1]: http://www.iiyama.com/de_DE/Product/category/2/product/29
 [2]: http://wiki.aladin-software.de/orga/monitor_calibration
 [3]: http://de.wikipedia.org/wiki/Digital_Visual_Interface
 [4]: http://de.wikipedia.org/wiki/VGA_(Anschluss)
 [5]: http://de.wikipedia.org/wiki/Gentoo_Linux