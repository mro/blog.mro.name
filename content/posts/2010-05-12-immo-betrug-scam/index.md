---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2010-05-12T10:20:42+00:00"
tags:
- Immo
- Nigeria
- Scam
title: Immo Betrug / Scam
type: post
url: /2010/05/immo-betrug-scam/
yourls_shorturl:
- http://s.mro.name/t
---
auf Wohnungssuche begegnet man lustigen Sachen. Da war dieses extrem billige Angebot (2 Zi, 80m² für
550€) mit [spannender Adresse][1], aber fragen kostet ja nix.

Die erste Mail der Anbieterin war dann:

> Hi,
>
> Thanks for your interest. The apartment is still available. I moved recently with my job in
> London, United Kingdom, but the rent is high here, I decided to offer to rent the apartment I have
> in Munchen . I rented the apartment for maximum 10 years &#8230; This is the period that I have a
> contract here, but I can rent it for a shorter period also. I own the ground and is exactly as in
> the photos. The rent for 1 month is 550 EUR and does not include all the utilities you (water,
> electricity, Internet, cable). You can enter the apartment on the same day when receiving the keys
> .. The only problem is that i had to move with my job to United Kingdom, London where i am now and
> i left the keys and contract already signed by me at a company called Rent.com and they will
> handle the payment and delivery for us. If you want to know more about how this deal can work
> please get back to me ASAP and i will send you the details step by step. Thank you and hope to
> hear back from you.
>
> My German is limited at just a few words so i wish a lot to continue our conversation in English.
>
dann ging's 2x per Mail hin und her,

> Hello,
>
> If we decide to proceed with this transaction, I will have to contact Rent.com and provide them
> all the necessary information, so they can start the process right away. I will need your full
> name and address for that. You will receive a notification from Rent.com shortly after that,
> together with all the instructions to follow and the invoice as well.
>
> Regarding the payment, you will be instructed to deposit the money to a Rent.com account. They
> will hold and insure your money until you check the apartment and decide if you want take it or
> not. That is how their buyer protection policy works. As far as my concerns, I will be glad to
> know that Rent.com has the possession of the money during the delivery period. That is my
> insurance.
>
> As soon as the funds have been deposited into their account, they will immediately start the
> shipping process.
>
> The keys and rental contract will be delivered at your address in no more than 3 working days. You
> will be given a ten days inspection period from the day you receive the keys and contract at home.
> If you decide to hold the apartment, then you will have to authorize Rent.com to release the funds
> to me, and the transaction will be completed. If you will not be satisfied with the apartment, you
> will be able to send the keys and contract back through the same service and ask Rent.com to
> return the funds to you.
>
> Through I am sure you will love the apartment, it is good to know that you do have this second
> option available. If you wish to proceed with renting the apartment, please provide me your full
> name and address so I can initiate the deal through Rent.com right away.
>
> I am looking forward to hearing from you.
>
> Regards,

die Anbieterin wollte nicht anrufen aber eine Zahlungsanweisung kam, die mich dann richtig stutizg
machte, weil

1. der ganze Ablauf eh schon recht seltsam war,
2. deren Absenderemail &#8222;rentcom@consultant.com&#8220; nicht mal von rent.com kam,
{{< figure  src="/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-12-um-12.11.41.png" caption=""  width="251"  height="300" >}}
3. rent.com außerhalb USA gar nix macht,
4. die [Einlieferungs-IP][3] der Zahlungsanweisung die gleiche wie die der Mails von der Anbieterin war,
5. ja sogar &#8222;In-Reply&#8220; auf die vorherigen Mails,
{{< figure  src="/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-12-um-12.08.37.png" caption=""  width="300"  height="92" >}}
6. das Geld an jemanden anderen gehen sollte,
{{< figure  src="/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-12-um-12.24.10.png" caption=""  width="300"  height="131" >}}
7. an der [Wohnungs-Adresse][1] wohl gar keine Wohnungen sind,
8. ein [Spezl][5] mich auf die [Warnungen bei rent.com][6] und einen [Bericht bei der BBC][7] hinwies.

Nach meiner Frage nach dem Namen des Hausmeisters war dann Funkstille in London.

Zu guter letzt hat mich [Immoscout24 heute nochmal per Mail vor Betrügern gewarnt][8].

 [1]: http://maps.google.de/maps?f=q&source=s_q&hl=de&geocode=&q=karolinenplatz+3,+m%C3%BCnchen&sll=51.151786,10.415039&sspn=21.085965,36.606445&ie=UTF8&hq=&hnear=Karolinenplatz+3,+M%C3%BCnchen+80333+M%C3%BCnchen,+Bayern&t=h&z=18
 [3]: http://aruljohn.com/info/howtofindipaddress/#aol
 [5]: http://aufgehts.wordpress.com/2010/05/12/augen-auf-beim-mietwohnungs-suchen/
 [6]: http://www.rent.com/company/security/
 [7]: http://www.bbc.co.uk/blogs/watchdog/2008/10/flathunters_transfer_trap.html
 [8]: http://aktuell.immobilienscout24.de/u/gm.php?prm=BEAgxe2VrU_116434665_159087_80916