---
author: Marcus Rohrmoser
categories:
- de
- offtopic
- seenontheweb
date: "2015-04-27T06:42:11+00:00"
tags:
- '#Qualitätsjournalismus'
- abloprox
- Datenschutz
- Hygiene
- Requestpolicy
- Verlag
title: '#Qualitätsjournalismus und Datenschutz'
type: post
url: /2015/04/qualitaetsjournalismus-und-datenschutz/
yourls_fetching:
- 1
---
sz.de<sup>*)</sup> denkt sich wohl &#8222;_was interessiert mich der Datenschutz meiner Besucher_&#8220;:

{{< figure src="/wp-content/uploads/2015/04/Bildschirmfoto-2015-04-23-um-17.08.54.png" caption="an wen verpetzt mich sz.de alles?" >}}

Hinweise versteckt weit unten im [Impressum](http://www.sz.de/1.555530) – allerdings wird selbst dort geschnüffelt und nicht vorher gefragt.

Danke [Requestpolicy][2] und [ablopac][3], so bleiben mir die Schnüffler vom Hals.

<sup>*)</sup> andere große Verlage sind ähnlich, das ist im _#Qualitätsjournalismus_ leider üblich.

 [2]: https://requestpolicycontinued.github.io/
 [3]: http://purl.mro.name/wpad