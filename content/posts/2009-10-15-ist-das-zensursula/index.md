---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-10-15T21:59:04+00:00"
tags:
- DNS
- Root-Nameserver
- T-Online
- zensursula
title: Ist das zensursula?
type: post
url: /2009/10/ist-das-zensursula/
yourls_shorturl:
- http://s.mro.name/1r
---
Seit heute ca. 15:30 sind die [Root-Nameserver][1] für mich (T-Online Kunde) nicht mehr erreichbar:

<pre class="line-numbers"><code class="language-shell-session">$ nslookup google.com - j.root-servers.net
Server:   j.root-servers.net
Address:  192.58.128.30#53

Non-authoritative answer:
*** Can't find google.com: No answer
</code></pre>

Ist das die Vorbereitung der [China Wochen][2] bei T-Online & Co.? Bei Gelegenheit mal ausprobieren: [eigene bind Nameserver Instanz][3].

**[Update]**: Am rosa Riesen scheint's nicht zu liegen – das Problem liegt wohl in [Layer 8][4].

<pre class="line-numbers"><code class="language-shell-session">$ nslookup google.com - resolver1.opendns.com
Server:   resolver1.opendns.com
Address:  208.67.222.222#53

Non-authoritative answer:
Name: google.com
Address: 74.125.67.100
Name: google.com
Address: 74.125.45.100
Name: google.com
Address: 74.125.53.100
</code></pre>

In Zukunft also nicht mehr mit den Root-Nameservern, sondern einem vom [CCC][5] empfohlenen.

 [1]: http://de.wikipedia.org/wiki/Root-Nameserver
 [2]: http://www.zdnet.de/news/wirtschaft_sicherheit_security_geheime_technische_details_zum_internetzensurgesetz_aufgetaucht_story-39001024-41515822-1.htm
 [3]: http://www.zdnet.de/sicherheit_in_der_praxis_sperre_von_freien_dns_servern_so_umgeht_man_die_blockade_story-39001543-41502966-1.htm
 [4]: http://de.wikipedia.org/wiki/OSI-Modell
 [5]: http://ccc.de/censorship/dns-howto/#dnsserver
