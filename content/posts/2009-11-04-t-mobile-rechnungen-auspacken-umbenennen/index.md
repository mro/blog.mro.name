---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-11-04T17:50:19+00:00"
tags:
- GnuPG
- gpg
- Ruby
- T-Mobile
title: T-Mobile Rechnungen auspacken und umbenennen
type: post
url: /2009/11/t-mobile-rechnungen-auspacken-umbenennen/
yourls_shorturl:
- http://s.mro.name/24
---
über [T-Mobile RechnungOnline][1] kann man sich die Rechnung als [gpg][2]-verschlüsselten Anhang per
Mail schicken lassen – incl. Einzelverbindungsnachweis. Und obwohl's nur 1x im Monat kommt, ist das
Auspacken und Umbenennen in sinnvolle Dateinamen eine lästige Routinearbeit.

Also – automatisieren. Das folgende Ruby-Script erledigt das, nachdem der Rechnungsanhang in einen
der Ordner &#8222;Downloads&#8220; oder &#8222;Desktop&#8220; gelegt wurde:

<pre class="line-numbers"><code class="language-ruby">#!/usr/bin/ruby

dst="#{ENV['HOME']}/Documents/tmobile"

# 1. pull together all pgp crypted find Rechnung_15.10.2009.zip.pgp in Desktop, Downloads, local dir
["#{ENV['HOME']}/Downloads", "#{ENV['HOME']}/Desktop", dst].each do |dir|
  Dir.foreach(dir) do |file|
    m = /Rechnung_(\d{2})\.(\d{2})\.(\d{4})\.zip(\.pgp|\.gpg)?/.match file
    if m
      s = system "mv #{dir}/#{file} #{dst}/#{m[3]}-#{m[2]}-#{m[1]}-bill.zip#{m[4]}"
      s = system "gpg #{dst}/#{m[3]}-#{m[2]}-#{m[1]}-bill.zip#{m[4]}" if m[4]
      system "rm #{dst}/#{m[3]}-#{m[2]}-#{m[1]}-bill.zip#{m[4]}" if s && m[4]
      s = system "unzip #{dst}/#{m[3]}-#{m[2]}-#{m[1]}-bill.zip"
      system "rm #{dst}/#{m[3]}-#{m[2]}-#{m[1]}-bill.zip" if s
      puts "de-crypted & unpacked #{dst}/#{m[3]}-#{m[2]}-#{m[1]}-bill.zip"
    end
  end
end

# 2. rename unpacked files accordingly
Dir.foreach(dst) do |file|
  m = /^Rechnung_(\d{4})_(\d{2})_(.+)$/.match file
  system "mv #{dst}/#{file} #{dst}/#{m[1]}-#{m[2]}-bill-#{m[3]}" if m
  m = /^Einzelverbindungsnachweis_(\d{4})_(\d{2})_(.+)$/.match file
  system "mv #{dst}/#{file} #{dst}/#{m[1]}-#{m[2]}-evn--#{m[3]}" if m
  m = /^Rechnung_(\d{5,20})_(\d{2})_(\d{4})(.+)$/.match file
  system "mv #{dst}/#{file} #{dst}/#{m[3]}-#{m[2]}-bill-#{m[1]}#{m[4]}" if m
  m = /^Einzelverbindungsnachweis_(\d{5,20})_(\d{2})_(\d{4})(.+)$/.match file
  system "mv #{dst}/#{file} #{dst}/#{m[3]}-#{m[2]}-evn--#{m[1]}#{m[4]}" if m
end
</code></pre>

 [1]: http://www.t-mobile.de/faq/1,1951,18-_,00.html
 [2]: http://www.gnupg.org/
