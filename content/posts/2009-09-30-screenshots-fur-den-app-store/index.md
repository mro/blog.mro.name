---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2009-09-30T08:24:05+00:00"
tags:
- App Store
- ImageMagick
- iPhone
title: Screenshots für den App Store
type: post
url: /2009/09/screenshots-fur-den-app-store/
yourls_shorturl:
- http://s.mro.name/1t
---
Der App Store möchte gerne Screenshots ohne Statuszeile und im TIFF Format. Da das immer gleich sein soll bietet sich [ImageMagick][1] an:

</code></pre>
$ convert MyScreenshot.png -compress None -crop '320x460-0+20' MyScreenshot.tiff
</code></pre>

 [1]: http://www.imagemagick.org/