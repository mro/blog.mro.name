---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-02-18T12:19:22+00:00"
tags:
- FTP
- lftp
- OS X
title: lftp OSX dmg
type: post
url: /2009/02/lftp-osx-dmg/
yourls_shorturl:
- http://s.mro.name/3g
---
Da's für [lftp][1] zwar einen [Darwinport][2] gibt, aber kein [Disk Image][3] (was unbedarfte Seelen durchaus von der Installation abhalten kann), habe ich ein solches gebaut und hier zum

**[Download lftp-3.7.6-1.dmg](/wp-content/uploads/2009/02/lftp-376-1.dmg)**

verlinkt. Leider ist das Binary nicht statisch gelinkt, drum hat das ganze wenig Sinn. Details siehe unten im &#8222;Nachtrag&#8220;.

Wie und wo genau das sich dann installiert weiß ich nicht – ich selbst benutze ja den Darwinport

## Zur Benutzung nur 2 kurze Anmerkungen:

Ich finde es bequem (wenn auch von Sicherheitsseite nicht die reine Lehre) die FTP Zugangsdaten zu
den meistbenutzten Servern in der
[`$HOME/.netrc`](http://manpages.unixforum.co.uk/man-pages/unix/solaris-10-11_06/4/netrc-man-page.html)
abzulegen.

**Vorsicht:** die Datei muß folgende Rechte haben (bzw. nicht haben):

<pre class="line-numbers"><code class="language-shell-session">$ chmod 600 $HOME/.netrc
</code></pre>

Außerdem habe ich folgende Einstellungen in der `$HOME/.lftp/rc` Konfiguration:

<pre class="line-numbers"><code class="language-shell-session">set ftp:list-options -a
set net:limit-total-rate 0,15360&
</code></pre>

Viel Spaß damit!

## Nachtrag

Leider funktioniert das so nicht, da das Binary nicht statisch gelinkt ist – was leider nicht geht:

* <http://dev.ultravelours.com/ports/enblend/index.html#static-builds>
* <http://el-tramo.be/guides/darcs-static-osx>
* <http://developer.apple.com/qa/qa2001/qa1118.html>

Noch dazu sind die dynamisch gelinkten Macport Libs sämtlich auch als Teil von OSX selbst vorhanden. Sehr ärgerlich.

 [1]: http://de.wikipedia.org/wiki/Lftp
 [2]: http://lftp.darwinports.com/
 [3]: http://de.wikipedia.org/wiki/Disk_Image_%28Apple%29
