---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2011-01-13T14:03:21+00:00"
tags:
- AlcoCalc
- App Store
- Blutalkoholkonzentration
- iPhone
- Projects
- Promille
- Promillerechner
- Widmark Formel
title: "'AlcoCalc' iPhone App online! 📱"
type: post
url: /2011/01/alcocalc-iphone-app-online/
yourls_shorturl:
- http://mro.name/41
---
Hurra! Vor einigen Tagen ist die erste Version des Promillerechners im 

{{< figure src="/wp-content/uploads/2011/01/appstore.png" caption="AlcoCalc App Store Link" width="116" height="40" >}}

{{< figure src="/wp-content/uploads/2011/01/alcocalc.curve_-300x200.png" caption="Promillekurve" width="300" height="200" >}}

erschienen. Die App merkt sich was Du wann getrunken hast und schätzt nach der [Widmark Formel][2] grob den aktuellen und fallenden Promillewert ab:
 
{{< figure  src="/wp-content/uploads/2011/01/alcocalc.curve_-300x200.png" caption="Promillekurve"  width="300"  height="200" >}}

{{< figure  src="/wp-content/uploads/2011/01/alcocalc.drinks-200x300.png" caption="Getrunkenes"  width="200"  height="300" >}}

{{< figure  src="/wp-content/uploads/2011/01/alcocalc.edit_-200x300.png" caption="Getränk anlegen"  width="200"  height="300" >}}

 [1]: http://itunes.apple.com/de/app/alcocalc/id383301852
 [2]: http://de.wikipedia.org/wiki/Blutalkoholkonzentration#Widmark-Formel
 [3]: http://blog.mro.name/wp-content/uploads/2011/01/alcocalc.curve_.png
 [4]: http://blog.mro.name/wp-content/uploads/2011/01/alcocalc.drinks.png
 [5]: http://blog.mro.name/wp-content/uploads/2011/01/alcocalc.edit_.png
