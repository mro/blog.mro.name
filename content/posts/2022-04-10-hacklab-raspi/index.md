---
title: 'Jugend hackt Lab – special Kreatives (digitales) Gestalten in der offenen Werkstatt'
url: /2022/04/hacklab-special-raspi/
draft: false
categories:
#- 🇩🇪
- de
date: 2022-04-10 11:21:17+02:00
tags:
- Jugendhackt
- HackLab
- Gestaltung
- Raspi
- Workshop
- Q3
- Traunstein
- CMW
type: post
author: Marcus Rohrmoser
---

Workshop für Jugendliche ab 12 Jahre

Das Jugend Hackt Lab in Traunstein bietet Raum und Unterstützung, sich kreativ auszuleben und Neues zu entdecken. Im Mittelpunkt steht jeden letzten Freitag im Monat kreatives (digitales) gestalten in einer offenen Werkstatt.
Diesmal großartige Raspberry-Pi-Projekte! Der Raspberry Pi ist das digitale Werkzeug für DIY-Projekte und ideal für Einsteiger. Kein Vorwissen über Codierung oder Verkabelungen ist notwendig, das Experimentieren mit dem Raspberry Pi steht im Mittelpunkt und soll Jugendlichen eine Chance geben durch den Zugang zu neuen Technologien eine offene, gerechte und solidarische Gesellschaft mitzugestalten.

[Programm Chiemgauer Medienwochen, Fr, 8. April
2022](https://web.archive.org/web/20220409161237/https://chiemgauer-medienwochen.de/programm/#fancy-title-164)

[Hacklab Kalender](http://jugendhacktlab.qdrei.info/kalender)
