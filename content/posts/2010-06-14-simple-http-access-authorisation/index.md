---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2010-06-14T09:56:46+00:00"
tags:
- apache
- authentication
- authorisation
- Basic Authentication
- Cram
- htaccess
- HTTPS
- mod_rewrite
- rest
- restful
- RewriteCond
- RewriteRule
title: Simple HTTP Access Authorisation
type: post
url: /2010/06/simple-http-access-authorisation/
yourls_shorturl:
- http://s.mro.name/27
---
sometimes you may want to lock down [RESTful APIs][1] or plain HTTP GET resources for [authorised access][2] by your own client software only, without requiring [authentication][3]. You don't know who (not authenticated), but you know she may access (is authorised).

If the server has a valid [SSL certificate][4] based on a root certificate pre-installed on the iPhone among the simplest ways to do it are:

* [HTTP Basic Authentication][5] with static username + password. This requires just a [`.htaccess` configuration setting][6] and you're done.
* send a custom [HTTP Request Header][7] with a secret token, also just a [`.htaccess`](http://httpd.apache.org/docs/2.0/mod/mod_rewrite.html) [rewrite][8] setting required:

<pre class="line-numbers"><code class="language-apacheconf">RewriteEngine On
RewriteCond %{HTTP:My-Secret-Token} !=WRdsWXwwTZjEIRrgD5tODVf0U
RewriteRule ^.*$ - [forbidden,last]
# Test: $  curl --header "My-Secret-Token:WRdsWXwwTZjEIRrgD5tODVf0U" http://myserver.example.com/demo/
</code></pre>

Decompiling an App may raise the bar high enough though hard-coded secrets surely aren't bulletproof Secret Service grade quality. If you don't want the password or secret token as literal string inside the App, synthesize it at runtime.

If your transport channel isn't confidential (e.g. plain HTTP, not HTTPS) you might think about [Digest Authentication][9] or a custom implemented [CRAMish][10] mechanism which I will not go into in this post.

P.S.: [Here are some really nice `.htaccess` examples][11].

 [1]: http://en.wikipedia.org/wiki/Representational_State_Transfer
 [2]: http://en.wikipedia.org/wiki/Authorization
 [3]: http://en.wikipedia.org/wiki/Authentication
 [4]: http://httpd.apache.org/docs/2.0/ssl/ssl_faq.html
 [5]: http://en.wikipedia.org/wiki/Basic_access_authentication
 [6]: http://httpd.apache.org/docs/2.0/mod/core.html#authtype
 [7]: http://www.w3.org/Protocols/HTTP/HTRQ_Headers.html
 [8]: http://httpd.apache.org/docs/2.0/mod/mod_rewrite.html
 [9]: http://en.wikipedia.org/wiki/Digest_access_authentication
 [10]: http://en.wikipedia.org/wiki/CRAM-MD5
 [11]: http://www.evolt.org/ultimate_htaccess_examples
