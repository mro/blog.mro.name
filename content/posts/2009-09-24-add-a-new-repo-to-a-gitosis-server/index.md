---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-09-24T21:21:54+00:00"
tags:
- git
- gitosis
title: Add a new repo to a gitosis server
type: post
url: /2009/09/add-a-new-repo-to-a-gitosis-server/
yourls_shorturl:
- http://s.mro.name/1z
---
Another quick recipe:

1. create a local repo: `git init`
2. add it to the gitosis conf (`git pull`, edit gitosis.conf, `git commit`, `git push`)
3. `git remote add origin git@example.com:repository.git`
4. `git push --force --all`