---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-07-20T14:11:56+00:00"
tags:
- Benchmark
- Image
- iPhone
- UIImage
title: Image nerdery
type: post
url: /2009/07/image-nerdery/
yourls_shorturl:
- http://s.mro.name/12
---
A quick comparison how fast images load on the iPhone using a simple [UIImage imageWithContentsOfFile:][1]. Each image is 8-Bit RGB without Alpha:

<table border="0">
  <tr>
    <th>
      Filename
    </th>
    
    <th>
      Bytes
    </th>
    
    <th>
      msec
    </th>
  </tr>
  
  <tr>
    <td>
      img-10000&#215;50.png
    </td>
    
    <td align="right">
      224193
    </td>
    
    <td align="right">
      283
    </td>
  </tr>
  
  <tr>
    <td>
      img-50&#215;10000.png
    </td>
    
    <td align="right">
      200397
    </td>
    
    <td align="right">
      643
    </td>
  </tr>
  
  <tr>
    <td>
      img-900&#215;600.png
    </td>
    
    <td align="right">
      879483
    </td>
    
    <td align="right">
      623
    </td>
  </tr>
  
  <tr>
    <td>
      img-30000&#215;100.png
    </td>
    
    <td align="right">
      727709
    </td>
    
    <td align="right">
      1222
    </td>
  </tr>
  
  <tr>
    <td>
      img-150&#215;20000.png
    </td>
    
    <td align="right">
      911923
    </td>
    
    <td align="right">
      2218
    </td>
  </tr>
</table>

Looks like the horizontal strip is the winner when it comes to loading speed.

 [1]: http://developer.apple.com/iphone/library/documentation/UIKit/Reference/UIImage_Class/Reference/Reference.html#//apple_ref/occ/clm/UIImage/imageWithContentsOfFile: