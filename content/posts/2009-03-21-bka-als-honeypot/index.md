---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-03-21T13:11:04+00:00"
tags:
- BKA
- Sicherheit
title: BKA als Honeypot
type: post
url: /2009/03/bka-als-honeypot/
yourls_shorturl:
- http://s.mro.name/3x
---
[&#8222;Spiegel&#8220;: Innenministerium stoppt Überwachung der BKA-Seite][1] – nach dem Motto, &#8222;wer bei uns vorbeiguckt _muß_ kriminell sein.&#8220; oder wie ist das zu verstehen?

Die sind ja richtig lustig.

Was wollen die mit den IPs? Das soll eine _&#8222;effiziente Ermittlungsmaßnahme&#8220;_ sein? Die spinnen, die Römer.

 [1]: http://www.heise.de/newsticker/Spiegel-Innenministerium-stoppt-Ueberwachung-der-BKA-Seite--/meldung/134954