---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-06-26T14:08:23+00:00"
tags:
- Marketingbla
- Sprache
title: Musikalische Abrechnungsregeln
type: post
url: /2009/06/musikalische-abrechnungsregeln/
yourls_shorturl:
- http://s.mro.name/r
---
Momentan recherchiere ich gerade zum Thema &#8222;Zeiterfassung&#8220; — und bin dabei auf ein Beispiel wie &#8222;Kompetenz in Sprache&#8220; nicht aussieht gestoßen.

Aber [die hier][1] werben ja auch mit dem Spruch &#8222;Kompetenz in Sachen Zeit&#8220;:

{{< figure  src="/wp-content/uploads/2009/06/bild-51-300x122.png" caption="biologisch abbaubare Abrechnungsregeln (siehe Seite 3)"  width="300"  height="122" >}}

&#8222;Abrechnungsregeln auf mathematischer Basis&#8220; im Kernprogramm — dann ist ja gut.

 [1]: http://b-solution.de/