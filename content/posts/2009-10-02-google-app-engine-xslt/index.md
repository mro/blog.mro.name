---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-10-02T13:47:46+00:00"
tags:
- GAE
- Google App Engine
- Java
- Xalan
- XSLT
title: Google App Engine + XSLT
type: post
url: /2009/10/google-app-engine-xslt/
yourls_shorturl:
- http://s.mro.name/o
---
[Google App Engine (java)][1] doesn't do [XSLT transformations][2] out of the box. Using a [Transformer][3] like

<pre class="line-numbers"><code class="language-java">TransformerFactory.newInstance().newTransformer(new StreamSource(...)).transform(...);
</code></pre>

yields a

<pre class="line-numbers"><code class="language-log">Nested in javax.servlet.ServletException: java.lang.NoClassDefFoundError: com.sun.org.apache.xalan.internal.xsltc.runtime.BasisLibrary is a restricted class. Please see the Google App Engine developer's guide for more details.:  
java.lang.NoClassDefFoundError: com.sun.org.apache.xalan.internal.xsltc.runtime.BasisLibrary is a restricted class. Please see the Google App Engine developer's guide for more details.
</code></pre>

These google groups posts <http://code.google.com/p/googleappengine/issues/detail?id=1452> and <http://groups.google.com/group/google-appengine-java/browse_thread/thread/9c9fb88e1b175395> lead into the right direction but didn't completely solve my problem. I added `xalan.jar` and `serializer.jar` from xalan-2.7.1 to `war/WEB_INF/lib` as suggested, but **didn't change** the TransformerFactory setup.

This got me the WARNING log entries
<pre class="line-numbers"><code class="language-log">Failed calling setMethod method` and `Failed calling setIndent method
</code></pre>

This [cyrillc blog-post][4] lead me to removing all `<xsl:output/>` statements from the transformation and voilá – the trafo ran fine!

The whole procedure in short:

* add `xalan.jar` and `serializer.jar` to `war/WEB_INF/lib`,
* remove `<xsl:output/>` from the stylesheet.

 [1]: http://code.google.com/intl/en-GB/appengine/docs/java/
 [2]: http://www.w3.org/TR/xslt
 [3]: http://java.sun.com/j2se/1.5.0/docs/api/javax/xml/transform/Transformer.html
 [4]: http://blog.ad.by/2009/07/xslt-google-app-enginejava.html
