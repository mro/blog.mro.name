---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-11-06T00:09:28+00:00"
tags:
- CAcert
- Email
- S/MIME
- Thawte
- X.509
title: Email Sicherheit II – neue Zertifikate
type: post
url: /2009/11/email-sicherheit-ii-neue-zertifikate/
yourls_shorturl:
- http://s.mro.name/2o
---
Nachdem [Thawte die Lust verloren hat kostenlose Email Zertifikate auszugeben][1], bin ich eben zu [CAcert][2] gewechselt. Schien mir sogar noch einfacher als [damals bei Thawte][3].

Ein kleiner Wermutstropfen ist, daß [CAcert Zertifikate (leider) noch selten von Haus aus als vertrauenswürdig eingestuft werden][4].

P.S.: Thunderbird zickt ein wenig wegen den Root-Zertifikate von CAcert, aber folgt man der [Anleitung von CAcert][5] ist das auch fix vom Tisch.

 [1]: http://www.heise.de/security/meldung/Thawte-stellt-Web-of-Trust-fuer-kostenlose-SSL-Zertifikate-ein-822145.html
 [2]: https://www.cacert.org/index.php?id=1
 [3]: http://blog.mro.name/2009/03/email-sicherheit/
 [4]: http://de.wikipedia.org/wiki/CAcert#Vertrauensw.C3.BCrdigkeit
 [5]: http://wiki.cacert.org/ThunderBird