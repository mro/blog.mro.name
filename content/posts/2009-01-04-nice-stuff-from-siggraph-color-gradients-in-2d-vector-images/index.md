---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2009-01-04T00:59:51+00:00"
tags:
- SVG
title: 'Nice stuff from SIGGRAPH: Color gradients in (2D) vector images.'
type: post
url: /2009/01/nice-stuff-from-siggraph-color-gradients-in-2d-vector-images/
yourls_shorturl:
- http://s.mro.name/c
---

<http://www.dailymotion.com/video/x55mj2_diffusion-curves_tech> 

[Diffusion Curves: A Vector Representation for Smooth-Shaded Images](http://artis.imag.fr/Publications/2008/OBWBTS08/)

Sadly this doesn't work well (rather: not at all) with SVG.
