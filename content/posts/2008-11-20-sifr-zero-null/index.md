---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2008-11-20T04:43:35+00:00"
tags:
- Ethymologie
- sifr
title: Sifr – Zero – Null
type: post
url: /2008/11/sifr-zero-null/
yourls_shorturl:
- http://s.mro.name/30
---
Als ich eben nachgucken wollte, ob sifr (arabisch) wirklich 0 bedeutet und auch in dieser Schreibweise transkribiert wird, bin ich außer auf einen

[netten Wikipedia Artikel über die Ethymologie der 0][1] noch auf einen

[erhellenden Aufsatz über Zählweisen und die 0 als &#8222;übernatürliche&#8220; Zahl][2]

gestoßen.

Soso. Fibonacci haben wir also das Dezimalsystem in Europa zu verdanken. Im Deutschen wurde aus der 0 (sifr) gleich der Name für die &#8222;Buchstaben&#8220; des Zeichensystems – Ziffer.

Und zu zählen beginnen wir am besten bei 0 und hören vor Intervallende auf. Das wird wohl noch ein wenig dauern, bis es sich im Alltag durchgesetzt hat. Aber die Argumentation ist bestechend.

Bis bald bei [sifr.mobi][3] (man weiß ja nie wozu diesen kurzen Domainnamen mal gut sind).

Alf shukr wa shukr.

 [1]: http://en.wikipedia.org/wiki/0_(number)#Etymology
 [2]: http://www.cs.utexas.edu/users/EWD/ewd08xx/EWD831.PDF
 [3]: http://sifr.mobi