---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-09-15T20:18:50+00:00"
tags:
- Device ID
- iPhone
title: Investigate iPhone Device ID
type: post
url: /2009/09/investigate-iphone-device-id/
yourls_shorturl:
- http://s.mro.name/1u
---
Just to have it quickly at hand next time when adding a new tester.

Get the device ID:

<http://www.innerfence.com/howto/find-iphone-unique-device-identifier-udid?mail_to=blog%40mro.name>

Install the App on the tester's device:

[http://www.innerfence.com/howto/install-iphone-application-ad-hoc-distribution?app\_name=DemoApp.ipa&help\_mail=blog%40mro.name&provision\_file=DemoApp\_beta.mobileprovision&zip_file=DemoApp.ipa][1]

 [1]: http://www.innerfence.com/howto/install-iphone-application-ad-hoc-distribution?app_name=DemoApp.ipa&help_mail=blog%40mro.name&provision_file=DemoApp_beta.mobileprovision&zip_file=DemoApp.ipa