---
title: 'NLnet NGI0 Grant for #Seppo'
url: /2022/12/nlnet-seppo/
draft: false
categories:
- en
date: 2022-12-02T09:39:12+0100
tags:
- CGI
- grant
- NLnet
- Seppo
type: post
author: Marcus Rohrmoser
---

I am happy to announce that #Seppo received [Grant 2022-08-141](https://nlnet.nl/project/Seppo/) from NLnet and the 🇪🇺 NGI Zero Entrust Fund to become reality in 2023.

All development will happen at [seppo.social/sourcecode](https://seppo.social/sourcecode/), I'll report progress on [seppo.social/blog](https://seppo.social/blog/) and provide a test and demo to be investigated and stressed by anyone as soon as possible.

All in all it's planned like this:

# Memorandum of Understanding

Number: 2022-08-141

"#Seppo!" Project

The parties:

**Stichting NLnet**, domiciled in Science Park 400, 1098 XH Amsterdam, The Netherlands, referred to as "**NLnet**" in this document, represented by **Bob Goudriaan**,

and

**Marcus Rohrmoser**, an individual domiciled at \<redacted\> in Germany

given that:

* **NLnet** has the mission _"to promote the exchange of electronic information and all that is related or beneficial to that purpose"_.
* **NLnet** manages the _NGI0 Entrust_ fund, a fund dedicated to **open technologies** that improve **privacy** and trustworthiness\*\*. The fund was established with financial support from the European Commission's **Next Generation Internet** programme, under the aegis of DG Communications Networks, Content and Technology under grant agreement No 101069594.
* **NLnet** collaborates with a number of other organisations and experts within _NGI0 Entrust_ to help improve the quality, sustainability and adoption of projects in a structured way.
* In 1989, Sir Tim Berners-Lee invented the web as a ‘vague but exciting’ idea where individuals connect. Today it is dominated by some billionaire-run platforms that set arbitrary rules and lock the public in.
* The “fediverse” recently became a viable open alternative, where many social media mini- platforms interconnect to form a world-wide social web.
* The #Seppo! project adds a new tool to the fediverse that experiments with a simple approach to self-hosting, aiming for small-scale instances with easily customisable interfaces.
* **Marcus Rohrmoser** is an experienced software developer and avid sustainability advocate that developed the publishing concept of \#Seppo!; having individual agency and permacomputing at the heart.
* **Marcus Rohrmoser** has expressed the intention to dedicate significant private time to contribute to self-reliant and responsible civic publication by developing the self-contained #Seppo!, creating encouraging instructions on how to obtain the prerequisites and making both available to the general public.
* **NLnet** thinks that this project falls within its mission and the mission of the _NGI0 Entrust_ fund, and wants to facilitate such a contribution.

agree to the following:

* **Marcus Rohrmoser** has written the proposal "_#Seppo!_" which is attached to this document as Annexe I. Annexe I forms an integral part of this Memorandum of Understanding. If and where statements in this annexe or other annexes are in contradiction with one or more statements in the main memorandum text, the statement or statements in the main memorandum text will prevail.
* **Marcus Rohrmoser** is voluntarily undertaking the project, and is solely responsible for all aspects of the project including planning and coordination as well as involving contributors and partners - as long as such happens in line with the terms and spirit of this MoU, and with their explict and voluntarily consent to join this Memorandum of Understanding with all its stated obligations and provisions, and to act in good faith.
* The source code and technical designs of the Project (as well as any documentation and supporting materials officially produced within the project) are to be made openly available to the general public under a suitable free/libre/open source software or hardware license. Eligible licenses include the licenses recognised by OSI and FSF.
* As the signatory of this MoU, **Marcus Rohrmoser** shall act as official point of contact in the context of this project; it is the responsibility of **Marcus Rohrmoser** to notify **NLnet** in case of any changes or issues.
* **NLnet** commits to make a reservation for the amount of **50000** EUR to **Marcus Rohrmoser** in order to support the "_#Seppo!_" project. The reservation is bound to the proposal as contained within _Annexe I_ of this MoU.
* Should the project fail to complete the goals described in Annexe I, partially or in full, there is no other consequence than the termination of this MoU.
* **Marcus Rohrmoser** commits to keep the user and developer community up to date with progress made within the project at least every two months (more often is never a bad thing) and will maintain a public status page for the project to keep the wider internet community informed. As a courtesy, **Marcus Rohrmoser** may send non-public updates about the status of the project to **NLnet**, but there is no obligation whatsoever to do so - **NLnet** is not operationally involved with the project; its only interest is the public benefit that is the result of the project succeeding in reaching its goals. **Marcus Rohrmoser** commits to help NLnet to clear any uncertainties about the overall effort and project achievements should the need for that emerge (e.g. in the context of an official audit).
* The validity of the Memorandum of Understanding is one calendar year (twelve months) from the date of signing. If the project is not finished at this point of expiry, and the work is still relevant, it may be prolonged based on mutual agreement between **NLnet** and **Marcus Rohrmoser**. Any _amendment_ to the MoU only changes the validity period if it explictly establishes a new time frame, otherwise the original validity period stays intact.
* Donations may be claimed up to the reserved amount within a maximum of six weeks after the end of the validity of the (amended) Memorandum of Understanding. Donations will be final when the specified milestones or previously agreed partial deliveries have been verified to have been completed. Payments will subsequently be made by wire transfer into a bank account designated by **Marcus Rohrmoser**. Payment requests submitted at a later date are not guaranteed to be paid.
* **NLnet** and **Marcus Rohrmoser** may issue one or more individual or joint public statements announcing the project and the financial support from NLnet and the _NGI0 Entrust_ fund. **Marcus Rohrmoser** is also encouraged to visibly and vocally acknowledge this contribution and the contribution of EC to the _NGI0 Entrust_ fund (NGI Zero Entrust was established with financial support from the European Commission's _Next Generation Internet_ programme, under the aegis of DG Communications Networks, Content and Technology.) where possible — e.g. through the public world wide web, promotional materials, in presentations, in the credits section of software and in source code.
* The involvement with any particular person(s) or organisations will be on the understanding that these coordinate their activities in agreement with **Marcus Rohrmoser**, in the spirit of cooperation, and in an effort to achieve the results of the "_#Seppo!_" project. **Marcus Rohrmoser** appreciates the support from the experts and organisations involved with _NGI0 Entrust_ to ensure that the results of _#Seppo!_ will be of the widest possible benefit to all.
* This Memorandum of Understanding cannot be seen as any kind of employment agreement or business contract. **NLnet** nor any of the organisations involved with _NGI0 Entrust_ receive any goods or services as a result of this MoU. Any payments are to be made as charitable donations to **Marcus Rohrmoser** in the light of a voluntary contribution to the public benefit such as defined within the statutory mission of **NLnet foundation**. **Marcus Rohrmoser** is responsible for paying any and all taxes or other fees with regard to this grant, should there be any, and to inform any relevant authorities within their country of these donations should this be legally required.

On behalf of NLnet: **Bob Goudriaan**

Amsterdam - December 1, 2022

**Marcus Rohrmoser**

\<redacted\> - December 1, 2022

# Annexe I: Project plan _#Seppo!_

_"Posting and liking self reliantly and still have a life."_ #Seppo! empowers you to publish short texts and images to the internet as easily as using a SaaS but retain full agency and responsibility.

What you publish is solely subject to public law. No 3rd parties hold a stake, nobody else imposes any rules on you. This is because you publish on your own property. Which is possible because housekeeping is no more than the known follow/unfollow/block/unblock content moderation of your own single account. You do that by yourself. There are no scripting engines or databases, no technical updates required. You can focus solely on the message to deliver. You build an online presence on your own digital property, robust for decades if you decide so. #Seppo! is built on mature web standards, a european technology stack, inspectable plain-text storage, is security aware and decentralised. It is made for but not limited to off-the-shelf static webspace as offered by numerous vendors all over the EU. #Seppo! targets individuals and small organisations joining the #Fediverse with max. 10k followers, optionally cross-posting to the closed platforms.

#Seppo! is free software. The resulting code of each development task will be published on the project website. After every milestone there will be a full end-user documentation, and a test instance for demonstration and federation testing.

## 1. New instance via commandline interface (CLI)

Start a new instance on empty webspace by calling a commandline generator on the server. Verify the server settings, create and populate the required technical files to comply with the ActivityPub and Webfinger standards for a valid, externally visible @actor@example.com without posts or followers for now.

### Milestone(s)

* Create the seppo commandline program

### Amount

€ 2400

## 2. New post via CLI

Post new text message via the CLI on the server. Ensure posted messages are visible and makes it to the ‘following’ instances and test with existing, real world instances of:

* Mastodon
* Pleroma
* Peertube
* Pixelfed
* GnuSocial
* Friendica
* Lemmy
* Mobilizon if available
* Hubzilla if available
* Bonfire if available

### Milestone(s)

* Update the seppo commandline program to post messages

### Amount

€ 3400

## 3. New instance via web interface

Start a new instance on empty webspace by copying the seppo.cgi binary there and visit https://example.com/subdir/seppo.cgi. Verify the server settings, create and populate the required technical files to comply with the ActivityPub and Webfinger standards for a valid, externally visible @actor@example.com without posts or followers for now.

### Milestone(s)

* Create the seppo.cgi program 

### Amount

€ 2400

## 4. New post via web interface

Post a new text message via the web interface. Ensure the message is visible and makes it to the instances mentioned in
task _2\. New post via CLI._

### Milestone(s)

* Update seppo.cgi program to post messages

### Amount

€ 3880

## 5. Security audit & improvements

With mentoring by Radically Open Security with focus on threat model.

### Milestone(s)

* Integrate the findings

### Amount

€ 2400

## 6. ActivityPub Activities Like/Unlike, Reply, Announce

Support the following ActivityPub Activities: - Like/Unlike - Reply - Announce

### Milestone(s)

* Implement the activities on the web interface
* Test against the instances mentioned in
task _2\. New post via CLI._

### Amount

€ 3840

## 7. ActivityPub Activities (Un)Follow, Block

Implement the following ActivityPub Activities:

* Follow/Unfollow
* Block

### Milestone(s)

* Implement the activities on the web interface
* Test against the instances mentioned in
task _2\. New post via CLI._

### Amount

€ 5760

## 8. Housekeeping via web interface

Design & build housekeeping features.

### Milestone(s)

* password change
* password reset (may imply deletion of a file on the webspace)
* instance name, owner bio, account images
* timezone
* monitoring, server health
* usage, federation queue stats

### Amount

€ 5760

## 9. Accessibility audit

With mentoring by HAN University of Applied sciences and/or the Accessibility Foundation, with focus on everyday use as well as onboarding & housekeeping UX.

### Milestone(s)

* Integrate the findings

### Amount

€ 2880

## 10. Images

Implement features & tests for posting images.

### Milestone(s)

* Post single jpeg and png images, care about alt text, strip metadata, scale to reasonable dimensions
* Test against the instances mentioned in 
task _2\. New post via CLI._

### Amount

€ 3840

## 11. Improve UX

### Milestone(s)

* Improve UX based on user testing sessions

### Amount

€ 5760

## 12. Improve reading UX via web interface

Fine tune the reading UX in terms of e.g. various screen form factors, input devices, lighting conditions and color schemes, threaded or chronological presentation, read/unread, daily / most recent, stats (accounts and tags with post counts), lists etc.

### Milestone(s)

* Make and UX test functional prototypes

### Amount

€ 4800

## 13. Documentation and presentation

### Milestone(s)

* Publish online documentation on Seppo for users and developers. Write at least three comprehensive blog posts documenting (progress of) the project. (€ 1440)
* Update documentation, write at least three additional blog posts, and present the project at a related event. (€ 1440)

### Amount

€ 2880
