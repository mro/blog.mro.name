---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2016-03-22T07:42:09+00:00"
tags:
- CAcert
- Email
- S/MIME
- StartCom
- X.509
title: Email Sicherheit III – neue Zertifikate (und Keys)
type: post
url: /2016/03/email-sicherheit-iii-neue-zertifikate-und-keys/
---

inzwischen bin ich auf [StartCom<sup>®</sup>][1] umgesattelt – anfangs mit Schlüssel von
StartCom<sup>®</sup> (ja, ich weiß, macht man nicht), inzwischen mit eigenem Schlüssel. Und das geht
so:

<pre class="line-numbers"><code class="language-shell-session">$ cd ~/.ssh
$ keyname="foo@example.com"
$ year=2016

# https://www.startssl.com/Certificates/
$ openssl req -new -newkey rsa:4096 -nodes -keyout "${keyname}.key" -out "${keyname}.${year}.csr"

# funnel through https://startssl.com/Certificates/ApplyClientCert
# download & unpack ${keyname}.zip

# http://apple.stackexchange.com/a/9011
$ openssl pkcs12 -export -clcerts -inkey "${keyname}.key" -in "${keyname}.${year}.crt" -out "${keyname}.${year}.p12" -name "John Doe"

$ open "${keyname}.${year}.p12"
</code></pre>

 [1]: https://www.startcom.org
