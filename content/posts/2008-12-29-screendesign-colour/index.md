---
author: Marcus Rohrmoser
categories:
- en
- screendesign
date: "2008-12-29T17:29:00+00:00"
tags:
- Colour
- Gnome
- HCI
- Styleguide
title: 'Screendesign: Colour'
type: post
url: /2008/12/screendesign-colour/
yourls_shorturl:
- http://s.mro.name/b
---
[Arnheims general thoughts about colour][1] and a design education strongly influenced by grey
paperboard made me very careful when it comes to colour.

Especially when displayed on (uncalibrated) monitors or under varying light conditions, achieving a
proper colour impression is an art by itself.

The [Gnome HCI Styleguide on Colour][2] suggests using a palette of 32 (predefined) colours and not
to rely on them.

Ideal would be to make the whole colour scheme skinable via something like a simplified CSS.

 [1]: http://books.google.de/books?id=9RktoatXGQ0C&pg=PA330&source=gbs_toc_r&cad=0_0
 [2]: http://library.gnome.org/devel/hig-book/2.24/design-color.html.en