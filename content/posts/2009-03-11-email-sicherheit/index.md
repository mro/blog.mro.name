---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-03-11T11:58:16+00:00"
tags:
- BSI
- Email
- GnuPG
- S/MIME
- Sicherheit
- Thawte
- X.509
title: Email Sicherheit
type: post
url: /2009/03/email-sicherheit/
yourls_shorturl:
- http://s.mro.name/21
---
(Kurzfassung für [ADSler][1]: Das [Problem][2], die [Lösung][3])

Da die [großartige BSI Broschüre zum Thema][4] sehr umfassend angelegt ist, möchte ich die im &#8222;Hausgebrauch&#8220; praktisch nie beachtete Echtheit und Vertraulichkeit herausgreifen.

Vielen Email Schreibern ist nicht bewußt, daß eine Mail [eher einer Postkarte denn einem Brief][4] ähnelt: Sie kann auf dem Weg zum Empfänger an jeder Stelle komplett gelesen und sogar verändert werden. Außerdem ist der in der Mail angegebene Absender keineswegs zwangsweise der wirkliche Absender – bei Postbriefen kann übrigens auch alles mögliche als Absender draufstehen.

<!--more-->

Abhilfe ist kostenlos zwar seit Jahren mit Hilfe von [PGP / GnuPG][5] möglich, die Installation ist aber für durchschnittliche Email Schreiber offensichtlich zu abschreckend. Und leider geht das praktisch mit keinem Mail Programm ohne Installation eines Plugins.

Aber vor kurzem entdeckte ich, daß es eine kostenlose Möglichkeit gibt, die in fast allen Mail Programmen bereits eingebaut ist: [S/MIME][6] mit [X.509][7] Zertifikaten (wie beim Online-Banking).

Und wie geht's nun?

1. bei z.B. [Thawte][8] ein kostenloses Email Zertifikat für ein Jahr anfordern (5 Minuten Aufwand),
2. Zertifikat dem Mailprogramm verklickern,
3. fertig!

Für Apple Mail Benutzer gibt's eine komplette Anleitung als [5-Minuten Screencast][3] oder [Daumenkino][9].

Was bringt das nun?

Zunächst mal das gleiche wie den Hosenladen zuzumachen – also eigentlich nichts. Zusätzlich springt aber raus, daß

* ich zeige, daß ich meine Kommunikationspartner respektiere und ihnen vertrauliche Kommunikation ermögliche,
* verschickte (signierte) Mails fälschungssicher sind – nur was von mir signiert ist, kommt auch sicher von mir,
* verschlüsselte Mails von und zu anderen Leuten mit S/MIME Zertifikaten unterwegs (außer der Betreffszeile) niemand sonst mitlesen kann,
* ich [Medienkompetenz][10] zum Ausdruck bringe und kein [Internetausdrucker][11] bin.

Ich denke dafür lohnen sich 10 Minuten Aufwand pro Jahr, oder?

Nachtrag: Es gibt noch ein Schmankerl für [Thunderbird & S/MIME][12].

 [1]: http://de.wikipedia.org/wiki/Aufmerksamkeitsdefizit-/Hyperaktivit%C3%A4tsst%C3%B6rung
 [2]: https://www.bsi.bund.de/cln_156/ContentBSI/grundschutz/kataloge/g/g04/g04037.html
 [3]: http://vimeo.com/3215359
 [4]: https://www.bsi.bund.de/ContentBSI/Themen/Internet_Sicherheit/Uebersicht/sinetstd.html
 [5]: https://www.bsi.bund.de/cln_156/ContentBSI/grundschutz/kataloge/m/m05/m05063.html
 [6]: http://de.wikipedia.org/wiki/SMIME
 [7]: http://de.wikipedia.org/wiki/X.509
 [8]: http://www.thawte.com/secure-email/personal-email-certificates/index.html
 [9]: http://comodin.com/86.112.0.0.1.0.phtml
 [10]: http://de.wikipedia.org/wiki/Medienkompetenz
 [11]: http://internetausdrucker.de/
 [12]: http://www.mozillablog.de/2009/03/21/die-smime-fahigkeiten-von-thunderbird-aufbohren/