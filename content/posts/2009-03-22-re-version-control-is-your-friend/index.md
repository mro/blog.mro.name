---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-03-22T15:16:58+00:00"
tags:
- git
- subversion
title: 'Re: Version Control is Your Friend'
type: post
url: /2009/03/re-version-control-is-your-friend/
yourls_shorturl:
- http://s.mro.name/37
---
[Jeff Lamarche blogs &#8222;Version Control is Your Friend&#8220;][1]. I fully agree that version management is a phantastic pain killer – even when you're a one man show.

The important difference version management makes to me, is, that I can delete, clean up and refactor with ease and without the fear of ever loosing anything. And removing cruft from my projects helps to remain focused.

<!--more-->

Jeff features [subversion][2]. I think which tool you use doesn't matter too much – important is to do version management at all, from the beginning and in a fine grained manner (per project repo, commit often).

I started using svn with version 0.17 (Jan 2003) but moved to [git][3] (and [gitx][4]) last year for a couple of reasons:

* full offline history and operations (due to its distributed nature), so there's no moment in your development process when you _need_ network,
* extremely compact storage. It's not uncommon that a whole git working copy + repo is smaller than a mere svn working copy,
* cheap branching and merging,
* flexible hosting. If you want to publish a git repo you have a lot of really simple options, starting with ftp + http (static files on a webserver) for readonly repos or any webdav enabled webserver for r/w repos. No installation nor configuration of git components on the server. Most elegant is using ssh and [gitosis][5] if your hosting permits,
* doesn't rely on IDE integration as much as svn does, as git detects moves and copies by itself (sic!),
* lightning fast and capable of insanely huge amounts of data. [I once had a svn commit of one hour done by git in seconds][6]. Really.

 [1]: http://iphonedevelopment.blogspot.com/2009/03/version-control-is-your-friend.html
 [2]: http://en.wikipedia.org/wiki/Subversion_%28software%29
 [3]: http://de.wikipedia.org/wiki/Git
 [4]: http://gitx.frim.nl/seeit.html
 [5]: http://scie.nti.st/2007/11/14/hosting-git-repositories-the-easy-and-secure-way
 [6]: http://code.google.com/p/piccolo2d/wiki/WebSiteHowTo#Version_Management