---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-04-27T15:17:09+00:00"
tags:
- dot
- Doxygen
- Graphviz
- iPhone
- make
- OS X
title: 'Tooltip: doxygen & graphviz (OS X)'
type: post
url: /2010/04/tooltip-doxygen-graphviz-os-x/
yourls_shorturl:
- http://s.mro.name/2i
---
ever seen error messages like this when generating doxygen docs for an iPhone/Mac Objective C project:

</code></pre>
.../build/doxygen/html/a00136.png' produced by dot is not a valid PNG!
You should either select a different format (DOT_IMAGE_FORMAT in the config file) or install a more recent version of graphviz (1.7+)
</code></pre>

?

Happened to me this morning when running [doxygen][1] on another box. The [doxygen dmg][2] was installed in `/Applications`, I even added the [graphviz macport][3], so WTF.

It turned out, that

* the doxygen dmg brings everything it needs, including graphviz,
* _but_: the full path to the dot executable has to be set _inside_ the [doxygen config][4].

So, when using the dot from the doxygen dmg, it has to be set to

</code></pre>
/Applications/Doxygen.app/Contents/Resources
</code></pre>

I fixed [my generic Makefile][5] and make a good guess now while keeping things configurable per user as the Makefile and doxygen config are committed sources and should not contain user specific settings.

 [1]: http://www.doxygen.org
 [2]: http://www.stack.nl/~dimitri/doxygen/download.html#latestsrc
 [3]: http://graphviz.darwinports.com/
 [4]: http://www.stack.nl/~dimitri/doxygen/config.html#cfg_dot_path
 [5]: http://wiki.mro.name/orga/xcode_project_setup#makefile