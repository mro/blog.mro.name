---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-10-29T22:00:44+00:00"
tags:
- App Store
- Filmfest
- iPhone
- Projects
title: "'Filmfest' iPhone App v1.0 online! 📱"
type: post
url: /2009/10/filmfest-iphone-app-v1-0-online/
yourls_shorturl:
- http://s.mro.name/2q
---
After almost 6 weeks of eagerly waiting I found [Filmfest 1.0][1] shows in the [App Store][2]. Didn't get a confirmation / announcement mail but anyway, here it is:

{{< figure  src="/wp-content/uploads/2009/10/IMG_1119.PNG" caption="" width="320" height="480" >}}

 [1]: http://filmfestapp.com/
 [2]: http://filmfestapp.com/go/appstore
