---
title: Tempo 30 in Traunstein
url: /2019/07/tempo30/
draft: false
categories:
#- ':heart:'
#- 🇩🇪
- de
date: 2019-07-21 20:53:17+02:00
tags:
#- 🏥
#- 🏫
#- 🚗
- Tempo30
- Traunstein
- Verkehr
type: post
author: Marcus Rohrmoser
---

Wie angekündigt hier der Brief des Oberbürgermeisters der Großen Kreisstadt Traunstein:

----

##  Brief des Oberbürgermeisters der Großen Kreisstadt Traunstein vom 26. März 2019 ([pdf](2019-03-26-kegel.pdf))

Sehr geehrter Herr Rohrmoser,

zu Ihrem Schreiben vom 17.02.2019 nehmen wir wie folgt Stellung:

1 . Zur Rechtslage

a) Gem. [§ 45 Abs. 9, S. 1 und 2
StVO](https://www.gesetze-im-internet.de/stvo_2013/__45.html) sind
Verkehrszeichen und Verkehrseinrichtun&shy;gen nur dort anzuordnen, wo dies
aufgrund der besonderen Umstände zwingend erforderlich ist (Satz 1).
Insbesondere dürfen Beschränkungen und Verbote des flie&shy;ßenden Verkehrs nur
angeordnet werden, wenn aufgrund der besonderen örtlichen Verhältnisse eine
Gefahrenlage besteht, die das allgemeine Risiko einer Beeinträch&shy;tigung der
in den vorstehenden Absätzen genannten Rechtsgüter erheblich über&shy;steigt
(Satz 3). Die Voraussetzungen des Satzes 3 (besondere Gefahrenlagen und
besonderes Risiko) gelten gem. Satz 4 Ziffer 6 nicht für die Anordnung von
innerörtli&shy;chen streckenbezogenen Geschwindigkeitsbeschränkungen von 30
km/h (Zeichen 274) nach Abs. 1 Satz 1 auf Straßen des überörtlichen Verkehrs
(Bundes-, Landes- und Kreisstraßen) oder auf weiteren Vorfahrtsstraßen (Zeichen
306) im unmittelbaren Bereich von an diesen Straßen gelegenen Kindergärten,
Kindertagesstädten, all&shy;gemeinbildenden Schulen, Förderschulen, Alten- und
Pflegeheimen oder Kranken&shy;häusern.

b) Nach der [allgemeinen Verwaltungsvorschrift der Bundesregierung zur
Straßenver&shy;kehrsordnung (VwV-StVO) in der Fassung vom 22.05.2017, in Kraft
getreten am 30.05.2017 (Bundesanzeiger vom
29.05.2017)](http://www.verwaltungsvorschriften-im-internet.de/bsvwvbund_26012001_S3236420014.htm)
ist die Geschwindigkeit innerhalb ge&shy;schlossener Ortschaften im
unmittelbaren Bereich von an Straßen gelegenen Kin&shy;dergärten,
-tagesstätten, -krippen, -horten, allgemein bildenden Schulen,
Förder&shy;schulen für geistig oder körperlich behinderte Menschen, Alten- und
Pflegeheimen oder Krankenhäusern in der Regel auf Tempo 30 km/h zu beschränken,
soweit die Einrichtungen über einen direkten Zugang zur Straße verfügen oder im
Nahbereich der Einrichtungen starker Ziel- und Quellverkehr mit all seinen
kritischen Begleiter&shy;scheinen (z.B. Bring- und Abholverkehr mit vielfachem
Ein- und Aussteigen, erhöhter Parkraumsuchverkehr, häufige Fahrbahnquerungen
durch Fußgänger, Pulkbildung von Radfahrern und Fußgängern) vorhanden ist. Dies
gilt insbesondere auch auf klassifizierten Straßen (Bundes-, Landes- und
Kreisstraßen) sowie auf weiteren Vor&shy;fahrtsstraßen (Zeichen 306). Im
Ausnahmefall kann auf die Absenkung der Ge&shy;schwindigkeit verzichtet werden,
soweit etwaige negative Auswirkungen auf den ÖPNV (z. B. Taktfahrplan) oder
eine drohende Verkehrsverlagerung auf die Wohn&shy;nebenstraße zu befürchten
ist. In die Gesamtabwägung sind dann die Größe der Einrichtung und
Sicherheitsgewinne durch Sicherheitseinrichtungen und Querungs&shy;hilfen (z.
B. Fußgängerüberwege, Lichtzeichenanlagen, Sperrgitter) einzubeziehen.  Die
streckenbezogene Anordnung ist auf den unmittelbaren Bereich der Einrichtung
und insgesamt auf höchstens 300 m Länge zu begrenzen.  Die beiden
Fahrteinrich&shy;tungen müssen dabei nicht gleich behandelt werden.  Die
Anordnungen sind, so&shy;weit Öffnungszeiten (einschließlich Nach- und
Nebennutzungen) festgelegt wurden, auf diese zu beschränken.

c) In einem Rundschreiben vom 02.08.2017 an die Regierungen, Landratsämter,
Kreisfreien Städte und Großen Kreisstädte in Bayern gibt das Bayerische
Staatsminis&shy;terium des Innern, für Bau und Verkehr Vollzugshinweise. U. a.
wird dort in Unterab&shy;schnitt 1 „Regelungsinhalt und Grundsatz der
Einzelfallentscheidung“ folgendes ausgeführt:

„Die Möglichkeit der erleichterten Anordnung für Beschränkungen des fließenden
Verkehrs vor den genannten Einrichtungen stellt eine zusätzliche und wichtige
neue Möglichkeit dar, unter Verkehrssicherheitsaspekten besonders
schützenswerte Be&shy;reiche im Einzelfall sicherer zu machen.

Ein Automatismus, dass vor den genannten Einrichtungen fortan stets
Geschwindig&shy;keitsbeschränkungen auf 30 km/h anzuordnen sind, ist mit der
Änderung der Vor&shy;schrift nicht verbunden (vgl. [Bundesratsdrucksache 332 /
16 vom 15.06.2016, Seite 14
oben](https://www.bundesrat.de/SharedDocs/drucksachen/2016/0301-0400/332-16.pdf?__blob=publicationFile&v=5#page=20)).
Die Regelung setzt eine ergebnisoffene Einzelfallprüfung anhand der
konkre&shy;ten örtlichen Verhältnisse voraus.

Der Nachweis besonderer örtlicher Verhältnisse, die eine Gefahrenlage bedingen,
die das im Straßenverkehr allgemein anzutreffende Risiko einer Beeinträchtigung
der Verkehrssicherheit überheblich übersteigt (§ 45 Abs. 9 Satz 3 StVO). muss
für die An&shy;ordnung einer Geschwindigkeitsbeschränkung nicht mehr geführt
werden. Die ail&shy;gemeine Hürde des § 45 Abs. 9 Satz 1 StVO, wonach
Verkehrszeichen und Ver&shy;kehrseinrichtungen nur dort anzuordnen sind, wo
dies aufgrund der besonderen Umstände erforderlich ist, bleibt von der
Neuregelung jedoch unberührt.

Damit ist von den Stroßenverkehrsbehörden weiterhin im Einzelfall zunächst zu
prü&shy;fen, ob die sachlichen Voraussetzungen für eine Anordnung erfüllt sind.
Der Grund&shy;satz der Verhältnismäßigkeit ist dabei zu beachten. Neben dem
Aspekt der Ver&shy;kehrssicherheit sind bei der Beurteilung durch die
Straßenverkehrsbehörde in Be&shy;nehmen mit der Straßenbaubehörde und der
Polizei auch alle weiteren relevanten Belange und Interessen, wie z. B. die
Leichtigkeit und Flüssigkeit des Verkehrs, die Funktion und Bedeutung der
betroffenen Straße oder zu erwartende Auswirkungen auf den ÖPNV zu
berücksichtigen. Zu prüfen ist auch, ob die erforderliche Steige&shy;rung der
Verkehrssicherheit auch mit den Verkehr weniger einschränkenden, z. B.
baulichen Maßnahmen ebenso erreicht werden kann.“

2 . Auf dieser Grundlage haben wir uns mit den in Betracht kommenden
Einrichtun- gen im Stadtgebiet Traunstein im Benehmen mit der Polizei sehr
ernsthaft auseinan&shy;der gesetzt.

Vor folgenden Einrichtungen war bereits Tempo 30 angeordnet:

* Kindergarten Haslach, Gamskogelstraße
* Kindergarten HI. Kreuz, Schloßstraße
* Kindergarten St. Oswald, Kellerweg
* Kinderhort „Die Murmel", Haslacher Straße
* Kinderkrippe „Kleine Bären“, Bachmayerstraße
* Waldorfkindergarten Chiemgau e. V., Traunerstraße
* Kindergarten Kammer, Hopfengartenweg
* Grundschule Kammer, Hopfengartenweg
* Franz-von-Kohlbrenner-Mittelschule, Haslacher Straße
* Montessori-Schule Traunstein
* Wirtschaftsschule Dr. Kalscheuer, Weckerlestraße
* Seniorenzentrum Wartberghöhe, Haslacher Straße
* ASB Seniorenzentrum, Haslacher Straße
* Seniorenresidenz Kirschgarten, Karl-Merkenschlager-Straße

Bei folgenden Einrichtungen wurde Tempo 30 noch angeordnet:

* Kindergarten an der Kindergartenstraße
* Waldkindergarten Mayerhofen
* Ludwig-Thoma-Grundschule, Ludwigstraße
* Anette-Kolb-Gymnasium, Güterhallenstraße
* Sonderpädagogisches Förderzentrum und Chiemgau-Gymnasium (Zone)

Beim Schulzentrum an der Wasserburger Straße wurde von Tempo 30 abgesehen, weil

* die berufsbildenden Schulen (hier: Berufsschule I) begrifflich nicht zu den
  „be&shy;günstigten“ Schulen zählen,
* die anderen Schulen mit Ein-/Ausgang nicht unmittelbar an die Wasserburger
  Straße angrenzen,
* bei den betroffenen Schülern von einer gewissen „Reife“ - auch im
  Straßen&shy;verkehr - ausgegangen werden kann,
* Querungshilfen, Bürgersteige und Bushaltestellen vorhanden sind,
* der „Schulweg“ entlang der Wasserburger Straße sich weit Über dem max.
  zu&shy;lässigen Maß von 300 m bewegt.

Ihren Vorhalt, dass rechtswidrig von der Anordnung von Tempo-30-Zonen
abgese&shy;hen wurde, weisen wir daher zurück. Auch nach der gesetzlichen
Neuregelung be&shy;steht kein Automatismus. Vielmehr ist weiterhin eine Prüfung
des Einzelfalles im Hin&shy;blick auf die Notwendigkeit einer
Geschwindigkeitsbeschränkung durchzuführen.

Eine Flut von Beschränkungen führt nicht unbedingt zu mehr Disziplin bei den
Kraft&shy;fahrern. Eher ist das Gegenteil zu befürchten, vor allem, wenn der
Sinn einer Beschränkung nicht offensichtlich ist. Das gilt speziell für
Hauptverkehrsstraßen bzw.  Ortsdurchfahrten. Die Stadt hat sich in der
Wasserburger Straße letztlich für die Auf&shy;stellung der Gefahrenzeichen mit
ZZ „Schule“ entschieden.

3 . Für die Salinenstraße wurde im Hinblick auf die Einrichtung „St. Josef“
ebenfalls eine Einzelprüfung durchgeführt. Nach dem die Einrichtung gerade aus
Gründen der Verkehrssicherheit vom Triftweg aus bedient wird, ist eine
diesbezügliche Ge&shy;schwindigkeitsbeschränkung nicht erforderlich.

4 . Bei den einzelnen Entscheidungen über die Anordnung von Tempo-30-Zonen
handelt es sich grundsätzlich um laufende Angelegenheiten im Sinne [Art. 37
Abs.  1 Satz 1 Ziffer 1 Bayerische
Gemeindeordnung](https://www.gesetze-bayern.de/Content/Document/BayGO-37).
Beschlussfassungen durch die städti&shy;schen Gremien sind deshalb im
Einzelfall nicht erforderlich.  Ungeachtet dessen hat aber die Verwaltung dem
Planungsausschuss des Stadtrates in der Sitzung vom 17.07.2017 alle Maßnahmen
umfassend vorgestellt und über den aktuellen Sach&shy;stand berichtet. Der
Ausschuss hat davon zustimmend Kenntnis genommen.

Die Stadt hat sich sehr intensiv mit allen Belangen befasst und im Rahmen der
Ab&shy;wägung die aus unserer Sicht notwendigen Maßnahmen getroffen.

Mit herzlichen Grüßen

Christian Kegel

