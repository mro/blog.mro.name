---
title: 'Lightning-Talk: Dissolving GAFAM, a bit at a time.'
url: /2018/12/dissolving-gafam/
draft: false
categories:
#- 🇩🇪
- en
date: 2018-12-28 12:25:00+01:00
tags:
- GAFAM
- 35c3
- CCC
- Lightning-Talk
- Vortrag
type: post
author: Marcus Rohrmoser
---

“We need to re-decentralise the web” @TimBl, 2014  
“You are never too small to make a difference” Greta Thunberg, 2018, Cattowice

[📺 Video](https://txt.mro.name/35c3/35c3-dissolving-gafam_--_480p.mp4)  
[📄 Slides](https://txt.mro.name/35c3/35c3-dissolving-gafam.pdf)

http://mro.name/35c3

