---
title: 'Lightning-Talk: Jailbreaking Governmental Data: PDF becomes RDF'
url: /2016/12/pdf-to-rdf/
draft: false
categories:
#- 🇩🇪
- en
date: 2016-12-29 11:05:00+01:00
tags:
- 33c3
- CCC
- Lightning-Talk
- Vortrag
- OpenData
- PDF
- RDF
type: post
author: Marcus Rohrmoser
---


[📺 Video](https://txt.mro.name/33c3/33c3-jailbreaking-governmental-data-pdf-rdf_--_480p.mp4)  
[📄 Slides](https://txt.mro.name/33c3/33c3-jailbreaking-governmental-data-pdf-rdf.pdf)

http://mro.name/33c3

