---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2009-01-08T06:04:52+00:00"
tags:
- Email
- Faces
- iPhoto
- PhotoSynth
title: Privatsphäre 2.0 – was wird das wohl sein?
type: post
url: /2009/01/privatsphare-20-was-wird-das-wohl-sein/
yourls_shorturl:
- http://s.mro.name/1y
---
Vielleicht sind die staatliche Überwachung (z.B. Maut-Überwachung, Vorratsdatenspeicherung, BKA Gesetz) und die gewerbliche Datensammelei (incl. Pannen) völliger Kinderkram – im Vergleich zu den öffentlichen, privaten Datensammlungen.

<!--more-->Nehmen wir mal Photos – von jedem schwirren Unmengen an Photos durch's Netz. Allein Party- und Urlaubsbilder&nbsp; mit einem selbst im Hintergrund.

Und jetzt kommt z.B. [iPhoto mit Faces][1] (Facebook, Picasa und [OpenCVLibrary][2] können's schon länger) und schon kann ich jemanden, den ich (oder evtl. jemand anders) 1x identifiziert habe in einer Menge an Photos automatisch finden.

Mit Geo-Tagging lassen sich so z.B. Bewegungsprofile erstellen. Ungetaggte Bilder lassen sich per [Photosynth][3] korrelieren.

Oder Netze – wer kennt wen. Aber das ist ja über die Sozialen Netzwerke eh schon öffentlich.

Naja – vielleicht war Privatsphäre ja nur eine verübergehende Erscheinung. Früher im Dorf (oder im Stamm, in der Sippe, in der Höhle) hat auch jeder alles von jedem gewußt (und manchmal auch noch etwas mehr). Erst in der (post?-)industriellen urbanen Gesellschaft gab's sowas wie Anonymität oder vertrauliche Privatsphäre. Und jetzt ist's halt wieder vorbei – bald kann jeder mit Computer über jeden recht viel nachforschen.

Also?

* Pseudonym &#8222;Peter Müller&#8220; zulegen,
* Gesichts-OP und Standardfresse machen lassen,
* Photomontagen auf Flickr hochladen (viele!),
* alle Welt bei Xing & Co einladen,
* [Freifunk][4] und Peer-to-Peer (z.B. VoIP via Jabber) Dienste benutzen
* ab und zu mal heiraten oder auf andere Weise den Namen ändern,
* vorzugsweise fremde Computer und Telephone benutzen,
* zur Untermiete wohnen oder Couchsurfing.

Nachtrag: Das mit der [Email Sicherheit geht einfacher als gedacht][5].

 [1]: http://www.tuaw.com/2009/01/06/apple-introduces-ilife-09-at-macworld-expo/
 [2]: http://sourceforge.net/projects/opencvlibrary/
 [3]: http://blog.mro.name/2008/12/ted-blaise-aguera-y-arcas-demos-photosynth/
 [4]: http://freifunk.net/
 [5]: http://blog.mro.name/2009/03/email-sicherheit/