---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-03-25T11:11:21+00:00"
tags:
- Coverage
- CoverStory
- gcov
- iPhone
- Objective C
- SenTestingKit
- Unit Test
title: iPhone Unit Test Coverage
type: post
url: /2010/03/iphone-unit-test-coverage/
yourls_shorturl:
- http://s.mro.name/h
---
1. have a look at the [CoverStory Howto][1],
2. download CoverStory,
3. open (with XCode) the XCode Project you want to measure,
4. run the script EnableGCov.scpt linked to from the howto,
5. run your tests and see a [linker error][2], – dead end for the time being. <http://groups.google.com/group/coverstory-discuss/browse_thread/thread/fbcbf5ed61d8d02b#>

 [1]: http://code.google.com/p/coverstory/wiki/UsingCoverstory
 [2]: http://lists.apple.com/archives/xcode-users/2009/Aug/msg00436.html