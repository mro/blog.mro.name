---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2008-10-07T18:13:55+00:00"
tags:
- EclipseME
- J2ME
- WTK
title: WTK post install / filesystem permissions
type: post
url: /2008/10/wtk-post-install-filesystem-permissions/
yourls_shorturl:
- http://s.mro.name/z
---
If running your app from the [eclipseme][1] emulator shows the following console output

<pre class="line-numbers"><code class="language-shell-session">Running with storage root DefaultColorPhone
Warning: Failed to initialize WMA message routing support
Error: Can't remove a lock file: /opt/sun-j2me-bin-2.2/wtklib/comcon-server.port.lock
Warning: Failed to initialize Bluetooth (JSR 82) support
...
Warning: The attempt to finalize unexisting client.
java.lang.NullPointerException
at com.sun.kvem.midp.MIDP.run(Unknown Source)
at com.sun.kvem.environment.EmulatorInvoker.runEmulatorImpl(Unknown Source)
at com.sun.kvem.environment.EmulatorInvoker.runEmulatorSameVM(Unknown Source)
at com.sun.kvem.environment.EmulatorInvoker.runEmulator(Unknown Source)
at com.sun.kvem.environment.ProfileEnvironment$KVMThread.runEmulator(Unknown Source)
at com.sun.kvem.environment.ProfileEnvironment$KVMThread.run(Unknown Source)
</code></pre>

give write permission to the `wtklib` folder inside the WTK:

<pre class="line-numbers"><code class="language-shell-session">$ sudo chmod o+w /opt/sun-j2me-bin-2.2/wtklib
</code></pre>

The following disease

<pre class="line-numbers"><code class="language-shell-session">...
javax.microedition.rms.RecordStoreException: error opening record store file
at javax.microedition.rms.RecordStore.<init>(+428)
at javax.microedition.rms.RecordStore.openRecordStore(+109)
at name.mro.billard.Stats.findPlayers(+6)
at name.mro.billard.Stats.startApp(+4)
at javax.microedition.midlet.MIDletProxy.startApp(+7)
at com.sun.midp.midlet.Scheduler.schedule(+270)
at com.sun.midp.main.Main.runLocalClass(+28)
at com.sun.midp.main.Main.main(+116)
startApp threw an Exception
...
</code></pre>

is cured by

<pre class="line-numbers"><code class="language-shell-session">$ sudo chmod o+w -R /opt/sun-j2me-bin-2.2/appdb
</code></pre>

A [Sun Forum Post][2] provided the correct hint.

 [1]: http://www.eclipseme.org
 [2]: http://forums.sun.com/thread.jspa?messageID=1553333
