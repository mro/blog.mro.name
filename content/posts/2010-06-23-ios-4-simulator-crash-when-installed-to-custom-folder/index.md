---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-06-23T10:42:11+00:00"
tags:
- crash
- iOS 4
- iPhone
- iPhone Simulator
- Softlink
- XCode
title: iOS 4 Simulator crash (when installed to custom folder)
type: post
url: /2010/06/ios-4-simulator-crash-when-installed-to-custom-folder/
yourls_shorturl:
- http://s.mro.name/19
---
just downloaded and installed the iOS 4 SDK and as my root OS X partition is rather (too) small, I put it into a custom location /Users/Developer.Snowleopard/.

This causes the iPhone Simulator to crash and compiling gives an error like:

<pre class="line-numbers"><code class="language-log">
ibtool failed with exception: Interface Builder encountered an error communicating with the iPhone Simulator. If you choose to file a crash report or radar for this issue, please check Console.app for crash reports for "Interface Builder Cocoa Touch Tool" and include their content in your crash report.
...
dyld: Library not loaded: /Developer/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator4.0.sdk/System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libBLAS.dylib
  Referenced from: /Users/Developer.SnowLeopard/Platforms/iPhoneSimulator.platform/Developer/SDKs/iPhoneSimulator4.0.sdk/System/Library/Frameworks/Accelerate.framework/Versions/A/Frameworks/vecLib.framework/Versions/A/libLAPACK.dylib
  Reason: image not found
</code></pre>

The cure – a softlink:

<pre class="line-numbers"><code class="language-shell-session">
$ ln -s /Users/Developer.SnowLeopard/Platforms /Developer/Platforms
</code></pre>
