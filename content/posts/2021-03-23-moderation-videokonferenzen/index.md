---
title: 'Online-Keynote: Moderation von Videokonferenzen ohne Registrierung und mit unerfahrenen Teilnehmern'
url: /2021/04/moderation-videokonferenzen/
draft: false
categories:
#- 🇩🇪
- de
date: 2021-03-23 15:00:00+01:00
tags:
- Moderation
- Videokonferenz
- Vortrag
- Traunstein
- CMW
- Jitsi
type: post
author: Marcus Rohrmoser
---

# Moderation von Videokonferenzen ohne Registrierung und mit unerfahrenen Teilnehmern

Aktuell fallen viele Veranstaltungen aus und niemand kann voraussagen, wann sich
das wieder ändert. Um trotzdem Kontakt zu halten – z.B. bei
Mitgliederversammlungen, Themen-Stammtischen, Elternabenden, mit Familie und
Freunden – können Videokonferenzen helfen. Am Beispiel des Videokonferenzsystems
‚Jitsi Meet‘ wird die Vorbereitung und Einladung zu einer Sitzung geübt und
gezeigt, wie man die Kontrolle selbst bei turbulenten Verläufen behält. Es wird
besprochen, welche unliebsamen Überraschungen auftreten können, wie man darauf
reagiert oder diese von vornherein vermeidet. Außerdem wird auf die Möglichkeiten
und Grenzen der entsprechenden Mobil-Apps eingegangen.

[Programm Chiemgauer Medienwochen, Di, 23. März
2021](https://web.archive.org/web/20210514230908/https://chiemgauer-medienwochen.de/programm/#fancy-title-132)

[Votragswebsite und Handout](http://marcus.rohrmoser.name/cmw21)

