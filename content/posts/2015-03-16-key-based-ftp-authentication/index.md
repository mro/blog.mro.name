---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2015-03-16T07:42:47+00:00"
tags:
- authentication
- curl
- FTP
- Keys
- lftp
- security
- SSH
title: Key-based FTP authentication
type: post
url: /2015/03/key-based-ftp-authentication/
yourls_fetching:
- 1
---
1. make a strong ssh key `$ [ssh-keygen](https://en.wikipedia.org/wiki/Ssh-keygen) -t rsa -b 4096 -f ~/.ssh/id_rsa`
2. turn to [RFC 4716][1] `$ ssh-keygen -e -f ~/.ssh/id_rsa.pub`
3. add to `~/.ssh/authorized_sftpkeys` on destination host
4. try out:
  1. `$ [curl](http://curl.haxx.se/) -u "<username>:" --key ~/.ssh/id_rsa --pubkey ~/.ssh/id_rsa.pub -T <file to upload> sftp://<target host>/<target path>/`
  2. `$ [lftp](http://lftp.yar.ru/) -u <username>,xx ... sftp://<target host>`

P.S.: [Hetzner FAQ zum Thema SFTP + Keys][2].

 [1]: https://tools.ietf.org/html/rfc4716
 [2]: http://wiki.hetzner.de/index.php/KonsoleH:Zugriff_per_FTP#SFTP_mit_Key-Authentifizierung