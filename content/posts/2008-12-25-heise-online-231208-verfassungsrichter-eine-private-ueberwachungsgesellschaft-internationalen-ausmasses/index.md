---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2008-12-25T09:05:33+00:00"
tags:
- BKA
- BSI
- Datenschutz
- GnuPG
- Sicherheit
title: „private Überwachungsgesellschaft internationalen Ausmaßes“
type: post
url: /2008/12/heise-online-231208-verfassungsrichter-eine-private-ueberwachungsgesellschaft-internationalen-ausmasses/
yourls_shorturl:
- http://s.mro.name/1p
---
[heise online – 23.12.08 – Verfassungsrichter: eine &#8222;private Überwachungsgesellschaft internationalen Ausmaßes&#8220;][1] – so ganz Unrecht hat Papier da nicht.

Solange es der gemeine EMail Schreiber nicht mal schafft den Briefumschlag zuzukleben (zu kompliziert), ist's mit dem Briefgeheimnis natürlich nicht weit her. Mich übrigens inklusive &#8212; da praktisch keiner meiner EMail Kontakte den Brieföffner bedienen kann.

Aber Rückzug ist auch keine Lösung. Und die staatlichen Signale (außer aus Karlsruhe) sind eine echte Katastrophe, es geht munter hin und her:

* Pro: [Datenschützer: Auch und gerade der Staat gefährdet Kontodaten der Bürger][2]
* Conta: [BKA-Chef: „Terroristen dürfen sich nicht durch Verschlüsselung dem Rechtsstaat entziehen“][3]
* Pro: [Keylogger stehlen in großem Maßstab Zugangsdaten][4]
* Contra: das jüngste [BKA Gesetz][5]
* Pro: [BSI fördert GnuPG][6]

Nachtrag: Das mit der [Email Sicherheit geht einfacher als gedacht][7].

 [1]: http://www.heise.de/newsticker/Verfassungsrichter-eine-private-Ueberwachungsgesellschaft-internationalen-Ausmasses--/meldung/120882
 [2]: http://www.computerzeitung.de/articles/datenschuetzer_auch_und_gerade_der_staat_gefaehrdet_kontodaten_der_buerger:/2008052/31766186_ha_CZ.html
 [3]: http://www.computerzeitung.de/articles/bka-chef_terroristen_duerfen_sich_nicht_durch_verschluesselung_dem_rechtsstaat_entziehen:/2008052/31779563_ha_CZ.html
 [4]: http://www.computerzeitung.de/articles/keylogger_stehlen_in_grossem_massstab_zugangsdaten:/2008052/31779555_ha_CZ.html
 [5]: http://www.sueddeutsche.de/politik/252/451960/text/
 [6]: http://www.bsi.de/gshb/deutsch/m/m05063.htm
 [7]: http://blog.mro.name/2009/03/email-sicherheit/