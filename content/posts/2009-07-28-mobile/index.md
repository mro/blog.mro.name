---
author: Marcus Rohrmoser
categories:
- en
- offtopic
date: "2009-07-28T00:20:52+00:00"
tags:
- mobile
- yet to come
title: Mobile
type: post
url: /2009/07/mobile/
yourls_shorturl:
- http://s.mro.name/3v
---
Two years from now:

* it's not mobile, it won't be used. 
* displays are furniture, so are keyboards. Nobody would carry his office chair along. 
* touch, spacial gestures, voice and handwriting will be the input devices. And a bit of keyboard. 
* it's not a *NIX OS, it won't be there. 
* your computer is in your pocket and syncs with your home/office server/accesspoint/vcr plus cloud services. 

Written on an iphone.