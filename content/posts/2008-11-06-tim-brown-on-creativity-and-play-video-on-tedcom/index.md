---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2008-11-06T17:15:48+00:00"
tags:
- inspire
- play
- TED
title: Tim Brown on creativity and play | Video on TED.com
type: post
url: /2008/11/tim-brown-on-creativity-and-play-video-on-tedcom/
yourls_shorturl:
- http://s.mro.name/1a
---
Share your wild ideas!

[The Video on TED.com][1]

{{<ted tim_brown_on_creativity_and_play >}}

* Exploration – go for quantity
* Building – think with your hands
* Role play – act it out; have more empathy for the situation we design for and create experiences that are seamless and authentic

I wonder how this raw prototyping – getting an idea into the real world within minutes and play with it – can be taken to software creation. Turning a software system into role play? Sketching on paper and shuffling this sketches?

 [1]: http://www.ted.com/index.php/talks/tim_brown_on_creativity_and_play.html