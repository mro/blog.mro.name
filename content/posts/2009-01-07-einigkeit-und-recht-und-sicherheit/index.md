---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2009-01-07T20:14:40+00:00"
tags:
- CCC
- Rhetorik
- Sicherheit
title: Einigkeit und Recht und Sicherheit
type: post
url: /2009/01/einigkeit-und-recht-und-sicherheit/
yourls_shorturl:
- http://s.mro.name/2c
---
Der Podcast [Neusprech im Schnüffelstaat (Chaosradio Express)][1] zieht sich anfangs ein wenig, aber
nach einer halben Stunde hat er richtig Fahrt aufgenommen und wird bis zum Schluß immer besser.

<!--more-->Zuletzt schwirrt mir schon der Kopf, da anscheinend Nicht-Kompetenz im Fach eine
wünschenswerte, ja sogar notwendige Eigenschaft oder Qualifikation für Führungskräfte (in der
Politik) zu sein scheint. Die von Fachkenntnis ungetrübte Entscheidung sozusagen.

Wer bestimmt da eigentlich über die Rahmenbedingungen unseres (meines) Lebens? Und mit welchem
Ernst?

Gruslig.

Was mich an dem ganzen Terrorhokuspokus extrem stört, ist daß der Mensch entweder als rein passiver
Konsument von Sicherheit oder eben als Terrorist auftaucht. Zivilcourage? Mündiger Bürger? Ist das
out? Was hilft dem [Opa in der U-Bahn die Videoüberwachung, wenn die Umstehenden nur glotzen und
keiner hilft][2]?

Abgesehen davon waren die Bilder damals gut für die Tagesschau und Bild – die Fahndung kam durch die
Anrufe über ein gestohlenes Mobiltelephon so schnell zu den Nachwuchs-Bushidos.

Und wenn mir mein Handy geklaut wird gebe ich der Polizei die Erlaubnis mit mir zusammen die letzten
Nummern unter die Lupe zu nehmen. Wenn der Dieb quasi nebenbei noch einen Mordversuch begangen hat
helfe ich der Polizei gerne mit meinem Einzelverbindungsnachweis.

Da braucht's keine Vorratsdatenspeicherung, keine Online-Durchsuchung, keine Bundeswehr im Inneren,
keine Fingerabdrücke in Paß oder Ausweis und es muß niemand verwanzt werden. Die Rufnummern von der
Telephonrechnung reichen – dem Cop eine Vollmacht ausgestellt, Fax an den Provider, evtl. ein paar
Nervanrufe – fertig, oder?

Also – wozu soll das alles eigentlich gut sein?

Sind das Abwehrkämpfe der Oldschool Nomenklatura einer immer demokratischer werdenden Gesellschaft
gegenüber? Wäre es nicht stattdessen sinnvoll sich Gedanken über die Bedeutung von Vertraulichkeit
für das menschliche und geschäftliche Miteinander zu machen?

 [1]: http://chaosradio.ccc.de/cre081.html
 [2]: http://www.sueddeutsche.de/muenchen/639/428394/text/