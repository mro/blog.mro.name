---
author: Marcus Rohrmoser
categories:
- en
date: 2019-05-19 17:22:23+02:00
tags:
- Hugo
- WordPress
- Degrowth
- Atom
- XSLT
- ShaarliGo
title: WordPress => Hugo
type: post
url: /2019/05/wp-to-hugo-making-of/
---

Wordpress got me started without having to deal with html and css or php. Thank
you!

Later, when blogging intensity dropped to almost zero, the quarterly or so php
and mysql updates became my only activities for the blog. And the more popular
wordpress became, the less comfortable I felt with it.

Then, 5 years ago, I learned about the static site generators
[jekyll](https://jekyllrb.com/) and [nanoc](https://nanoc.ws/). But what about
comments? Having the old comments around was dear to me and handing them to a 3rd
party was not an option, nor was adding external dependencies. I thought about
[isso](https://posativ.org/isso/), but was reluctant to add another runtime
(python), now that I just would get rid of php.

Meanwhile, I built a microblogging engine,
[ShaarliGo](https://mro.name/ShaarliGo), that actually is a static site
generator as well and uses [atom](https://tools.ietf.org/html/rfc5005) feeds as
the sole publication format and leaves the template rendering to the browser.
No javascript. No php. No database. Nice.

Finally, I learned about [hugo](https://gohugo.io/), which also got me rid of
all runtime and gem dependencies. More and more, I got fascinated by minimal
solutions. ([A plea for lean software](https://cr.yp.to/bib/1995/wirth.pdf))

The wish to migrate to a no-bullshit static blog grew, until I finally accepted
to sacrifice the comments as well as the search --- just to get rid of php and
mysql.

But not before moving, I recognised that comments can remain the atom feeds
they already were and be rendered in the browser! Like ShaarliGo does all the
time. Just inside an iframe!

The actual move then was

1. export with
	 [SchumacherFM/wordpress-to-hugo-exporter](https://github.com/SchumacherFM/wordpress-to-hugo-exporter),
2. customise the wonderful hugo theme [xmin](https://xmin.yihui.name/) a bit,
3. brush through all posts for markdown conversion hickups and inline html,
4. dump all existing comment atom feeds (iterate all urls and `curl`),
5. gently clean them (`sed` and a bit of `perl`) and add a
	 [xslt](https://www.w3.org/TR/xslt-10/) processing instruction,
6. add duckduckgo site search. Scoped by redirect. What's good enough for
	 [w3c](https://w3.org), is surely good enough for me.

Commenting via email. New comments now require manually copy+paste into a
new atom feed entry.

Not perfect, but sounds feasible for the time being --- until the 100th comment
:-)

