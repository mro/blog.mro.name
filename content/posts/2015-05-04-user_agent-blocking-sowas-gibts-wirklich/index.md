---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2015-05-04T06:42:55+00:00"
tags:
- Blocking
- HTTP Header
- USER_AGENT
title: USER_AGENT Blocking – sowas gibt’s wirklich!
type: post
url: /2015/05/user_agent-blocking-sowas-gibts-wirklich/
yourls_fetching:
- 1
---
schon kindisch, oder?

<pre class="line-numbers"><code class="language-shell-session">$ curl --head http://www.wetteronline.de/wetter/traunstein
HTTP/1.1 403 Forbidden
Server: AkamaiGHost
Mime-Version: 1.0
Content-Type: text/html
Content-Length: 290
Expires: Mon, 27 Apr 2015 21:01:07 GMT
Date: Mon, 27 Apr 2015 21:01:07 GMT
Connection: keep-alive
Vary: User-Agent

$ curl -A Mozilla --head http://www.wetteronline.de/wetter/traunstein
HTTP/1.1 200 OK
Server: Apache
res-obj: www
Content-Type: text/html; charset=UTF-8
Date: Mon, 27 Apr 2015 21:01:14 GMT
Content-Length: 132153
Connection: keep-alive
Vary: User-Agent
</code></pre>
