---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-02-02T20:26:12+00:00"
tags:
- Binary
- exchange
- Huffman
- XML
title: 'Poignant: Xml meets Huffman'
type: post
url: /2010/02/poignant-xml-meets-huffman/
yourls_shorturl:
- http://s.mro.name/33
---
There's a [spec at the w3c about compressing (XML) named &#8222;Efficient XML Interchange&#8220; Format][1] taking into account the grammar and likelihood of atoms within the document. They indeed use something similar the the [Huffman Coding][2].

The [results][3] are quite impressive – nice charts!

 [1]: http://www.w3.org/TR/2009/CR-exi-20091208/
 [2]: http://en.wikipedia.org/wiki/Huffman_coding
 [3]: http://www.w3.org/TR/2009/WD-exi-evaluation-20090407/#results