---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2009-01-10T14:06:24+00:00"
tags:
- Privacy
- Social Networks
title: Social Networks vs. Privacy
type: post
url: /2009/01/social-networks-vs-privacy/
yourls_shorturl:
- http://s.mro.name/1g
---
[Slashdot | Google Researchers Warn of Automated Social Info Sharing][1].

No big surprise, is it?

See also the german post [Heise: Google-Forscher warnen vor Datenschutzgefahren sozialer Netzwerke][2].

 [1]: http://yro.slashdot.org/article.pl?sid=09/01/10/0028236&from=rss
 [2]: http://www.heise.de/newsticker/Google-Forscher-warnen-vor-Datenschutzgefahren-sozialer-Netzwerke--/meldung/121481