---
title: 'NLnet Grant Application for #Seppo! – Questions and Answers'
url: /2022/09/nlnet-grant-application/
# https://ar.al/2022/09/28/nlnet-grant-application-for-domain-first-update-questions-and-answers/
draft: false
categories:
#- 🇬🇧
- en
date: 2022-09-27 11:14:14+02:00
tags:
- ActivityPub
- CGI
- grant
- horizon2020
- NLnet
- OCaml
- Seppo
type: post
author: Marcus Rohrmoser
---

Dear &lt;redacted>,

thank you for asking, your questions are obvious but there was no room in the application. So I am happy to answer!

On 26 Sep 2022, at 12:54, &lt;redacted> wrote:

> you applied to the 2022-08 open call from NLnet. We have some questions regarding your project proposal #Seppo!.  
>  
> You requested 50000 euro. Can you provide some more detail on how you arrived at this amount? Could you provide a breakdown of the main tasks, and the associated effort?

There is a rough project plan summing up to 6 months full time effort, the figure mentioned. Milestones being

- Personas and user stories for the central use cases including lifecycle and housekeeping,
- Security audit, especially of the file system storage concept as part of mentoring,
- non-functional UX design prototype,
- Project plan/calendar.

In parallel approx. every 3 weeks

- UX tests with users from the target groups (a la Steve Krug).

Each approx. 3 weeks for

- Posts via CLI without web server (storage, engine),
- Posts via web interface,
- Reply, Boost via ActivityPub,
- (Un)Follow, I/O, via ActivityPub,
- Images,
- POSSE to Twitter, Instagram, Facebook, possibly via 3rd party relay,
- Optional further APIs: micro.blog, AtomPub, WebSub, pinboard.in

> You did not mention any relevant (open source) projects you've worked on. Can you still provide this information?

Prior software projects were

- GMX Customer Lifecycle and Ordering
- iOS Sharing Extension https://codeberg.org/mro/ShaarliOS
- Geohash service (OCaml) https://demo.mro.name/geohash.cgi

The first was as an employee (and is closed source) and the last 2 being open source.

See more in my cv &lt;redacted>. I am long-time member of GI.de and ACM.org and have delivered several related lightning talks at the CCC.de congress.

> What is your prior experience with ActivityPub?

My experience with ActivityPub is being an active community member at https://socialhub.activitypub.rocks/u/mro for the time I did a proof of concept for liking (see at https://codeberg.org/mro/activitypub) and exploring ActivityPub. The result was successful likes and unlikes to mastodon, peertube, pixelfed, pleroma, lemmy and friendica (but not to gnu.social where I wait for it's maintainer to respond).

> Is #Seppo! a plan, or is there already running code/designs/etc? What is the architecture, and in which language will it be crafted? What specs would you be implementing and what level of compliance with the W3C ActivityPub standard can users expect?

#Seppo! is already slowly emerging (see the develop branch at https://codeberg.org/mro/seppo) but will take long and lack polish being a side project for personal use. The architecture is a monolithic CGI built with OCaml.org. #Seppo!s main feature is self-operability for non-tech citizens, so it must be very lean on runtime dependencies and maintenance, but rock solid.

#Seppo! targets to implement https://www.w3.org/TR/activitypub/ federation at least with above mentioned implementations for Like, Respond, Boost, Follow, Unfollow and blocking. ActivityPub being the standard it is, requires repeated testing on each update of each single other server product. As a safety fallback there will remain a RFC 5005 Atom feed to subscribe to.

> Is this all brand new code, or would you be reusing existing components or efforts? What would be the on-disk format in which content is to be stored, both local and remote content? What does the security model look like?

Aside from basic OCaml opam packages, e.g. for unicode or json handling or encryption, the code related to ActivityPub is mostly to be built, a some important bits are already done. Goal is as low a complexity as possible. That's one pillar of the project.

Storage is in clear-text files on the server, the content in form of RFC 5005 paged atom feeds. All data is held in the form required by the consumers and updated on modification. No server code for reading.

> How does it keep remote content in sync?

What remote content to keep in sync do you think of?

> Would administration be through a user interface, or through config files? As a site becomes more successful and/or exposed to the outside world, other issues than easy deployment start to emerge: moderation, scalability, security etc. Can you clarify what your approach is for each?

The scaling to n=1 has surprising effects but one paramount precondition: The user must rent, own or control webspace and copy the CGI program there. The user legally owns that domain and acts on it's own behalf. This must be explained encouragingly and carefully and is another pillar of the project. Config is done via web, no need to touch the clear-text config, still everything is inspectable.

Administration is mostly obtaining the CGI program and copying it to said webspace plus an initial start (choosing time-zone and name). There is exactly one user per server, so there are no malicious (write-allowed) users and there is no moderation.

Scaling to 10k followers shouldn't be a problem as the server has to deliver static content only – which is written on edit by the said single, write-allowed user.

Incoming federation housekeeping is batched and throttled to keep #Seppo! responsive.

Thorough security analysis has to be done as part of the project repeatedly. The main idea is to keep complexity low and rather sacrifice features but safety.

> What problem are you trying to solve, and for which target group?

#Seppo! enables non-tech persons to post and like self-reliantly and still have a life.

For everyday citizens currently there is no offer to self-reliantly and responsibly - without help or say of others - publish anything the the internet. You always need services and helpers and have to accept T&Cs because there are components that you can't deploy yourself and those operators do at their own conditions.

And they tend to go out of business, get bought or otherwise change conditions over time.

#Seppo! can be deployed on dumb shared hosting webspace. No database and other server components required. It's simple to mirror, backup and restore. Shared webspace is offered by thousands of vendors at affordable price, it's a common infrastructure. And it's all you need.

The target group are individuals and small groups such as

- internet participants with up to 10k followers,
- associations and NGOs without staff,
- schools and youth projects,
- Mastodon and Twitter users who want to write on their own behalf,
- journalists who value decades of continuity,
- Freethinkers who hesitate to accept terms and conditions,
- Web hosters who want to offer this service.

> There are many different types of AP powered applicatoins (e.g. videostreaming, audio collections, podcasting, photosharing, etc) Is there a specific specialisation/niche #Seppo! would have within that sphere?

Yes: your long-term, personal, reliable internet home for short texts and single pictures.

No Myspace/Google+/del.icio.us shutdowns or acquisitions, Facebook/Twitter blocks, no arbitrary rules, just civil law. No Wordpress, mysql, npm, ruby or php vulnerabilities or operational risks, no operating system updates.

> Can you describe a more elaborate scenario how people would use the application?

Let me quote Patrick Breyer, MEP Pirate party: "ist eigentlich ganz charmant, auf den eigenen Server zu posten, ohne eine ganze Instanz betreiben zu müssen - solange das Cross-Posting ins Fediverse und die kommerziellen Plattformen funktioniert. Dann ja." https://chaos.social/@echo_pbreyer/106102514538681431

(en: "it's actually quite charming to post to your own server without having to run a whole instance - as long as cross-posting to the Fediverse and commercial platforms works. Then yes.")

There is a whole movement indieweb.org/POSSE that longs for a simple solution to use individually as a non-tech person.

> What is the main differentiator with projects like: https://nlnet.nl/project/Wordpress-ActivityPub, https://github.com/tsileo/microblog.pub and https://github.com/superseriousbusiness/gotosocial ? If people can easily install and maintain such applications as a plugin to Wordpress or with e.g. Yunohost, wouldn't the threshold be low enough?

Wordpress brings huge security update pressure and so does maintaining a whole server (yunohost). Non-tech persons won't do that on their own.

Go again is sponsored by Google and may change directions at any time as Google has done several times before. I have been watching Go for a decade, a predecessor of #Seppo! was https://github.com/mro/ShaarliGo, now I show that useful things can be created without GAFAM dependencies and thus decades of reliability.

#Seppo! uses no database, no scripting engine, no containers, no runtime dependencies, is compiled and statically linked, not scripted and is operated by it's single user. A tiny trusted computing base, based on mature technology but still part of the Fediverse.

#Seppo! relies on a not-for-profit funded, european toolchain (OCaml is lead by the french INRIA, has 25 years of maturity, is a statically and strongly typed, garbage collected and functional programming language). The community is small, stable, competent and friendly.

#Seppo! is as convenient as a SaaS but you keep your sovereignty and still have a life.

I hope I have been able to answer your questions to your satisfaction.

Kind regards,
Marcus Rohrmoser
