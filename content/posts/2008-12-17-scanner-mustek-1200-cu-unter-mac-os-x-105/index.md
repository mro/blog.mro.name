---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2008-12-17T14:07:21+00:00"
tags:
- Mustek
- OS X
- SANE
- Scanner
- Twain
title: Scanner Mustek 1200 CU unter Mac OS X 10.5
type: post
url: /2008/12/scanner-mustek-1200-cu-unter-mac-os-x-105/
yourls_shorturl:
- http://s.mro.name/j
---
Ein aktueller Mac mag den alten Scanner

{{< figure src="" caption="Scanner Mustek 1200 CU" width="200" height="150" >}}

zwar nicht von Haus aus, läßt sich aber dazu überreden: [Scanner Mustek 1200 CU unter Mac OS X 10.5][1] verweist auf [TWAIN SANE Interface for MacOS X][2]:

1. libusb
2. SANE backends
3. SANE Preference Pane
4. TWAIN SANE Interface

 [1]: http://blog.davidgraesser.de/?p=58
 [2]: http://www.ellert.se/twain-sane/