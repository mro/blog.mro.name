---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2008-11-01T14:20:06+00:00"
tags:
- inspire
- TED
title: Mobile Phones fighting Poverty
type: post
url: /2008/11/mobile-life/
yourls_shorturl:
- http://s.mro.name/3m
---
That article on slashdot about [Cellphone Banking Helping To Fight Poverty In India][1] (don't miss the [Video][2]) reminded me of two TED talks about much the same idea.

See [Iqbal Quadir's talk at TED.com][3].

{{<ted iqbal_quadir_says_mobiles_fight_poverty >}}

See [Jan Chipchase's talk at TED.com][4].

{{<ted jan_chipchase_on_our_mobile_phones >}}

 [1]: http://mobile.slashdot.org/article.pl?sid=08/10/25/1555256&from=rss
 [2]: http://www.technologyreview.com/video/?vid=40
 [3]: http://www.ted.com/index.php/talks/iqbal_quadir_says_mobiles_fight_poverty.html
 [4]: http://www.ted.com/index.php/talks/jan_chipchase_on_our_mobile_phones.html