---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-03-02T14:58:22+00:00"
title: 'Umfrage: Handy wichtiger als Liebe'
type: post
url: /2009/03/umfrage-handy-wichtiger-als-liebe/
yourls_shorturl:
- http://s.mro.name/3f
---
&#8222;[Die Mehrheit der Jugendlichen in Deutschland würde einer Studie zufolge eher auf den aktuellen Partner verzichten als auf Handy und Internet.][1]&#8220;

Naja – wenn man ein wenig Luft rausläßt bleibt doch noch, daß Web + Mobilfon (also: ein internetfähiger Taschenrechner mit Telephonfunktion) heute DAS Ding bei Jugendlichen ist.

Tauchen Spielekonsolen in der Studie eigentlich irgendwo auf?

 [1]: http://www.heise.de/newsticker/Umfrage-Handy-wichtiger-als-Liebe--/meldung/133799