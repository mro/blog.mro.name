---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2008-09-25T15:02:22+00:00"
title: Ein bißchen Überwachung
type: post
url: /2008/09/ein-bischen-uberwachung/
yourls_shorturl:
- http://s.mro.name/1l
---
Zwei Artikel zum Thema:

Für Videofreunde: [Film aus Überwachungsvideos](http://www.sueddeutsche.de/computer/938/310865/text/)

Für Computerbenutzer: [Razzia wegen Bundestrojaner](http://www.sueddeutsche.de/computer/557/310486/text/)