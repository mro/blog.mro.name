---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-03-25T17:24:07+00:00"
tags:
- BKA
- Sicherheit
- Zensur
title: 'Fwd: Internetzensur: Die grossen Lügen der Ursula von der Leyen'
type: post
url: /2009/03/fwd-internetzensur-die-grossen-lugen-der-ursula-von-der-leyen/
yourls_shorturl:
- http://s.mro.name/32
---
Die [Kinderschutzorganisation Carechild bezichtigt Ministerin von der Leyen der Lüge][1] – u.A. in einem [Interview in der &#8222;Radiowelt am Mittag&#8220;][2] heute auf [Bayern2][3].

<!--more-->Besonders geschmacklos finde ich nicht, daß Fr. von der Leyen Einwände gegen ihr Vorgehen als 

[&#8222;unterirdisch&#8220;][4] bezeichnet und damit als ausgeräumt ansieht, sondern **nichts dafür unternimmt gegen die Produzenten und Verbreiter strafrechtlich vorzugehen und die Server abzuschalten**.

Warum nicht?

Carechild formuliert das so: [&#8222;Bundesfamilienministerin will Kinderpr0n verbreiten&#8220;][5].

Offensichtlich geht's also nicht um Kinder. Aber worum dann? Evtl. [weiß Wolfgang Schäuble mehr][6].

 [1]: http://www.carechild.de/news/politik/internetzensur_die_grossen_luegen_der_ursula_von_der_leyen_572_1.html
 [2]: http://www.br-online.de/bayern2/radiowelt/radiowelt-09-kw13-beitr-landhofer-ID123797259318.xml
 [3]: http://www.br-online.de/bayern2/radiowelt/index.xml
 [4]: http://www.heise.de/newsticker/Rechtsprofessor-kritisiert-Vertragsentwurf-fuer-Kinderporno-Sperren--/meldung/132714
 [5]: http://www.carechild.de/carechild/careblog/bundefamilienministerin_will_kinderpornos_verbreiten_548_154.html
 [6]: http://www.heise.de/newsticker/Schaeuble-will-Kampf-gegen-Kinderpornografie-internationalisieren--/meldung/121809