---
title: 'Workshop: Interaktive Anfahrtspläne mit dem BayernAtlas'
url: /2021/05/bayernatlas/
draft: false
categories:
#- 🇩🇪
- en
date: 2021-05-04 11:05:00+02:00
tags:
- CMW
- Vortrag
- BayernAtlas
- BayernLab
type: post
author: Marcus Rohrmoser
---

Im Rahmen der [Chiemgauer Medienwochen](https://chiemgauer-medienwochen.de/) im Auftrag des [BayernLab
Traunstein](https://ldbv.bayern.de/digitalisierung/bayernlab/100.html):

[📄 Handout](https://marcus.rohrmoser.name/anfahrtsplaene)  
[📺 Video](https://mro.name/2021/bayernlab/anfahrtsplaene-mit-bayernatlas/2021-05-04-223657-anfahrtsplaene-bayernatlas-yt__81v2yowAw_--_720p.mp4)  

