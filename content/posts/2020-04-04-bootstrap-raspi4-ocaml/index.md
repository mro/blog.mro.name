---
title: Bootstrap Raspi4 + 🐫 OCaml
url: /2020/04/bootstrap-raspi4-ocaml/
draft: false
categories:
#- 🇬🇧
- en
- development
date: 2020-04-04 10:41:17+02:00
tags:
- Raspi
- OCaml
- Linux
type: post
author: Marcus Rohrmoser
---

Yesterday a new [Raspi4
(1GB)](https://www.heise.de/preisvergleich/raspberry-pi-4-modell-b-a2081093.html)
arrived and I'd like to have the OCaml toolchain for static builds running on it.
Headless for now, accessible via WIFI and ssh.

Let's go!

## Prepare SD-Card

At first have a look at
https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up, next download
the OS image:

<pre class="line-numbers"><code class="language-shell-session">$ curl -Lo 2020-02-13-raspbian-buster.txt https://downloads.raspberrypi.org/raspbian/release_notes.txt
$ curl -Lo 2020-02-13-raspbian-buster.zip.sha256 https://downloads.raspberrypi.org/raspbian/images/raspbian-2020-02-14/2020-02-13-raspbian-buster.zip.sha256
$ curl -Lo 2020-02-13-raspbian-buster.zip https://downloads.raspberrypi.org/raspbian/images/raspbian-2020-02-14/2020-02-13-raspbian-buster.zip
$ shasum -c 2020-02-13-raspbian-buster.zip.sha256 
2020-02-13-raspbian-buster.zip: OK
</code></pre>

Then insert a SD-Card and flash it (I use https://www.balena.io/etcher/).

## Inject ssh + wifi settings

Mount the SD-Card on a Linux-box, follow
https://www.raspberrypi.org/documentation/configuration/wireless/headless.md and
adjust as needed:

<pre class="line-numbers"><code class="language-shell-session">cat > /media/$(whoami)/boot/wpa_supplicant.conf &lt;&lt;EOF
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=<Insert country code here>

network={
 ssid="<Name of your WiFi>"
 psk="<Password for your WiFi>"
}
EOF
</code></pre>

And enable ssh by default

<pre class="line-numbers"><code class="language-shell-session">$ touch /media/$(whoami)/boot/ssh
</code></pre>

## Power On! and log in

1. insert the SD-Card,
2. connect the USB-Power,
3. plug it into an outlet.

<pre class="line-numbers"><code class="language-shell-session">$ ssh pi@raspberrypi
$ sudo apt-get update
$ sudo apt-get upgrade
$ sudo apt-get dist-upgrade
$ sudo reboot
$ # sudo systemctl disable vncserver-virtuald.service
$ sudo raspi-config
$ sudo apt-get install fortunes fortunes-de fortune-anarchism vim fish mosh
</code></pre>

## 🐫 OCaml

Based on https://blog.danny-willems.be/ocaml-raspberry-pi/

<pre class="line-numbers"><code class="language-shell-session">$ sudo apt-get install git m4 ocaml musl-tools
...
Setting up musl:armhf (1.1.21-2) ...
Setting up musl-dev:armhf (1.1.21-2) ...
Setting up musl-tools (1.1.21-2) ...
...
$ git clone https://github.com/ocaml/opam
$ cd opam
# today's master 1bab95d0d006ec08db022bc89907986b63f30883 fails, https://github.com/ocaml/opam/issues/4126
# use a known working version:
$ git checkout d6e4e4acb2a13e010631d9f27c638072030fd93d
# $ git checkout 2.0.6
$ ./configure
$ make lib-ext
$ make 
$ sudo make install
$ cd
$ opam init
$ opam switch --jobs=1 create 4.10.0+musl+static+flambda
$ eval $(opam env)
$ opam install dune
</code></pre>

Voilà!

P.S.: Be aware that `uname -m` is `armv7l` which won't run on a [Raspi
Zero](https://www.heise.de/preisvergleich/raspberry-pi-zero-w-a1585318.html) or
Raspi 1 but vice versa.

