---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2008-10-03T20:22:36+00:00"
tags:
- gentoo
- IrDA
- udev
title: Hotplug USB IrDA Dongle (gentoo Linux)
type: post
url: /2008/10/hotplug-usb-irda-dongle-gentoo-linux/
yourls_shorturl:
- http://s.mro.name/1q
---
[Udev][1] can help to get rid of the ugly `$ sudo irattach irda0 -s` in my [previous blog post][2].

I added the following udev rule:

<pre class="line-numbers"><code class="language-shell-session">$ cat /etc/udev/rules.d/95-usb-irda.rules
# Connect the IRDA USB Dongle
# see http://de.gentoo-wiki.com/Udev_Rules
# use "$ lsusb -v" to get the details about the dongle
ACTION=="add", BUS=="usb", SYSFS{manufacturer}==" Sigmatel Inc ", SYSFS{product}==" IrDA/USB Bridge", NAME="%k", RUN+="/usr/local/bin/usb-irda-add.sh"
# ACTION=="remove", BUS=="usb", SYSFS{idVendor}=="066f", SYSFS{idProduct}=="4200", RUN+="/usr/local/bin/usb-irda-remove.sh"
</code></pre>

combined with the script

<pre class="line-numbers"><code class="language-sh"># cat /usr/local/bin/usb-irda-add.sh
#!/bin/sh
# Init script for /etc/udev/rules.d/95-usb-irda.rules
# modprobe ircomm_tty
irattach irda0 -s
# http://osdir.com/ml/drivers.gnokii/2003-09/msg00014.html
echo 115200 > /proc/sys/net/irda/max_baud_rate
</code></pre>

and voila, the dongle hotplugs.

What's puzzling me, is that `/var/log/messages` shows multiple identical entries:

<pre class="line-numbers"><code class="language-log">Oct  3 22:06:17 sifr irattach: executing: '/sbin/modprobe irda0'
Oct  3 22:06:17 sifr irattach: executing: 'echo sifr > /proc/sys/net/irda/devname'
Oct  3 22:06:17 sifr irattach: executing: '/sbin/modprobe irda0'
Oct  3 22:06:17 sifr irattach: executing: 'echo 1 > /proc/sys/net/irda/discovery'
Oct  3 22:06:17 sifr irattach: executing: '/sbin/modprobe irda0'
Oct  3 22:06:17 sifr irattach: executing: 'echo sifr > /proc/sys/net/irda/devname'
Oct  3 22:06:17 sifr irattach: Starting device irda0
Oct  3 22:06:17 sifr irattach: executing: 'echo 1 > /proc/sys/net/irda/discovery'
Oct  3 22:06:17 sifr irattach: executing: 'echo sifr > /proc/sys/net/irda/devname'
Oct  3 22:06:17 sifr irattach: executing: '/sbin/modprobe irda0'
Oct  3 22:06:17 sifr irattach: Starting device irda0
Oct  3 22:06:17 sifr irattach: executing: 'echo 1 > /proc/sys/net/irda/discovery'
Oct  3 22:06:17 sifr irattach: executing: 'echo sifr > /proc/sys/net/irda/devname'
Oct  3 22:06:17 sifr irattach: Starting device irda0
Oct  3 22:06:17 sifr irattach: executing: 'echo 1 > /proc/sys/net/irda/discovery'
Oct  3 22:06:17 sifr irattach: Starting device irda0
</code></pre>

and that unplugging doesn't unload the kernel modules, despite the script

<pre class="line-numbers"><code class="language-shell-session">$ cat /usr/local/bin/usb-irda-remove.sh
#!/bin/sh
# Removal script for /etc/udev/rules.d/95-usb-irda.rules
rmmod ircomm_tty ircomm stir4200 irda crc_ccitt
</code></pre>

 [1]: http://de.wikipedia.org/wiki/Udev
 [2]: /2008/10/nokia-6610-linux-gentoo-stable-amd64/
