---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2014-06-20T21:43:43+00:00"
tags:
- Adblock
- Android
- PAC
- Proxy
- raspi
- Ruby
title: Ad Blocking Proxy = abloprox
type: post
url: /2014/06/ad-blocking-proxy-abloprox/
yourls_shorturl:
- http://mro.name/4o
---
as an act of digital hygiene, I installed [abloprox][1] on a raspi and added this [PAC][2] file to save some keystrokes when configuring:

<pre class="line-numbers"><code class="language-javascript">function FindProxyForURL(url, host) {
  if (shExpMatch(host,"*.fritz.box")) return "DIRECT";
  if (shExpMatch(host,"*.local")) return "DIRECT";
  if (shExpMatch(host,"*.akamaistream.net")) return "DIRECT";
  if (shExpMatch(host,"*.m945.mwn.de")) return "DIRECT";
  // auto config:
  // 1. ensure there's a host 'wpad' in the current network, see
  //   - https://en.wikipedia.org/wiki/Web_Proxy_Autodiscovery_Protocol#Context
  //   - http://fritz.box/net/network_user_devices.lua
  // 2. have a http webserver running on that host
  // 3. ensure http://wpad/wpad.dat or http://wpad.fritz.box/wpad.dat contains a PAC file like in http://blog.mro.name/2014/06/ad-blocking-proxy-abloprox/
  // return "PROXY wpad:3126"; // Default return condition is the proxy on host 'wpad'.
  return "PROXY &lt;hostname_of_the_raspi>:3126"; // Default return condition is the proxy.
}
</code></pre>

**Update**: I pushed this one step further recently and use the automatic proxy-configuration itself to filter — see <http://purl.mro.name/wpad>.

**Update:** hu – it took [Android until version 5.0 (lollipop) to support PAC][3].

 [1]: https://github.com/sononum/abloprox/
 [2]: https://en.wikipedia.org/wiki/Proxy_auto-config#The_PAC_File
 [3]: https://code.google.com/p/android/issues/detail?id=42696#c20
