---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-11-08T10:10:12+00:00"
tags:
- Cocoa
- Image
- iPhone
- iPhone4
- Retina Display
- UIImage
title: High-Res UIImage remote load
type: post
url: /2010/11/high-res-uiimage-remote-load/
yourls_shorturl:
- http://s.mro.name/3q
---
Loading [<tt>UIImage</tt>s automatically in high-resolution][1] works fine for locally stored images – but if you want to fetch them via remote URL you have to code yourself.

A simple, blocking but backward compatible (iOS >= 3.0, maybe even 2.0 but untested)  implementation could look like this:

<pre class="line-numbers"><code class="language-objc">
@implementation UIImage (MRORemote)

// add the @2x filename suffix
+(NSURL *)url2x:(NSURL *)url
{
  NSString *path = url.path;
  NSAssert(path != nil, @"");
  NSString *last = path.lastPathComponent;
  NSString *ext = path.pathExtension;
  NSAssert(last != nil, @"");
  NSAssert(ext != nil, @"");
  NSString *part = [last substringToIndex:MAX(0, last.length - ext.length - 1)];
  return [NSURL URLWithString:[NSString stringWithFormat:@"%@@2x.%@", part, ext] relativeToURL:[url absoluteURL]];
}

+(UIImage *)imageWithContentsOfURL:(NSURL *)url probe2x:(BOOL)probe2x
{
  if ( url == nil )
    return nil;
  UIScreen *screen = [UIScreen mainScreen];
  const CGFloat scale = [screen respondsToSelector:@selector(scale)] ? [screen scale] : 1.0f;
  if ( probe2x && 2.0f == scale && [UIImage respondsToSelector:@selector(imageWithCGImage:scale:orientation:)] ) {
    UIImage *img = nil;
    NSData *raw = [NSData dataWithContentsOfURL:[UIImage url2x:url]];
    if ( raw != nil )
      img = [UIImage imageWithData:raw];
    if ( img != nil )
      img = [UIImage imageWithCGImage:img.CGImage scale:scale orientation:img.imageOrientation];
    if ( img != nil )
      return img;
    NSAssert(raw == nil && img == nil, @"");
  }
  // MRLogD(@"loading %@", [url absoluteURL]);
  NSData *raw = [NSData dataWithContentsOfURL:url];
  if ( raw == nil )
    return nil;
  return [UIImage imageWithData:raw];
}
@end
</code></pre>

Use at your will but without any warranty.

 [1]: http://developer.apple.com/library/ios/#documentation/iphone/conceptual/iphoneosprogrammingguide/SupportingResolutionIndependence/SupportingResolutionIndependence.html%23//apple_ref/doc/uid/TP40007072-CH10-SW3