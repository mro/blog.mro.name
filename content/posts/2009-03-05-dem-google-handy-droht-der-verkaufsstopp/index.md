---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2009-03-05T17:40:29+00:00"
tags:
- Android
title: Dem Google-Handy droht der Verkaufsstopp – bitte?
type: post
url: /2009/03/dem-google-handy-droht-der-verkaufsstopp/
yourls_shorturl:
- http://s.mro.name/29
---
Heute scheint ein guter Tag für irre Schnapsideen zu sein. Nach [dem hier][1] kommt jetzt noch eine [Rundumschlag-Patenklage wegen eines UMTS Patents][2]. u.a. gegen Google, weil deren Android OS hilft Geräte zu betreiben, mit denen o.g. Patent verletzt wird. Daß identische Hardware auch ohne Android betrieben wird – geschenkt.

Daß dieser Jux zu einem Verkaufsstopp führt glaube ich erst wenn ich's sehe.

 [1]: http://www.netzeitung.de/politik/ausland/1291657.html
 [2]: http://www.golem.de/0903/65732.html