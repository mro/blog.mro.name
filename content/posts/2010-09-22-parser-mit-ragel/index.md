---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2010-09-22T21:51:29+00:00"
tags:
- Cocoa
- CocoaHeads
- iPhone
- NSXMLParser
- ragel
- SVG
- Vortrag
- XML
title: 'Vortrag: Parser mit Ragel'
type: post
url: /2010/09/parser-mit-ragel/
yourls_shorturl:
- http://s.mro.name/2t
---
* **[Parser mit Ragel][1]** – komplizierte Grammatiken und rattenschnelles XML.
* [Warum Parser bauen?][2]
* [Was spricht gegen Ad-Hoc Parsing (a.k.a. Gefrickel)?][3]
* [Quelltext Impression][4]
* [Wie komme ich drauf?][5]
* [Anschnallen: Die Bausteine][6]
* [Beispiel: SVG Path Parser][7]
* [Beispiel: XML Parser][8]
* [Ausblick: Zustandsautomaten][9]
* [Vielen Dank][10]

[Die Folien vom Vortrag am 22. September][11] bei den [CocoaHeads München][12].

 [1]: http://wiki.mro.name/cocoaheads/ragel#parser_mit_ragel
 [2]: http://wiki.mro.name/cocoaheads/ragel#warum_parser_bauen
 [3]: http://wiki.mro.name/cocoaheads/ragel#was_spricht_gegen_ad-hoc_parsing_aka_gefrickel
 [4]: http://wiki.mro.name/cocoaheads/ragel#quelltext_impression
 [5]: http://wiki.mro.name/cocoaheads/ragel#wie_komme_ich_drauf
 [6]: http://wiki.mro.name/cocoaheads/ragel#anschnallendie_bausteine
 [7]: http://wiki.mro.name/cocoaheads/ragel#beispielsvg_path_parser
 [8]: http://wiki.mro.name/cocoaheads/ragel#beispielxml_parser
 [9]: http://wiki.mro.name/cocoaheads/ragel#ausblickzustandsautomaten
 [10]: http://wiki.mro.name/cocoaheads/ragel#vielen_dank
 [11]: http://wiki.mro.name/_export/s5/cocoaheads/ragel
 [12]: http://cocoaheads.org/de/Munich/