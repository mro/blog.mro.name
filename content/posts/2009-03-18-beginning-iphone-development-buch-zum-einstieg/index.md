---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2009-03-18T11:21:57+00:00"
tags:
- Book
- iPhone
title: Beginning iPhone Development / Buch zum Einstieg
type: post
url: /2009/03/beginning-iphone-development-buch-zum-einstieg/
yourls_shorturl:
- http://s.mro.name/1j
---
Das Buch [Beginning iPhone Development][1] sieht recht gut aus. Bei [Google Books][2] kann man reingucken, die [Quellen der Beispiele][3] sind online zu bekommen.

 [1]: http://iphonedevbook.com/
 [2]: http://books.google.co.in/books?id=WV9glgdrrrUC&printsec=frontcover&hl=de#PPP1,M1http://books.google.co.in/books?id=WV9glgdrrrUC&printsec=frontcover&hl=de#PPP1,M1
 [3]: http://iphonedevbook.com/BeginningiPhoneDev.121808.zip