---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2009-07-26T13:39:44+00:00"
tags:
- iPhone
- Objective C
- OCUnit
- SenTestingKit
- Unit Test
- XCode
title: Unit Testing / iPhone
type: post
url: /2009/07/unit-testing-iphone/
yourls_shorturl:
- http://s.mro.name/f
---
Having Eclipse & JUnit in mind I missed unit testing quite a bit while developing with XCode for iPhone.

As a first shot, I [set SenTestingKit up as explained by it's author][1] and it works really nicely. One thing I still miss is Step'n'Trace debugging the tests.

Other intros to the topic from Apple about [OCUnit (2005)][2] and [SenTestingKit (2008)][3] are ok but not as good as from the author of SenTestingKit.

Didn't examine other Unit Test frameworks like e.g. the [one from Google][4] yet.

 [1]: http://www.sente.ch/s/?p=535&lang=en
 [2]: http://developer.apple.com/tools/unittest.html
 [3]: http://developer.apple.com/documentation/developertools/Conceptual/UnitTesting/Articles/CreatingTests.html#//apple_ref/doc/uid/TP40002171-125425
 [4]: http://code.google.com/p/google-toolbox-for-mac/wiki/iPhoneUnitTesting