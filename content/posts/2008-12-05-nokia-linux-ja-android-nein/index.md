---
author: Marcus Rohrmoser
categories:
- de
- seenontheweb
date: "2008-12-05T16:24:41+00:00"
tags:
- Android
- Linux
- Maemo
- Nokia
title: 'Nokia: Linux ja, Android nein.'
type: post
url: /2008/12/nokia-linux-ja-android-nein/
yourls_shorturl:
- http://s.mro.name/n
---
[heise schreibt: Linux-Smartphones: Nokia bleibt Eigenbrötler][1]:

&#8230;&#8220;Ich sehe nichts, worin Android besser wäre als Maemo.&#8220;&#8230;

Naja, so richtig überzeugt von Maemo klingt das ja nicht.

 [1]: http://www.heise.de/mobil/Linux-Smartphones-Nokia-bleibt-Eigenbroetler--/newsticker/meldung/119942