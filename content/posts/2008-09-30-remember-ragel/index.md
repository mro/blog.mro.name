---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2008-09-29T23:49:24+00:00"
tags:
- piccolo2d
- ragel
- Ruby
- SVG
title: Remember Ragel
type: post
url: /2008/09/remember-ragel/
yourls_shorturl:
- http://s.mro.name/1b
---
Es ist schon etwas her aber dennoch einen kurzen Eintrag hier sicher wert.

[Ragel][1] habe ich für ein Experiment (ist [SVG][2] als 2D [Szenengraph][3] Format für [piccolo2d][4] machbar?) benutzt um [einige Parser][5] zu bauen.

<!--more-->Als Vorlage dienten die 

[BNF Grammatiken direkt aus der W3C SVG Spezifikation][6]. Nachdem der erste funktionsfähige Stand erreicht war habe ich noch ein [anderes Projekt][7] entdeckt, das genau dasselbe Problem ebenfalls mit Ragel gelöst hat und mich von deren Ansatz Mehrdeutigkeiten aufzulösen inspirieren lassen.

Schön ist dabei

* BNF Grammatiken direkt verwerten (die vom [W3C][6] mußten fast nur von [unten nach oben][8] umgedreht werden),
* dadurch glasklare Definition der erlaubten formalen Sprache,
* keinerlei Abhängigkeiten von irgendwelchen Libs,
* irre schneller Parser-Code,
* Ragel läuft auf zig Systemen,
* Ragel spuckt aktuell C, C++, Objective-C, C#, D, Java und Ruby aus,
* wird von richtig hellen Köpfen benutzt (nette Beispiele im Netz),
* und die Doku taugt auch noch was!

Für die Ruby Freunde unter uns gibt's auch einen [speziellen netten Blog Eintrag][9].

 [1]: http://research.cs.queensu.ca/~thurston/ragel/ "Ragel"
 [2]: http://www.w3.org/TR/SVG11 "SVG"
 [3]: http://de.wikipedia.org/wiki/Szenengraph
 [4]: http://www.piccolo2d.org/
 [5]: http://code.google.com/p/piccolo2d/source/browse/?r=390#svn/piccolo2d.java/branches/svg/svg/src/main/ragel
 [6]: http://www.w3.org/TR/SVG11/paths.html#PathDataBNF
 [7]: https://lib2geom.svn.sourceforge.net/svnroot/lib2geom/lib2geom/trunk/src/2geom/svg-path-parser.rl
 [8]: http://code.google.com/p/piccolo2d/source/browse/piccolo2d.java/branches/svg/svg/src/main/ragel/PathParser.rl?r=382#131
 [9]: http://www.devchix.com/2008/01/13/a-hello-world-for-ruby-on-ragel-60/