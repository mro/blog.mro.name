---
author: Marcus Rohrmoser
categories:
- en
- seenontheweb
date: "2009-04-28T19:53:08+00:00"
tags:
- Africa
- cellphone
- Computer
- Eno
- inspire
- TED
title: Maybe enough Africa in cellphones?
type: post
url: /2009/04/maybe-enough-africa-in-cellphones/
yourls_shorturl:
- http://s.mro.name/2h
---
Brian Eno once said [the problem with computers is, it's just not enough Africa in them][1]. Well – there's definitively more of it in cell phones.

Computers in Africa:

[original Talk at TED][2]

Africa in the Computer:
  
[original Talk at TED][3]

 [1]: http://www.wired.com/wired/archive/3.05/eno_pr.html
 [2]: http://www.ted.com/talks/view/id/523
 [3]: http://www.ted.com/talks/view/id/320