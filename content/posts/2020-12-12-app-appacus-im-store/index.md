---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2020-12-12T21:05:26+00:00"
tags:
- App Store
- github
- GPL
- iOS
- Appacus
title: "App 'Appacus' im Store 📱"
type: post
url: /2020/12/app-appacus-im-store/
---

brandneu im [App Store][1]: [Appacus][2].

 [1]: http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1512396111&mt=8
 [2]: http://mro.name/appacus
