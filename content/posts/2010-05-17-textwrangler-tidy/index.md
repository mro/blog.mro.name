---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2010-05-17T09:30:14+00:00"
tags:
- sh
- html
- OS X
- TextWrangler
- tidy
- utf8
- xhtml
title: TextWrangler + tidy
type: post
url: /2010/05/textwrangler-tidy/
yourls_shorturl:
- http://s.mro.name/1h
---
as I didn't get [TidyService][1] to work correctly with [UTF8 umlauts][2], I created a [UNIX Shell Script][3] wrapper for [html tidy as it comes with OS X][4] that does the job at least for [TextWrangler][5]:

1. open TextWranglers &#8222;Unix Filters Folder&#8220;
   {{< figure  src="/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-17-um-11.15.12.png" caption=""  width="300"  height="211" >}}
2. create a file named e.g. &#8222;Tidy Html.sh&#8220;,
3. paste the following lines into the file and save it:

<pre class="line-numbers"><code class="language-sh">#!/bin/sh
# run "tidy" on the file given as 1st (and only) parameter.
#
/usr/bin/tidy -utf8 -asxhtml -indent -wrap 100 -quiet "$1" 2> /dev/null
</code></pre>

4. now you can run
   {{< figure  src="/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-20-um-02.25.44.png" caption=""  width="300"  height="206" >}}
   tidy on files opened in TextWrangler, even remote files.
5. assign a keyboard shortcut (I used CTRL-T in the shot above) via the &#8222;Unix Filters&#8220; Palette:
   {{< figure  src="/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-20-um-02.22.56.png" caption=""  width="300"  height="160" >}}

 [1]: http://www.pixelfreak.net/tidy_service/
 [2]: http://en.wikipedia.org/wiki/UTF-8
 [3]: http://en.wikipedia.org/wiki/Bourne_shell
 [4]: http://developer.apple.com/mac/library/documentation/Darwin/Reference/ManPages/man1/tidy.1.html
 [5]: http://www.barebones.com/products/textwrangler/
 [6]: http://blog.mro.name/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-20-um-20.25.44.png
 [7]: http://blog.mro.name/wp-content/uploads/2010/05/Bildschirmfoto-2010-05-20-um-20.22.56.png
