---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-04-21T14:28:41+00:00"
tags:
- GSM Code
- iPhone
- T-Mobile
title: GSM Codes zum Ausschalten der Mobilbox
type: post
url: /2009/04/gsm-codes-zum-ausschalten-der-mobilbox/
yourls_shorturl:
- http://s.mro.name/7
---

[Nachtrag: Auch nicht schlecht: [GSM-Codes für's iPhone](http://blog.webmart.de/2007/11/20/iphone-visual-voicemail-codes-fur-rufumleitung/).]

im Moment mag ich keine Mobilbox haben – das scheint aber für iPhones nicht vorgesehen zu sein.

Aber da gibt's ja noch die [GSM Codes](http://mobilfaq.in-ulm.de/t-mobile.faq.html) (wie ich heute erfahren habe – danke, Ex-Chef!):

<!--more-->

{{< figure src="/wp-content/uploads/2009/04/img_00041.png" caption="Statusabfrage" width="192" height="288" >}}
  
{{< figure src="/wp-content/uploads/2009/04/img_00051.png" caption="Status vorher" width="192" height="288" >}}
  
{{< figure src="/wp-content/uploads/2009/04/img_00061.png" caption="Umleitung Ausschalten" width="192" height="288" >}}

{{< figure src="/wp-content/uploads/2009/04/img_00071.png" caption="Feedback" width="192" height="288" >}}
  
{{< figure src="/wp-content/uploads/2009/04/img_00081.png" caption="Statusabfrage" width="192" height="288" >}}
  
{{< figure src="/wp-content/uploads/2009/04/img_00091.png" caption="Status nachher" width="192" height="288" >}}
  
Ach ja – die 0179 kommt weil ich meine Nummer von O2 zu T-Mobile mitgenommen habe.
