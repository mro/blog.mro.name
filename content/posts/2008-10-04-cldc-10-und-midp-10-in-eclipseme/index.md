---
author: Marcus Rohrmoser
categories:
- de
- development
date: "2008-10-04T21:22:39+00:00"
tags:
- 6610
- CLDC
- EclipseME
- J2ME
- MIDP
- Nokia
title: CLDC 1.0 und MIDP 1.0 in EclipseME
type: post
url: /2008/10/cldc-10-und-midp-10-in-eclipseme/
yourls_shorturl:
- http://s.mro.name/22
---
da das [Nokia 6610][1] schon ein recht betagtes Mobilfon ist und nur CLDC 1.0 und MIDP 1.0 unterstützt, sollte das im &#8222;Eclipse > Window > Preferences > J2ME > Device Management&#8220; eingestellt werden.

Ich habe also das &#8222;DefaultColorPhone&#8220; unter Beibehaltung des Namens (sic!) geklont (duplicate):

{{< figure src="/wp-content/uploads/2008/10/preferences_j2me_device_management.png" caption="J2ME Device Management" >}}

und wie folgt eingestellt:

{{< figure src="/wp-content/uploads/2008/10/edit_defaultcolorphone_definition.png" caption="Edit DefaultColorPhone Definition" >}}

Anschließend läßt sich per &#8222;Eclipse > File > New > Project > J2ME > Midlet Suite&#8220; ein neues Projekt anlegen.

Vorsicht, daß auch wirklich das richtige DefaultColorPhone mit den richtigen CLDC und MIDP jars zugeordnet ist:

{{< figure src="/wp-content/uploads/2008/10/firstnokia6610projectpackageexplorer.png" caption="Package Explorer Ansicht" >}}

Und schon kann's losgehen!

Evtl. sind die (nicht markierten) anderen jars noch nicht korrekt, aber Rom wurde auch nicht an einem Tag erbaut, oder?

 [1]: http://www.forum.nokia.com/devices/6610