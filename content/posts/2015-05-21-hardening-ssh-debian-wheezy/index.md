---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2015-05-21T06:42:49+00:00"
tags:
- Debian
- LogJam
- security
- SSH
- Wheezy
title: hardening ssh (debian wheezy)
type: post
url: /2015/05/hardening-ssh-debian-wheezy/
yourls_fetching:
- 1
---
[LogJam][1] requires some action. ([Article in german][2])

## Update (open-)ssh to a recent version (6.6)

<pre class="line-numbers"><code class="language-shell-session">$ echo "deb http://ftp.de.debian.org/debian wheezy-backports main" | sudo tee -a /etc/apt/sources.list
$ sudo apt-get -u update
$ sudo apt-get install -t wheezy-backports ssh
$ sudo apt-get autoremove
</code></pre>

## Harden crypto

<pre class="line-numbers"><code class="language-shell-session">$ sudo tee -a /etc/ssh/sshd_config &lt;&lt;EOF_SSH_CFG

# https://stribika.github.io/2015/01/04/secure-secure-shell.html
KexAlgorithms curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr
MACs hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-ripemd160-etm@openssh.com,umac-128-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,hmac-ripemd160,umac-128@openssh.com

EOF_SSH_CFG
$ sudo /etc/init.d/ssh restart
</code></pre>

## Keys

Replace host keys, /etc/ssh/sshd_config

<pre class="line-numbers"><code class="language-shell-session">Protocol 2
# https://stribika.github.io/2015/01/04/secure-secure-shell.html#server-authentication
HostKey /etc/ssh/ssh_host_ed25519_key
HostKey /etc/ssh/ssh_host_rsa_key
</code></pre>

and

<pre class="line-numbers"><code class="language-shell-session">$ cd /etc/ssh
$ sudo rm ssh_host_*key*
$ sudo ssh-keygen -t ed25519 -f ssh_host_ed25519_key &lt; /dev/null
$ sudo ssh-keygen -t rsa -b 4096 -f ssh_host_rsa_key &lt; /dev/null
$ sudo /etc/init.d/ssh restart
</code></pre>

## Resources

* <https://stribika.github.io/2015/01/04/secure-secure-shell.html>
* <https://bettercrypto.org/static/applied-crypto-hardening.pdf>

 [1]: https://weakdh.org/
 [2]: http://heise.de/-2657502
