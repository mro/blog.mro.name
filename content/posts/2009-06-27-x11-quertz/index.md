---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2009-06-27T11:17:23+00:00"
tags:
- gentoo
- HAL
- Linux
- Quertz
- Tastatur
- X11
title: X11 Quertz
type: post
url: /2009/06/x11-quertz/
yourls_shorturl:
- http://s.mro.name/3c
---
Seltsamerweise hat mein [gentoo Rechner][1] vergessen, daß eine [Quertz][2]-Tastatur dranhängt.

<!--more-->

Die `/etc/hal/fdi/policy/10-xinput-configuration.fdi` Konfig-Datei hat komplett gefehlt; also wie [hier beschrieben][3]

<pre class="line-numbers"><code class="language-xml">&lt;?xml version="1.0" encoding="UTF-8"?>
&lt;!--
Section "InputDevice"
   Driver "evdev"
   Option "XkbLayout" "de"
EndSection
-->

&lt;deviceinfo version="0.2">
   &lt;match key="info.capabilities" contains="input.keyboard">
      &lt;merge key="input.x11_options.XkbLayout" type="string">de&lt;/merge>
      &lt;merge key="input.x11_options.XkbVariant" type="string">nodeadkeys&lt;/merge>
   &lt;/match>
&lt;/deviceinfo>
</code></pre>

eingetragen und alle sind glücklich.

 [1]: http://de.wikipedia.org/wiki/Gentoo_Linux
 [2]: http://de.wikipedia.org/wiki/Quertz
 [3]: http://de.gentoo-wiki.com/wiki/Xorg_1.5_Upgrade
