---
author: Marcus Rohrmoser
categories:
- de
- offtopic
date: "2011-03-22T11:25:31+01:00"
featured_image: /wp-content/uploads/2011/03/Aufschrift-auf-Schutzmantelmadonna-im-Palas-der-Burg-zu-Burghausen.jpg
tags:
- Bayern
- Bayernkurier
- Burghausen
- CSU
- Elisabethkapelle
- Fibonacci
- Frühgotik
- Geschichte
- Heinrich XIII
- Islam
- Kunstgeschichte
- Laptop
- Lederhose
- Mathematik
- Rant
- Ziffern
title: 'Randnotiz: Die bayerische Frühgotik und der Islam'
type: post
url: /2011/03/randnotiz-die-bayerische-fruehgotik-und-der-islam/
yourls_shorturl:
- http://mro.name/45
yourls_tweeted:
- 1
---
Neulich war ich auf der [Burg zu Burghausen][1]<sup>1</sup> und entdeckte auf einem der ältesten Bilder (eine Schutzmantelmadonna) im Palas eine merkwürdige Aufschrift:

<p style="text-align: center;">
  <a href=""><img class="aligncenter" title="" src="/wp-content/uploads/2011/03/Aufschrift-auf-Schutzmantelmadonna-im-Palas-der-Burg-zu-Burghausen.jpg" alt="" width="440" height="151" /></a>
</p>

<!--more-->

Das Bild kam unter einem Stifterbild der [Hedwigskapelle][2]<sup>1</sup> zum Vorschein und ist eine Rekonstruktion. Offiziell datiert ist das Bild auf

<p style="text-align: center; font-size: 128pt; margin: 0; font-family: serif">
  1507
</p>

Mir fiel jedoch die verblüffende Ähnlichkeit mit der [Zahl 1258 in arabischer Schreibweise][3]<sup>2</sup>

<p style="text-align: center; font-size: 128pt; margin: 0;">
  ١٢٥٨
</p>

auf, was mir und meiner kunstgeschichtlichen Expertenbegleiterin keine Ruhe ließ. Ergibt eine solche Jahreszahl einen Sinn? Könnte ein Künstler in Burghausen die in Arabien gebräuchlichen Ziffern gekannt haben? Und wieso benutzte er nicht die in Europa (heute) übliche Schreibweise?

Eine zackige Wikipedia Recherche ergab:

* 1202 vollendete [Leonardo Fibonacci][4]<sup>3</sup> sein [Liber Abbaci][5]<sup>3</sup>, in dem er laut Wikipedia &#8222;[unter anderem die indischen Ziffern vorstellt und diese in der Tat als »indische Ziffern« und nicht als »arabische Ziffern« bezeichnet.][6]&#8222;<sup>2</sup>,
* 1235 empfängt Otto II., Vater und Vorgänger von [Herzog Heinrich XIII.][7]<sup>4</sup>, &#8222;[den aus Italien heimkehrenden Friedrich II. in Burghausen.][8]&#8222;<sup>5</sup> — [Friedrich II.][9]<sup>6</sup> war immerhin Kaiser des [Heiligen Römischen Reiches][10] und hatte vermutlich Arabisch-Kenntnisse<sup>7</sup>, ihm hat Fibonacci das Buch gewidmet,
* 1255 entsteht die [Elisabethkapelle im Palas der Burg zu Burghausen: &#8222;Die innere Schlosskapelle aus diesem Jahr gilt als die älteste frühgotische Kirche im südbayerischen Raum.&#8220;][11]<sup>1</sup> – gebaut von Herzog Heinrich XIII.

## Fazit

* Spannend wäre eine Verbindung zwischen Burghausen und Fibonacci abzuklopfen,
  * rege Verbindungen nach Italien bestanden allemal und lassen sich sicher durch kunsthistorische Analysen oder Wirtschaftsbücher überprüfen,
  * eine Eingrenzung des Künstlermilieus und Untersuchung ob's Verbindungen zu Algebra oder Geometrie aus dem Liber Abbaci gibt,
* zumindest **zur bayerischen Frühgotik gehört islamische Gelehrsamkeit** allemal, das sagt der Laptop,

<p style="text-align: center; font-size: 24pt; margin: 5ex 0;">
  aber <a href="http://www.bayernkurier.de/?id=224&showUid=1288">so posaunt's aus der Lederhose</a>
</p>

<http://www.zeit.de/politik/deutschland/2011-03/innenminister-islam-friedrich>

<p style="text-align: center">
* * *
</p>

## Quellen

1. Seite „Burg zu Burghausen“. In: Wikipedia, Die freie Enzyklopädie.  
   Bearbeitungsstand: 11. Januar 2011, 12:48 UTC.  
   URL: [http://de.wikipedia.org/w/index.php?title=Burg\_zu\_Burghausen&oldid=83748961][1]  
   (Abgerufen: 21. März 2011, 16:00 UTC)
2. Seite „Indische Zahlendarstellung“. In: Wikipedia, Die freie Enzyklopädie.  
   Bearbeitungsstand: 15. März 2011, 10:46 UTC.  
   URL: <http://de.wikipedia.org/w/index.php?title=Indische_Zahlendarstellung&oldid=86473530>  
   (Abgerufen: 21. März 2011, 16:03 UTC)
3. Seite „Leonardo Fibonacci“. In: Wikipedia, Die freie Enzyklopädie.    
   Bearbeitungsstand: 19. Februar 2011, 17:59 UTC.  
   URL: <http://de.wikipedia.org/w/index.php?title=Leonardo_Fibonacci&oldid=85499344>  
   (Abgerufen: 21. März 2011, 16:05 UTC)
4. Seite „Heinrich XIII. (Bayern)“. In: Wikipedia, Die freie Enzyklopädie.  
   Bearbeitungsstand: 14. Februar 2011, 17:08 UTC.  
   URL: [http://de.wikipedia.org/w/index.php?title=Heinrich\_XIII.\_(Bayern)&oldid=85290895][7]  
   (Abgerufen: 21. März 2011, 16:08 UTC)
5. Seite „Geschichte der Stadt Burghausen“. In: Wikipedia, Die freie Enzyklopädie.  
   Bearbeitungsstand: 4. März 2011, 10:16 UTC.  
   URL: [http://de.wikipedia.org/w/index.php?title=Geschichte\_der\_Stadt_Burghausen&oldid=86028680][12]  
   (Abgerufen: 22. März 2011, 09:02 UTC)
6. Seite „Friedrich II. (HRR)“. In: Wikipedia, Die freie Enzyklopädie.  
   Bearbeitungsstand: 21. März 2011, 06:52 UTC.  
   URL: [http://de.wikipedia.org/w/index.php?title=Friedrich\_II.\_(HRR)&oldid=86720244][9]  
   (Abgerufen: 22. März 2011, 09:07 UTC)
7. Siehe [hier][13]

P.S.: Sämtliche Zitate und Quellen sind zwar nur aus Wikipedia, aber als solche ausgewiesen und verlinkt – das hier ist schließlich ein seriöser Blog und keine juristische Dissertation.

 [1]: http://de.wikipedia.org/w/index.php?title=Burg_zu_Burghausen&oldid=83748961
 [2]: http://de.wikipedia.org/w/index.php?title=Burg_zu_Burghausen&oldid=83748961#F.C3.BCnfter_Burghof
 [3]: http://de.wikipedia.org/w/index.php?title=Indische_Zahlendarstellung&oldid=86473530
 [4]: http://de.wikipedia.org/w/index.php?title=Leonardo_Fibonacci&oldid=85499344
 [5]: http://de.wikipedia.org/w/index.php?title=Leonardo_Fibonacci&oldid=85499344#Der_Inhalt_des_Liber_abbaci
 [6]: http://de.wikipedia.org/w/index.php?title=Indische_Zahlendarstellung&oldid=86473530#Liber_abaci
 [7]: http://de.wikipedia.org/w/index.php?title=Heinrich_XIII._(Bayern)&oldid=85290895
 [8]: http://de.wikipedia.org/w/index.php?title=Geschichte_der_Stadt_Burghausen&oldid=86028680#Hochmittelalter
 [9]: http://de.wikipedia.org/w/index.php?title=Friedrich_II._(HRR)&oldid=86720244
 [10]: http://de.wikipedia.org/wiki/Heiliges_R%C3%B6misches_Reich
 [11]: http://de.wikipedia.org/w/index.php?title=Burg_zu_Burghausen&oldid=83748961#Hochmittelalter
 [12]: http://de.wikipedia.org/w/index.php?title=Geschichte_der_Stadt_Burghausen&oldid=86028680
 [13]: http://de.wikipedia.org/w/index.php?title=Friedrich_II._(HRR)&oldid=86720244#cite_note-0
