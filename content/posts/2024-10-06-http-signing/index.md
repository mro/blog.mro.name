---
title: 'Http Request Signing'
url: /2024/10/http-signing/
draft: false
categories:
- en
date: 2024-10-06T17:32:42+0200
tags:
- ActivityPub
- Federation
- HTTP
- Seppo
- Signing
type: post
author: Marcus Rohrmoser
---

Part IV of ‘Implementing Federation’.

# What is the situation?

Today many use an implementation[^impl] of a draft[^cav] depite it stating "It is
inappropriate to use Internet-Drafts as reference material or to cite them other
than as "work in progress.". And, beyond that, having expired 2020. There are
unexpired drafts, but still. Nothing final.

Some[^stat] implementors use the algorithm rsa-sha256[^2] that is deprecated in cavage (since
2011), others the hs2019. But with the same hash (SHA256) and digital signature
algorithm (RSASSA-PKCS1-v1_5) as rsa-sha256 which are not the recommended ones
for hs2019 to say the least. Follow-up drafts make me doubt the old crypto being
legit at all or rather a typo in cavage (comma instead of colon). Concerning
the hash, the referenced rfc6234 "makes open source code performing these SHA
hash functions conveniently available to the Internet community." and SHA-512
(explicitly mentioned by cavage) isn't used by quite some implementors, but a lesser one from said RFC.
Crypto done wrong tends to be worse than no crypto at all.

The question "why is that" leads nowhere as adoption seems not standards
driven[^3], but ad-hoc and arbitrary. Incentives remain subject of speculation.
Once signing was in the wild, as an ActivityPub server implementor you had the
choice of either doing ActivityPub without signing and nobody talking to your
servers or accepting whatever was thrown at you, be it standard or not or even
erroneously-implemented, expired drafts. rsa-sha256 being the lowest bar. Alas,
there is no compliance test covering this.

So if you want to do signing in 2024, you can choose from a deprecated algorithm
rsa-sha256 from a "work in progress" in a 2011 draft, expired in 2020. Or the
parody of an algorithm declared hs2019 using the exact crypto standards as
rsa-sha256.

# The elephant in the room: benefit?

All this complexity to sign http requests. With a 'private' key that resides
happily on the server and no user will likely ever see. While TLS[^tls] already
being a "SHOULD" on ActivityPub[^aptls]. So what scenarios, be it real world or
contrived, benefit from such http signatures?

I only can see the possibility to detect messages being tampered with during
transmission over the wire. Either server can always lie (the sending server holds
the key and the receiving server can just display whatever it pleases). For
transport security there's a dedicated protocol if you want that.

# Is http signing to date anything other than an interop obstacle?

A ritual to tell the faithful maybe? Security theater? Cargo cult? Gaslighting?

I don't know. Tell me your take on it: [@social@mro.name](https://seppo.social/aseppototry/apchk.cgi/webfinger?resource=acct:social@mro.name&redirect=http://webfinger.net/rel/profile-page).
I am curious, especially concerning the tangible benefits for end users[^RFC8890].

[^impl]:    Implementation as used in https://Seppo.Social  
            https://codeberg.org/seppo/seppo/src/commit/7e676b1/lib/http.ml#L317
[^cav]:     Signing HTTP Messages  
            https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12#
[^stat]:    Are We HS2019 Yet? https://arewehs2019yet.vpzom.click
[^tls]:     Transport Layer Security  
            https://datatracker.ietf.org/doc/html/rfc2246
[^aptls]:   https://www.w3.org/TR/activitypub/#x3-1-object-identifiers
[^2]:       https://datatracker.ietf.org/doc/html/draft-cavage-http-signatures-12#appendix-E.2  
            Algorithm Name: rsa-sha256  
            Status: deprecated, specifying signature algorithm enables attack vector.
[^3]:       https://blog.mro.name/2023/12/implementing-federation-i/#request-signing
[^RFC8890]: The Internet is for End Users  
            https://www.rfc-editor.org/rfc/rfc8890.html
