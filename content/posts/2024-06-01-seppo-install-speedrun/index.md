---
title: '#Seppo Install Speedrun (Video 📺)'
url: /2024/06/seppo-install-speedrun/
draft: false
categories:
- en
date: 2024-06-01T16:18:18+0200
tags:
- ActivityPub
- Admin
- Install
- Seppo
- Speedrun
- Video
type: post
author: Marcus Rohrmoser
---

Recently I made a first [#Seppo](https://seppo.social) Install Speedrun clip,
more to come. Assumes you already have [webspace](https://seppo.social/en/support/#webspace):

<video src="2024-05-31-193852-_--_720p_video.mp4" controls/>

What do you think should be mentioned more explicitly, what less?

Comments appreciated below or to [@mro@digitalcourage.social](https://digitalcourage.social/@mro).
