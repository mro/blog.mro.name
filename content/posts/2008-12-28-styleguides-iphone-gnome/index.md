---
author: Marcus Rohrmoser
categories:
- en
- screendesign
date: "2008-12-28T16:45:15+00:00"
tags:
- Gnome
- HCI
- iPhone
- Styleguide
title: Styleguides – iPhone & Gnome
type: post
url: /2008/12/styleguides-iphone-gnome/
yourls_shorturl:
- http://s.mro.name/2r
---
Hunting for design guidelines, I found the [Gnome styleguide][1] to be rather detailed whereas the
[iPhone HCI styleguide][2](1) remains quite general.

I was looking for advice how to use colours (I'll blog about this soon) – and some (official)
thoughts about when to use premade [widgets][3] and when to use custom ones.

(1) Due to Apple's rather esoteric communication policy, you need to register to see the webpage.
Call me a coward, but I won't mirror the pdf.

 [1]: http://library.gnome.org/devel/hig-book/
 [2]: http://developer.apple.com/iphone/library/documentation/UserExperience/Conceptual/MobileHIG
 [3]: http://en.wikipedia.org/wiki/GUI_widget