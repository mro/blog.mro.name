---
author: Marcus Rohrmoser
categories:
- de
- sysadmin
date: "2008-09-25T14:19:18+00:00"
tags:
- Rails
title: RoR@campusspeicher.de – Hello, world
type: post
url: /2008/09/rorcampusspeicherde-hello-world/
yourls_shorturl:
- http://s.mro.name/28
---
Eine Schritt-für-Schritt Anleitung zur ersten eigenen [Ruby on Rails][1] Applikation beim Shared
Hosting Anbieter [http://www.campusspeicher.de](http://www.campusspeicher.de):

## Die lokale Rails Applikation

1. lokal Rails 1.2.3 als Gem installieren:
<pre class="line-numbers"><code class="language-shell-session">$ sudo gem install rails -v 1.2.3
</code></pre>

2. Eine neue Applikation anlegen und testweise starten:
<pre class="line-numbers"><code class="language-shell-session">$ rails hellorails
$ cd hellorails
$ script/server
</code></pre>

Der Browser sollte jetzt bei http://localhost:3000 einen kurzen Begrüßungstext zeigen.

* Die gewünschte Rails Version in der neuen Applikation einstellen. Dazu mit einem Texteditor die
Datei `config/environment.rb` so einstellen, daß sie `1.2.3` in der folgenden Zeile enthält:
<pre class="line-numbers"><code class="language-ruby"># Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '1.2.3' unless defined? RAILS_GEM_VERSION
</code></pre>
* Einen neuen Controller anlegen:
<pre class="line-numbers"><code class="language-shell-session">$ script/generate controller hello
</code></pre>
* Den Controller `app/controllers/hello_controller.rb` anpassen, so daß er folgenden Inhalt hat:
<pre class="line-numbers"><code class="language-ruby">class HelloController < ApplicationController
  def index
    render :text => "Hello, world!"
  end
end
</code></pre>
* Ausprobieren:
<pre class="line-numbers"><code class="language-shell-session">$ script/server
</code></pre>

Der Browser sollte jetzt bei http://localhost:3000/hello als Text &#8222;Hello, world!&#8220; zeigen.

## Zum Campusspeicher hochladen

1. per Plesk eine Subdomain &#8222;hellorails&#8220; anlegen, CGI + FastCGI aktivieren
2. per [lftp][2] nachgucken ob sie da ist:
<pre class="line-numbers"><code class="language-shell-session">$ lftp mydomain.example
lftp ftp_user@mydomain.example:/> cd /subdomains/hellorails/httpdocs
lftp ftp_user@mydomain.example:/subdomains/hellorails/httpdocs> ls -Al
lftp ftp_user@mydomain.example:/subdomains/hellorails/httpdocs> quit
</code></pre>

3. die Datei `public/dispatch.fcgi` anpassen, so daß sie mit folgender Zeile beginnt:
<pre class="line-numbers"><code class="language-ruby">#!/usr/bin/ruby
</code></pre>

4. das komplette Verzeichnis `hellorails` zum Webserver in dessen Unterverzeichnis `subdomains/hellorails/httpdocs` kopieren,
<pre class="line-numbers"><code class="language-shell-session">$ cd ..
$ lftp -e "mirror --delete --reverse hellorails subdomains/hellorails/httpdocs/;quit" mydomain.example
</code></pre>

5. eine `.htaccess` Datei mit folgendem Inhalt anlegen:
<pre class="line-numbers"><code class="language-apacheconf">RewriteEngine On
RewriteRule ^$ /hellorails/public/index.html [L]
RewriteCond %{REQUEST_URI} !^/hellorails/public
RewriteRule ^(.*)$ /hellorails/public/$1 [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ /hellorails/public/dispatch.fcgi/$1 [QSA,L]
</code></pre>

6. in das Wurzelverzeichnis der Subdomain hochladen: `subdomains/hellorails/httpdocs`
7. fertig! Der Browser sollte jetzt unter http://hellorails.mydomain.example/hello &#8222;Hello, world!&#8220; anzeigen.

 [1]: http://www.rubyonrails.org/
 [2]: http://lftp.yar.ru/