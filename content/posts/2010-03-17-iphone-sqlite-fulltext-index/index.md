---
author: Marcus Rohrmoser
categories:
- en
- development
date: "2010-03-17T17:30:28+00:00"
tags:
- Binary
- Fulltext
- Index
- iPhone
- Search
- SQLite
title: 'iPhone: SQLite Fulltext Index'
type: post
url: /2010/03/iphone-sqlite-fulltext-index/
yourls_shorturl:
- http://s.mro.name/39
---
Most blogposts I found on this topic don't explain it from the beginning, so it took me quite a while to figure out how simple and powerful the [sqlite fulltext search API (FTS3)][1] actually is.

1. get the [sqlite3 amalgamation sources (v3.6.22)][2] and just unpack it into your iPhone project – sqlite3 as shipped on the iPhone doesn't support FTS3,
2. [activate FTS3 support][3],
3. [create the index table][4] programmatically inside your App as your Macs default sqlite3 doesn't support FTS3 either,
4. use it like any other table except [using the MATCH operator for index queries][5].

That's it, and it's hell quick.

 [1]: http://www.sqlite.org/fts3.html
 [2]: http://www.sqlite.org/amalgamation.html
 [3]: http://www.v2ex.com/2008/11/22/full-text-search/
 [4]: http://www.sqlite.org/fts3.html#section_1_1
 [5]: http://www.sqlite.org/fts3.html#section_3