---
title: 'Digitalisierung provinziell'
url: /2022/10/digitalisierung/
draft: false
categories:
- de
date: 2022-10-16T09:35:49+0200
tags:
- Digitalisierung
- Traunstein
type: post
author: Marcus Rohrmoser
---

Letzte Woche gab es hier in Traunstein zwei Ereignisse, die oberflächlich
betrachtet nichts miteinander zu tun haben aber doch in Verbindung stehen. Nicht
nur weil sie exakt denselben Ort betreffen, eine Halle am Bahnhof.

Es ist dies der Abriß der 'Güterhalle' und die Eröffnung des Studiengangs
'E-Commerce' am 'Campus Chiemgau' (der zum Platz der Güterhalle ziehen soll) in der
Stadt, in der die Eisenbahn vor 100 Jahren ein zuvor 300 Jahre lang florierendes
Geschäftsmodell beendet hat. (Das alte lief ohne fossile Energie, war
'enkeltauglich' und nachhaltiger als alles was nachkam.)

Doch zunächst einmal zum Abriß der Güterhalle. Sie war einmal Lager beim
Güterumschlag von und zur Bahn. Bevor LKWs diesen Transport komplett übernommen
haben. Güterverkehr per Bahn ist in Traunstein heute nicht mehr zu sehen. Wenn
diese Hallen nicht mehr gebraucht werden, heißt das, die Tonnage wird anders
bewegt – also per dieselgetriebenem LKW. Und zwar nicht nur vom Bahnhof zum Ziel,
sondern wesentlich weitere Strecken – nicht erst 2022 ein Unding.

Doch was soll nachkommen? Eine Idee vermittelt vielleicht, womit sich der
Hochschulstandort Traunstein befassen soll:

"Aus- und Weiterbildungsangebote mit dem Themenschwerpunkt Digitalisierung"[^1]
schreibt die Hochschule selbst und "Online-Shopping, Online Marketing, Künstliche
Intelligenz"[^2] schreibt Christine Haberlander vom Bayerischen Rundfunk.

Das sind die üblichen Stichworte zur Beschönigung, wenn Digitalisierung nichts
produziert und keine Aufgaben löst, sondern verwaltet und viel (Online-Shopping),
sehr viel (Online-Marketing) oder sogar immens viel (KI) Energie dazu verbraucht.

Mit Online-Marketing ist schlicht Werbung mit Beschnüffeln und kommerzieller
Rasterfahndung gemeint. Stickwort Real-Time-Bidding.

Die ganzen online geshoppten Güter werden dann per LKW her- und ggf. wieder zurück gefahren.

Das sind Visionen kleinster Kragenweite, hochgradig abhängig von Konsum, billigem
Transport und billiger Energie, ein ungebremstes 'weiter so!'. Sie nehmen keine
Herausforderung der realen Gegenwart an, sondern flüchten in eine online Scheinwelt
und verweisen auf ein obszön primitives Gesellschaftsbild. Consumo ergo sum.

Nüchtern betrachtet ist diese Realitätsverweigerung bereits heute schon nicht mehr
möglich.

Das Handwerk wie Service und der soziale Sektor leiden an krassem Personal- und
Nachwuchsmangel, das Vereinsleben verkümmert. Das Artensterben und die soziale
Verarmung haben dramatische Ausmaße angenommen.

Doch Online-Shopping, Online-Marketing und KI wird Handwerk, Pflege oder Vereinen
nicht helfen.

Das ist aber was gebraucht wird. In der Realität, von Menschen, vor Ort, überall.

[^1]: https://www.th-rosenheim.de/die-hochschule/standorte/campus-chiemgau/  
[^2]: https://www.br.de/nachrichten/bayern/traunstein-soll-mit-campus-chiemgau-hochschulstandort-werden,TJwzh0R

