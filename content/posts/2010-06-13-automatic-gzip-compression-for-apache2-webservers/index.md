---
author: Marcus Rohrmoser
categories:
- en
- sysadmin
date: "2010-06-13T11:55:25+00:00"
tags:
- apache
- Debian
- gzip
- htaccess
- Linux
- mod_deflate
title: Automatic gzip compression for Apache2 Webservers
type: post
url: /2010/06/automatic-gzip-compression-for-apache2-webservers/
yourls_shorturl:
- http://s.mro.name/2n
---
after failing and failing again in the last months, I finally got it with the help of <http://www.debian-administration.org/articles/137>

The `.htaccess` configuration

<pre class="line-numbers"><code class="language-apacheconf">AddOutputFilterByType DEFLATE text/html text/plain text/xml text/css application/javascript
</code></pre>

requires Apache's `mod_deflate` enabled via

<pre class="line-numbers"><code class="language-shell-session">$ a2enmod deflate
Module deflate installed; run /etc/init.d/apache2 force-reload to enable.
</code></pre>

Check the result with <http://www.gidnetwork.com/tools/gzip-test.php>

**Caution:** There seems to be an [If-Modified-Since/Last-Modified HTTP 304 bug in mod_deflate][1], so better don't use it for large, rarely changing files.

 [1]: http://phpperformance.de/mod_gzip-mod_deflate-und-sonstige-komprimierungsverfahren-fuer-web-inhalte/#comment-17970
