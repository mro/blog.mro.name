---
title: '#Seppo Security Audit'
url: /2024/10/seppo-security-audit/
draft: false
categories:
- en
date: 2024-10-23T20:22:20+0200
tags:
- Audit
- NGI0
- NLnet
- Security
- Seppo
type: post
author: Marcus Rohrmoser
---

A part of NGI0 grant https://nlnet.nl/project/Seppo was a mandatory security
audit by Radically Open Security (ROS)[^ros]. I was very happy and curious
to see #Seppo[^seppo] inspected by 3rd-party, neutral field experts, because
I write software for decades, however have done few web and picked up
Ocaml[^ocaml] just a few years ago mostly on my own. So if there was one thing
lacking, it was eyes. Plus I made some unusual design decisions to be content
without root privileges for installation and rather enable a broad audience to
operate a #Seppo emancipated, on their own, on shared web hosting. And then you better go at length to
make it safe &amp; secure from the start.

It took ROS a bit to find an Ocaml expert for the white-box code review, but
finally there was a black-box (pen) test and a code audit. The black-box test
was against a sibling installation next door to @aSeppoToTry@Seppo.Social[^try],
while the code audit was of https://seppo.social/development in version 89fad6b.

The results from the pen test were roughly:

- an issue with session lifetime rated 'moderate' which appeared worse to me,
- malicious or erratic input getting HTTP error codes without UI nor retry buttons,
- login rate limiting so discrete it was overlooked by the tester (and mistaken for a DOS),
- some 500s from firing assertions.

The code audit found:

- some unusual but justified, non-mainstream design decisions bearing the danger of loosing the required technical environment in the future (CGI[^cgi], XSLT[^xslt]),
- some bugs with lwt[^lwt] and exceptions,
- error messages refering to non-existing support pages
- documentation left a bit to be desired, esp. for onboarding developers.

Within 2 weeks those issues were fixed (and documentation improved) and there
was confidence that the unique approach of #Seppo was in fact sane, safe and
sound, certified by neutral technical experts!

A small downer was the long turnaround times and that I got the final
report[^report] after writing this article, but hey, that's paper &mdash; the fixes are in running
software meanwhile.

The project improved a lot, I learned a bit about web sessions and idiomatic
Ocaml and was able to confidently drop a huge burden of doubt.

I recommend this to all FOSS developers, if you have the opportunity to get your
software audited - take it! There even may be funds for that, NLNet is
happy to see such happen.

If you're interested in more details, let me know, I may cover them in a
follow up.

[^ros]:   https://www.radicallyopensecurity.com
[^seppo]: https://Seppo.Social
[^ocaml]: https://ocaml.org
[^try]:   https://seppo.social/aseppototry
[^cgi]:   CGI https://www.rfc-editor.org/rfc/rfc3875.html
[^xslt]:  XSLT 1.0 https://www.w3.org/TR/xslt-10/
[^lwt]:   https://opam.ocaml.org/packages/lwt/
[^report]: [📄 Radically Open Security: Penetration Test Report](NGIE Seppo penetration test report 2024 1.0.pdf)
[^report0]: meanwhile a dump of the [list of issues](../../../wp-content/uploads/2024/10/Issues%20%C2%B7%20NLnet%20_%20ngie-seppo%20%C2%B7%20GitLab%20(19.9.2024%2012_00_48).html) has to do
