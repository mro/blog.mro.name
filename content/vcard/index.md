---
author: Marcus Rohrmoser
date: "2010-11-04T12:22:40+00:00"
title: Kontakt
type: page
yourls_shorturl:
- http://mro.name/49
---
Um mich zu kontaktieren können Sie

  * mir eine Email an [contact@mro.name][1] schreiben oder
  * mich anrufen: +49 86 eins zwei null 07 62 neun drei.

Eine [<img class="alignnone size-full wp-image-2063" title="vcard-mini.logo" src="http://blog.mro.name/wp-content/uploads/2010/11/vcard-mini.logo_.png" alt="" width="16" height="13" />elektrische Visitenkarte für Ihr Adressprogramm][2] gibt&#8217;s auch.

Außerdem bin ich u.a. in diesen Verbänden und Plattformen mit dabei:

<p style="text-align: center; valign: middle;">
  <a style="margin: 0 1em; border: none;" href="http://r.mro.name/acm"><img class="size-full wp-image-2024 alignnone valignmiddle" title="Association for Computing Machinery" src="http://blog.mro.name/wp-content/uploads/2014/01/gchacm-logo.svg" alt="ACM Logo" width="50" height="50" /></a> <a style="margin: 0 1em; border: none;" href="http://www.gi-ev.de"><img class="alignnone size-full wp-image-2025 valignmiddle" title="Gesellschaft für Informatik" src="http://blog.mro.name/wp-content/uploads/2014/09/GI_Logo.svg" alt="GI Logo" height="36" /></a> <a style="margin: 0 1em; border: none;" href="http://www.ccc.de"><img class="alignnone size-full wp-image-2025 valignmiddle" title="Chaos Computer Clup" src="http://blog.mro.name/wp-content/uploads/2014/09/Logo_CCC.svg" alt="CCC Logo" height="48" /></a> <a style="margin: 0 1em; border: none;" href="http://r.mro.name/gulp"><img class="alignnone size-full valignmiddle" title="Gulp" src="http://blog.mro.name/wp-content/uploads/2014/09/gulp-logo.png" alt="Gulp Logo" height="35" /></a> <a style="margin: 0 1em; border: none;" href="http://r.mro.name/xing"><img class="alignnone size-full wp-image-2028 valignmiddle" title="Xing" src="http://blog.mro.name/wp-content/uploads/2014/09/Xing_logo.svg" alt="Xing Logo" height="44" /></a> <a style="margin: 0 1em; border: none;" href="http://r.mro.name/github"><img class="alignnone size-full wp-image-2026 valignmiddle" title="GitHub" src="http://blog.mro.name/wp-content/uploads/2014/09/GitHub_Logo.svg" alt="GitHub Logo" height="24" /></a> <a style="margin: 0 1em; border: none;" href="http://cocoaheads.org/de/Munich/"><img class="alignnone size-full wp-image-2026 valignmiddle" title="CocoaHeads" src="http://blog.mro.name/wp-content/uploads/2014/01/cocoaheads-logo.svg" alt="CocoaHeads Logo" width="50" height="50" /></a>
</p>

 [1]: mailto:contact@mro.name?subject=Kontakt
 [2]: http://mro.name/vcard/