---
author: Marcus Rohrmoser
date: "2010-05-30T12:58:39+00:00"
language: de-de
title: Impressum & Datenschutz
type: page
yourls_shorturl:
- http://mro.name/48
---
## Impressum

verantwortlich für diese Seite ist:

Marcus Rohrmoser  
Rupprechtstr. 16, 83278 Traunstein  
email: webmaster [at] mro [punkt] name, [gpg public key][1]  
fon: +49 861 zwo null 07 62 neun drei  
USt.-ID-Nr. DE263240032

### Mitgliedschaften

[![CCC Logo](ccc_--_logo.svg#height50)](https://ccc.de/)
[![CocoaHeads Logo](cocoaheads_--_logo.svg#height50)](http://cocoaheads.org/de/Munich/)
[![FIfF Logo](fiff_--_logo.opt.svg#height50)](https://fiff.de/)
[![noyb Logo](noyb_--_logo.png#height50)](https://noyb.eu/)
[![ACM Logo](gchacm_--_logo.svg#height50)](https://member.acm.org/~mrohrmoser)
[![SIGCHI Logo](sigchi_--_logo.svg#height50)](https://www.acm.org/sigchi)
[![SIGMOBILE Logo](sigmobile_--_logo.svg#height50)](https://www.acm.org/sigmobile)
[![GI Logo](gi_--_logo.svg#height50)](https://gi.de/)
[![OCaml Logo](ocaml_--_logo.svg#height50)](https://discuss.ocaml.org/u/mro/)
[![Codeberg Logo](codeberg_--_logo.svg#height50)](https://codeberg.org/mro/)
[![App Store Logo](download-on-the-app-store.svg#height50)](https://itunes.apple.com/de/developer/marcus-rohrmoser-mobile-software/id333210958)
[![Liberapay Logo](liberapay_--_logo.svg#height50)](https://liberapay.com/mro.name)

## Datenschutz

### Logdaten des Webservers

Bei jedem Aufruf dieser Website werden vom Webserver die folgenden Daten gespeichert und für vier Wochen aufbewahrt:

* Referrer (zuvor besuchte Webseite)
* Angeforderte Webseite oder Datei
* Browsertyp und Browserversion
* Verwendetes Betriebssystem
* Verwendeter Gerätetyp
* Uhrzeit des Zugriffs
* IP-Adresse (derzeit nicht)

### Kommentarfunktion

Wenn Sie zu einem Blogbeitrag einen Kommentar hinterlassen haben, dann werden außer dem
Kommetartext und Zeitpunkt auch noch der Absendername und die Email-Adresse gespeichert. Letztere
wird nicht veröffentlicht. Sie können in diesem Blog vollständig anonym kommentieren.

Die Daten werden nur nach Widerspruch gelöscht. Dies betrifft nicht den Inhalt Ihres Kommentars.

### Drittanbieter

keine.

 [1]: /wp-content/uploads/2013/07/F87AF309.asc
