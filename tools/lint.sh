#!/bin/sh
cd "$(dirname "$0")/../content/posts/" || exit 1

grep -E "^url: .+[^/]\$" ./*/index.md
# grep -E "^title: [^']"   ./*/index.md

ls ./*/comments.xml \
| while IFS= read -r fn
do
    if xmllint --noout --relaxng ../../tools/atom.rng "$fn" >/dev/null 2>&1
    then
        echo "✅ $fn"
    else
        echo "❌ $fn"
    fi
done
# /home/mro/Documents/2024/blog.mro.name/content/posts

