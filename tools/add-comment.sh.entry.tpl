  <entry>
    <title>💬 <![CDATA[${p_title}]]></title>
    <author>
      <name><![CDATA[${c_author}]]></name>
    </author>
    <id>https://blog.mro.name${p_slug}comments.xml#c${c_num}</id>
    <updated>${p_date}</updated>
    <content type="text"><![CDATA[${c_content}]]></content>
    <thr:in-reply-to ref="https://blog.mro.name${p_slug}" href="https://blog.mro.name${p_slug}" type="text/html"/>
  </entry>
