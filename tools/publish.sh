#!/bin/sh
set -e
cd "$(dirname "${0}")/.." || exit 1

# preflight validation
grep -E "^url: .+[^/]\$" ./content/posts/*/index.md && exit 1
grep -E '[^/]comments.xml$' ./content/posts/*/comments.xml && exit 1
# grep -lE '[^/]" type="text/html"/>' ./content/posts/*/comments/index.xml && exit 1
# grep -E '^title: [^\']'  ./content/posts/*/comments/index.xml && exit 1

# must be valid
xmllint \
  --relaxng ./tools/atom.rng \
  --noout \
  ./content/posts/*/comments.xml \
|| exit 1

rm -rf public

dst="c1:/usr/local/www/vhosts/blog.mro.name/pages/"
dst="v0:~/mro.name/blog/"
opts=""
if [ "${1}" = "v" ] ; then
  dst="s0:/usr/local/www/lighttpd/vorschau.blog.mro.name/public_html/"
  opts="--buildDrafts --buildFuture"
fi

~/bin/hugo ${opts}

rsync \
  --bwlimit=80 \
  --delete \
  --delete-excluded \
  --exclude '.DS_Store' \
  --exclude '*.raw*' \
  --exclude 'resources*' \
  -aPz \
  public/ "${dst}"

exit

ssh s0 sh /var/www/lighttpd/blog.mro.name/public_html/assets/B612.sh

ssh s0 curl \
	--location \
	--remote-time \
	--output "/usr/local/www/lighttpd/blog.mro.name/public_html/assets/eicar.com" \
	https://secure.eicar.org/eicar.com
