#!/bin/sh
cd "$(dirname "${0}")/.." || exit 1
readonly me="tools/$(basename "$0")"

read -rp "New Post Title: " p_title
read -rp "New Post Slug:  " p_slug
read -rp "New Post Date: [now] " p_date

[ "" = "${p_date}" ] && p_date="$(date +%FT%T%z | sed 's/\(..\)$$/:\1/')"

p_day="$(echo "${p_date}" | cut -c -10)"
p_month="$(echo "${p_date}" | cut -c -7 | sed 's;-;/;')"
p_dir="content/posts/${p_day}-${p_slug}"
p_slug="/${p_month}/${p_slug}/"

export p_title p_date p_dir p_slug

mkdir -p "${p_dir}/"
envsubst \
  < "${me}.tpl" \
  > "${p_dir}/index.md"

"${EDITOR}" \
  "${p_dir}/index.md"

