<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type='text/xsl' href='../../../assets/comments.xslt'?>
<feed xmlns="http://www.w3.org/2005/Atom" xmlns:thr="http://purl.org/syndication/thread/1.0" xml:lang="de-DE">
  <title type="text">💬 <![CDATA[${p_title}]]></title>
  <subtitle type="text">Marcus Rohrmoser mobile Software</subtitle>
  <updated>${p_date}</updated>
  <link rel="self" type="application/atom+xml" href="https://blog.mro.name${p_slug}comments.xml"/>
  <id>https://blog.mro.name${p_slug}comments.xml</id>
</feed>
